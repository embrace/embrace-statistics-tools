"""
$Id: plot_ABdriftComparison.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Mon 14 Jan 2013 12:58:41 CET

make a plot comparing Beam-A and Beam-B drift scan

bxA=estats('scudata/....')
bxB=

$modified: Tue 27 Aug 2013 18:52:36 CEST
  changing to defined function for import
  name change from plot-AB-drift-comparison.py to plot_ABdriftComparison.py
  (removing minus signs)

$modified: Wed 02 Oct 2013 17:55:58 CEST
  allowing for comparison of csx homemade beamlets and bsx
  (could be Beam-A and Beam-B or whatever)
  see also plot_homemadeBSXcomparison.py


"""
import sys
import matplotlib.pyplot as plt
import numpy as np


def ABdriftComparison(sxA,sxB,offset=18.3,pmin=None,pmax=None):
    
    pngname=sxA.Filename().replace('BeamA_','').replace('BeamB_','').replace('.fits','')\
        +'_BeamA-BeamB_timeline'+str("%7.2f" % sxA.RFcalfreq())+'.png'

    print 'using offset for B beam: ',offset,'dB'



    ttl='Timeline '+sxA.Title()+', Beam-A and Beam-B'
    subttl='(20dB attenuators on RCU inputs for Beam-A only)'
    fig=plt.figure(figsize=sxA.metainfo['figsize'])
    fig.canvas.set_window_title('plt: '+ttl) 
    ax1 = fig.add_subplot(111)

    plt.suptitle(ttl,fontsize=16)
    plt.title(subttl,fontsize=14)

    labelA='Beam '+sxA.RFbeam()
    if sxA.statsType()=='CSX':
        pA,tA=sxA.Timeline(plot='beamlet',ignore=True)
        labelA+=' homemade'
    else:
        pA=sxA.Timeline()
        tA=np.array(range(sxA.nData()))
    plt.plot(tA,pA,label=labelA)

    labelB='Beam '+sxB.RFbeam()
    if sxB.statsType()=='CSX':
        pB,tB=sxB.Timeline(plot='beamlet',ignore=True)
        labelB+=' homemade'
    else:
        pB=sxB.Timeline()
        tB=np.array(range(sxB.nData()))

    labelB+=' (shifted down '+str(offset)+'dB)'
    timeOffset=tot_seconds(sxB.startTime() - sxA.startTime())
    plt.plot(tB+timeOffset,pB-offset,label=labelB)
        

    minval=min([min(pA),min(pB-offset)])
    maxval=max([max(pA),max(pB-offset)])
    axes=[0,sxA.metainfo['ndata'],minval,maxval]

    if isinstance(pmin,float) or isinstance(pmin,int):
        axes[2]=pmin
    if isinstance(pmax,float) or isinstance(pmax,int):
        axes[3]=pmax

    ax1.axis(axes)
    sxA.pltObsModeTimes([axes[2],axes[3]])


    plt.xlabel('time / seconds since '+str(sxA.startTime()))
    plt.ylabel('power / dBm')
    plt.legend(prop={'size':12},loc='upper right',bbox_to_anchor=(0, 0, 1.05, 1.05))

    ret=plt.savefig(pngname,format='png',dpi=100)
    plt.show()
        
    return

