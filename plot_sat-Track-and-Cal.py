import datetime as dt
def plot_TrackCal(bsx,submitlog,freq,pmin=None,pmax=None):
    """
    $Id: plot_sat-Track-and-Cal.py
    $auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
    $created: Fri Nov 18 15:38:01 CET 2011
    to plot sat-Track-and-Cal with labels for tracking and calibrating
    
    this should go into the BLStats class
    
    """


    strpfmt='%Y-%m-%d %H:%M:%S'#.%f UT'

    pngfile=bsx.filename.replace('.fits','_timeline.png')

    trackstart=[]
    trackend=[]
    RFcalstart=[]
    digcalstart=[]
    import re

    # from the submitlog
    # make a loop to read all the  times
    handle=open(submitlog,'r')
    loglines=handle.readlines()
    handle.close()
    for logline in loglines:
        logline=logline.replace('\n',"")
        match=re.search('tracking.*beginning at ',logline)
        if match:
            datestr=logline.replace(match.group(),"")
            datestr=datestr[0:19]
            trackstart.append(dt.datetime.strptime(datestr,strpfmt))
            continue
        
        match=re.search('tracking.*ending at ',logline)
        if match:
            datestr=logline.replace(match.group(),"")
            datestr=datestr[0:19]
            trackend.append(dt.datetime.strptime(datestr,strpfmt))
            continue

        match=re.search('RF calibration beginning at ',logline)
        if match:
            datestr=logline.replace(match.group(),"")
            datestr=datestr[0:19]
            RFcalstart.append(dt.datetime.strptime(datestr,strpfmt))
            continue

        match=re.search('digital calibration beginning at ',logline)
        if match:
            datestr=logline.replace(match.group(),"")
            datestr=datestr[0:19]
            digcalstart.append(dt.datetime.strptime(datestr,strpfmt))
            continue
        

    plt.figure(figsize=(12.80,9.60))
    plt.title(bsx.title+' tracking with periodic RF and digital calibration')
    plt.ylabel('uncalibrated power at '+str(freq)+'MHz / dB')
    plt.xlabel('time / seconds since '+str(bsx.Timestamp(1))+' UT')

    if pmin==None:pmin=80
    if pmax==None:pmax=105
    y=[pmin,pmax]
    text_y=y[1]-0.1*(y[1]-y[0])
    text_size=10
    plt.axis([-60,bsx.ndata,y[0],y[1]])
    plt.plot(bsx.Timeline(freq,0))
        
    for t in trackstart:
        tsecs=(t-bsx.Timestamp(1)).seconds
        plt.plot([tsecs,tsecs],y,color='grey')
        plt.text(tsecs+125,text_y,'tracking',fontsize=text_size,rotation=90,va='top')

    for t in trackend:
        tsecs=(t-bsx.Timestamp(1)).seconds
        plt.plot([tsecs,tsecs],y,color='red',linestyle='--')

    for t in RFcalstart:
        delta=t-bsx.Timestamp(1)
        tsecs=delta.days*24*3600 + delta.seconds    
        plt.plot([tsecs,tsecs],y,color='green',linestyle=':')
        plt.text(tsecs+10,text_y,'RF calibration',fontsize=text_size,rotation=90,va='top')

    for t in digcalstart:
        delta=t-bsx.Timestamp(1)
        tsecs=delta.days*24*3600 + delta.seconds    
        plt.plot([tsecs,tsecs],y,color='green',linestyle=':')
        plt.text(tsecs+10,text_y,'digital calibration',fontsize=text_size,rotation=90,va='top')

    ret=plt.savefig(pngfile,format='png',dpi=100)
    plt.show()
    return
