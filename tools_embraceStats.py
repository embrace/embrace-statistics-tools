"""
$Id: tools_embraceStats.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Fri Mar 23 10:42:27 CET 2012

tools include methods and global variables used by the statistics classes

In general, these are methods originally developed in BLStats(),
SBStats(), and CLStats() which became more generally useful.

Functions defined here should be called with the metainfo argument.

$modified: Wed Jun 13 15:16:09 CEST 2012
  calculateTimeline() is now here
    convert to linear before summing frequency channels, and then go back to dBm

    not normalizing by number of frequency channels

$modified: Tue Jun 19 16:55:59 CEST 2012
  bandwidthChannels() : option to give bandwidth in number of channels instead of frequency

$modified: Tue 05 Mar 2013 13:18:21 CET
  tilesetBeamSize() : taken from ObsFunctions.py

$modified: Tue 19 Mar 2013 10:45:06 CET
  recentdata() : find the most recent data file
  
$modified: Wed 20 Mar 2013 12:06:39 CET
  bringing stuff here from embraceStats and BLStats

  embraceNancay for ephem calculations

  pointing() : for RF or digital beam, for direction X or Y

$modified: Wed Jun 19 13:15:27 CEST 2013
  trying to implement CalculateTimeline4bigfile
  trying different stuff...

$modified: Fri 21 Jun 2013 15:02:46 CEST
  using child processes for CalculateTimeline4bigfile()
  this allows memory to return to system after the task is done
  pyfits is pretty crap about reading big files
  python is pretty crap about memory management
    del and gc.collect() don't really do anything

  returning the timeline in file, which is not a very elegant solution
  but I've been wrestling with this for too long.

$modified: Sun Jun 23 10:39:01 CEST 2013
  child forking seems to work, but cannot do parallel access to the FITS file
  so only one child is forked at a time, and we wait for it to finish
  when it returns, the memory is returned to system

$modified: Wed 26 Jun 2013 13:08:06 CEST
  readKeyword() now takes headerlist as argument instead of hdulist
  all references to hdulist.header are replaced by headerlist

$modified: Fri 28 Jun 2013 10:18:12 CEST
  str2dt() : added format YYYYmmdd-HHmm

$modified: Fri 28 Jun 2013 16:52:22 CEST
  SSMAP() : now returns from metainfo instead of reading the FITS header
  headerlist : abandoned.  going back to previous setup (see also comment in embraceStats.py)

$modified: Thu 04 Jul 2013 15:13:42 CEST
  added onesid and onesol:  useful time deltas

$modified: Tue 23 Jul 2013 12:30:07 CEST
  tot_seconds() : copied from ObsFunctions.py

$modified: Mon 26 Aug 2013 12:27:39 CEST
  tot_seconds() and str2dt() moved to datefunctions.py

$modified: Thu 14 Nov 2013 16:58:33 CET
  CalculateTimeline() and CalculateTimeline4Bigfile() : option to return linear instead of dBm
"""

import os,gc#,psutil
import pickle as PyPickle
import numpy as np
import math
import subprocess
import ephem as eph
import datetime as dt
import re
from datefunctions import *

embraceNancay = eph.Observer()
embraceNancay.long = '2:11:57.48'    # angle 'deg:min:sec.xx' or x.y radians
embraceNancay.lat = '47:22:55.2'    # angle 'deg:min:sec.xx' or x.y radians
embraceNancay.horizon='45:00:00.0' # minimum elevation
embraceNancay.elevation = 100
embraceNancay.pressure=0 # to ignore atmospheric corrections (optical only)

speedolight=299.792458 # m * MHz

# useful time delta, one solar, and one sidereal day
onesid=dt.timedelta(hours=23)+dt.timedelta(minutes=56)
onesol=dt.timedelta(hours=24)

bigfilelimit=1024**3
#bigfilelimit=1024 # for testing

# return a datetime object given a string

def tilesetBeamSize(freq):
    

    # EMBRACE@Nancay array dimensions
    vivaldi_width=0.125

    tile_width=vivaldi_width*6/math.sqrt(2)
    tileset_width=4*tile_width
    array_width=4*tileset_width

    vivaldi_maxno_NS=23+1+23+1+23+1+23 # see Chenanceau notebook 2012-05-22
    vivaldi_maxseparation_NS=vivaldi_maxno_NS-1

    vivaldi_maxno_EW=4*24
    vivaldi_maxseparation_EW=vivaldi_maxno_EW-1
        
    #### this is the maximum width, ie. the smallest dimension for beam size
    arrayWidth=vivaldi_maxseparation_EW*vivaldi_width


    wvln=speedolight/freq
    return math.degrees(wvln/(arrayWidth/4))

# find the most recent file
def recentdata(stats='csx',path=None,beam='A'):
    datapath=['/scu/data/obs',
              '/SCU/data/obs',
              '/data/archive/embrace-archive/2012',
              '/henrik/2012',
              os.environ['HOME']+'/data']
                     
    if path!=None:datapath.insert(0,path)

    for d in datapath:
        if not os.path.exists(d):continue
        cmd='/bin/ls -t '+d+'/*_Beam'+beam+'_'+stats+'_*.fits'
        f=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
        fstr,err=f.communicate()
        flist=fstr.split('\n')
        fname=flist[0]
        if fname!='': return fname
            
    print 'no file found'
    return None

# read a keyword value 
def readKeywordValue(hdulist,keyword,hdu=0):
    """
    read a FITS header keyword value
    """
    if hdu>len(hdulist):
        print "ERROR: trying to access an HDU which is out of range"
        return None
    
    if not keyword in hdulist[hdu].header.keys():
        print "ERROR: keyword does not exist: ",keyword
        return None
    
    return hdulist[hdu].header[keyword]

# read a keyword value for keywords which are duplicated
# for example:
# for embrace FITS version 1.1 and lower
# the SSMAP keyword appears once for each digital beam
# repeated keywords is not FITS compatible, so we have to
# loop around to get all the SSMAPS
# we want the nth value, where n is counted from 1
def readDuplicatedKeywordValue(hdulist,n,keyword,hdu=0):
    """
    read a keyword value for keywords which are duplicated
    """
    keys=hdulist[hdu].header.keys()
    index=0
    value=[]
    for key in keys: 
        if key==keyword: 
            # create an evaluated list of the VALUE
            value.append(hdulist[hdu].header[index])
        index+=1
    nvalues=len(value)
    return value[n-1]

def picklefilename(metainfo,datatype):
    if datatype=='metainfo':
        return metainfo['filename'].replace('.fits','')+'.metainfo.pickle'
    if datatype=='timeline':
        return metainfo['filename'].replace('.fits','')+'.timeline.pickle'
    return metainfo['filename'].replace('.fits','')+'.pickle'
    
# dump data to a pickle file for quick reloading later
# cannot pickle a file pointer, so for metainfo, hdulist has to be temporarily replaced
def pickle(metainfo,data=None,datatype='dynamicspec'):
    picklename=picklefilename(metainfo,datatype)
    opickle=open(picklename,'w')

    if datatype=='metainfo':
        hdulist=metainfo['hdulist']
        metainfo['hdulist']=None
        PyPickle.dump(metainfo,opickle)
        metainfo['hdulist']=hdulist
    else:
        PyPickle.dump(data,opickle)

    opickle.close()
    print "created pickle file: ",picklename
    return True

# load a pickle file
def unpickle(metainfo,datatype='dynamicspec'):
    picklefile=picklefilename(metainfo,datatype)
    if not os.path.exists(picklefile):
        print "pickle file not found: ",picklefile
        return None
    print "reading pickle file: ",picklefile
    ipickle=open(picklefile,'r')
    data=PyPickle.load(ipickle)
    ipickle.close()
    return data

# readjust given index to an index for the FITS file and
# check input argument for max ntime
def nTime(metainfo,ntime):
    ntime_index=ntime+metainfo['firstHDU']
    if ntime>=metainfo['ndata'] or ntime < 0:
        print "that time reference is out of range"
        return None
    return ntime_index

# find the start and end channels for a desired bandwidth
def bandwidthChannels(sx,freq0,bwidth=0.0,nchans=None,n=None,direction='x'):
    n=sx.verify_n(n)
    if n==None: return None
    freq=sx.Freq(n)

    nbwidthchans=None
    if nchans!=None:
        nbwidthchans=nchans
        bwidth=nbwidthchans*sx.metainfo['chanwidth']


    # find number of freq channels.  
    # For subband statistics, nchans=512
    # For a digital beam, this is variable.  
    # don't use metainfo['nchans'] because that's the number of RF channels (always 512)
    nchans=len(freq)
    chanwidth=sx.metainfo['chanwidth']

    # convert frequency to a channel number
    start_chan=-1
    end_chan=-1
    freq1=freq0 - 0.5*bwidth 
    freq2=freq0 + 0.5*bwidth 
    ilist=range(nchans)
    for i in ilist:
        if ( (start_chan==-1) and (freq1 <= freq[i]) ):
            start_chan=i

    ilist.reverse()
    for i in ilist:
        if ( (end_chan==-1) and (freq2 >= freq[i]) ):
            end_chan=i


    # check if we've asked for a bandwith outside the range
    if start_chan==-1:start_chan=0

    # end_chan depends on whether or not we asked for number of channels or bandwidth
    if nbwidthchans!=None:
        end_chan=start_chan+nbwidthchans-1

    # check if we've asked for a bandwith outside the range
    if end_chan==-1 or end_chan>=nchans: end_chan=nchans-1

    # hack: check if we've gone inside out
    if end_chan<start_chan: end_chan=start_chan

    # calculate the true bandwidth used
    nbwidthchans=end_chan-start_chan+1
    bwidth=nbwidthchans*sx.metainfo['chanwidth']

    # number of channels in the bandwidth used for the integration
    print 'start channel     : ',start_chan
    print 'end channel       : ',end_chan
    print 'number of channels: ',nbwidthchans
    print 'bandwidth         : ',bwidth,' MHz'
    if (nbwidthchans <= 0): return None,None,None

    return start_chan,end_chan,nbwidthchans

# return the frequency given a sub band number
def SBnum2Freq(metainfo,sbnum):
    # start freq is the offset from the beginning of the RF band
    # but we don't need that info, we use the subband select map instead
    # knowing the RF centre, and that the centre is at subband#256 (numbered from 0)
    # (see also the SBStats frequency scale from FITS CRPIX1 etc)
    # the RF centre frequency is in the middle of the 256th counting from 0
    subband0=metainfo['RFcentre']-(metainfo['chanwidth']*256)

    return subband0+sbnum*metainfo['chanwidth']


# return the timeline data for a given frequency range and digital beam/RCU
# for beamlet statistics, n=digbeam
# for subband statistics, n=rcu
def CalculateTimeline(sx,freq0,bwidth=0.0,nchans=None,n=None,direction='x',linear=False):
    print 'requested bandwidth: ',bwidth,' MHz'
    start_chan,end_chan,nbwidthchans=bandwidthChannels(sx,freq0,bwidth,nchans,n,direction)
    if start_chan==None: return None
    print 'actual bandwidth   : ',sx.metainfo['chanwidth']*nbwidthchans,' MHz'

    # check if the file is too big to load all in memory
    if sx.metainfo['filesize'] > bigfilelimit:
        return CalculateTimeline4bigfile(sx,start_chan,end_chan,n,direction)



    # now make the integrated power for each time
    X=sx.DynamicSpec(n,direction)
    if X==None: return None

    # if it's for only one channel, simply assign that column
    if nbwidthchans==1:
        power=X[:,start_chan].flatten(1)
        if linear:
            return 10**(power/10.0)
        return power
        
    # otherwise, sum up the number of channels
    power=np.empty(sx.metainfo['ndata']) # create an empty vector
    lin_spec=np.empty(nbwidthchans) # used to hold the linear power after conversion from dBm
    for ntime in range(sx.metainfo['ndata']):
        chan=0
        for dBm in X[ntime,start_chan:end_chan+1]:
            lin_spec[chan]=10**(dBm/10.)
            chan+=1
#        power[ntime]=sum(lin_spec)
        int_power=sum(lin_spec)/nbwidthchans
        if linear:
            power[ntime]=int_power
        else:
            power[ntime]=10*math.log10(int_power)

    return power

# return timeline data in the case of a very big file
def CalculateTimeline4bigfile(sx,start_chan,end_chan,n=None,direction='x',linear=False):
    print "BIG FILE:  calculating timeline without creating dynamic spectrum in memory"
    print "number of points:  ",sx.metainfo['ndata']

    #nforks=psutil.NUM_CPUS
    #print "number of CPU cores available: ",nforks
    ### override nforks because in fact you can't access the file 
    ### multiple times in parallel
    nforks=1 
    #print "memory available: ",psutil.virtual_memory().available
    maxbatchsize=10000

    # create vector to hold result
    power = np.empty( sx.metainfo['ndata'],dtype=float )

    # launch child processes, and do the work in batches without overrunning memory
    batchsize=sx.metainfo['ndata']/nforks
    ngenerations=1 + batchsize/maxbatchsize
    generation_batchsize=batchsize/ngenerations
    generation=1
    childpid=np.zeros(nforks,dtype=int)
    nstarts=np.zeros(nforks,dtype=int)
    nends=np.zeros(nforks,dtype=int)
    nstart=0
    while generation<=ngenerations and nstart<sx.metainfo['ndata']:
        print "launching child processes..."
        for i in xrange(nforks):
            nend=nstart+generation_batchsize
            if nend >= sx.metainfo['ndata']:
                nend=sx.metainfo['ndata']-1
            nstarts[i]=nstart
            nends[i]=nend
            newpid = os.fork()
            #newpid=0
            if newpid == 0:
                childtimelabourer(sx,nstart,nend,start_chan,end_chan,n,direction)
            else:
                childpid[i]=newpid
                print "parent: %d, child: %d" % (os.getpid(),childpid[i])
                os.wait()
                nstart=nend+1
                
        #print "wait for this generation of children to finish labouring"
        #for i in xrange(nforks):
            #os.waitpid(childpid[i],0)
        # now read the child generated files
        for i in xrange(nforks):
            print "reading results for data points ",nstarts[i]," to ",nends[i]
            fname=childfilename(sx,nstarts[i],nends[i],n,direction)
            ifile=open(fname,'r')
            counter=nstarts[i]
            for line in ifile:
                power[counter]=float(line)
                counter+=1
            ifile.close()

    if linear: return 10**(power/10.0)
    return power

# filename to use for portions of extracted timeline data
def childfilename(sx,nstart,nend,n,direction):
    if sx.metainfo['statstype']=='BSX':
        nstr=sx.DigitalBeamDirection(direction)
    elif isinstance(i,int):
        nstr=str('%02i' % n)
    else:
        nstr=str(n)
    fname=sx.metainfo['filename'].replace('.fits','')+'_timeline'+nstr+'.dat.'+str("%06d-%06d" % (nstart,nend))
    return fname

# child process for calculating/extracting timeline from big fits file
# write result to file to be reread afterwards. so crap.
# for now, it's only picking out one channel.  no integrating of channels
def childtimelabourer(sx,nstart,nend,start_chan,end_chan,n,direction):
    print "child labourer: ",os.getpid()," nstart=",nstart," nend=",nend
    # filename for this portion
    fname=childfilename(sx,nstart,nend,n,direction)
    fhandle=open(fname,'w')

    i=nstart
    while i <= nend:
        ntime_index=nTime(sx.metainfo,i)
        p=sx.Data(ntime_index,n,direction)
        power=p[start_chan]
        fhandle.write(str(power)+'\n')
        i+=1
    fhandle.close()
    os._exit(0)
    return

# return the SSMAP for beamlet/frequency assignments
def SSMAP(metainfo,digbeam=0):
    if metainfo['nDigBeams']<=0:
        print "ERROR: no digital beams defined"
        return None
    # make sure digbeam is a valid number
    digbeam=DigitalBeam(metainfo,digbeam)
    if digbeam==None: return None
    return metainfo['SSMAP'][digbeam]

# return the number of beamlets for a digital beam
def nBeamlets(metainfo,digbeam=0):
    digbeam=DigitalBeam(metainfo,digbeam)
    ssmap=SSMAP(metainfo,digbeam)
    if ssmap==None: return 0
    return len(ssmap)

# this is used to check input argument for max digital beams
# digbeam counts from 0
def DigitalBeam(metainfo,digbeam):
    if metainfo['nDigBeams']<=0:
        return None

    if digbeam==None: digbeam=0

    if digbeam>=metainfo['nDigBeams']:
        print "not that many digital beams.",\
            "Returning value for digital beam #",metainfo['nDigBeams']
        digbeam=metainfo['nDigBeams']-1
    if digbeam<0:
        print "are you testing my error handling?"
        digbeam=0
    return digbeam

# find the corresponding HDU with the digital beam info
def digbeamHDU(metainfo,digbeam):
    if metainfo['fitsversion']<=1.1: return 0

    # make sure digbeam is a valid number
    digbeam=DigitalBeam(metainfo,digbeam)
    if digbeam==None: return None

    gotit=False
    i=0
    while not gotit and i<metainfo['nHDU']:
        i+=1
        if metainfo['hdulist'][i].header['HDUTYPE'] == 'BEAM':
            if metainfo['hdulist'][i].header['BEAMTYPE']== 'DIGITAL':
                if metainfo['hdulist'][i].header['BEAMINDX']==digbeam:
                    gotit=True
                    if metainfo['RFbeam']!=metainfo['hdulist'][i].header['FOV']:
                        print "WARNING: RFbeam header and DigBeam header don't match for Field of View"

    if not gotit:
        i=0
        print "ERROR: no digital beam information for digital beam #",digbeam

    return i

# return timestamp of a given data point as a datetime object
def Timestamp(metainfo,ntime):
    ntime_index=nTime(metainfo,ntime)
    if ntime_index==None: return None

    return metainfo['Timestamp'][ntime]


# read planned pointing angles
# read pointings for digital beams by default
def readPointing(metainfo,digbeam=0,beamtype='dig'):
    if beamtype.upper()=='RF': 
        hdu=metainfo['RFbeamHDU']
    else:
        hdu=digbeamHDU(metainfo,digbeam)

    t=[]
    for datestr in metainfo['hdulist'][hdu].data.field(5):
        t.append(str2dt(datestr))

    az1=metainfo['hdulist'][hdu].data.field(0)
    el1=metainfo['hdulist'][hdu].data.field(1)
    # for RF beam, the angles will be zero, but they are in the fits file
    az2=metainfo['hdulist'][hdu].data.field(2)
    el2=metainfo['hdulist'][hdu].data.field(3)

    return t,az1,el1,az2,el2
    

# return the pointing at a given time
# this is different from readPointing() above
# if the pointing is in RA-dec, convert to AZ-EL
def pointing(metainfo,ntime,digbeam=0,beamtype='dig',direction='x',quiet=False):
    dt_ntime=Timestamp(metainfo,ntime)
    if dt_ntime==None:return None

    if beamtype.upper()=='RF': 
        hdu=metainfo['RFbeamHDU']
    else:
        hdu=digbeamHDU(metainfo,digbeam)

    pointing_times,a1,a2,a3,a4=readPointing(metainfo,digbeam,beamtype)
    # pointing times are not necessarily in chronological order
    # so resort them
    sorted_index=sorted(range(len(pointing_times)), key=lambda k: pointing_times[k])

    # find the last pointing command before the requested time
    CmdTime=pointing_times[sorted_index[0]]
    index=None
    for i in sorted_index:
        t=pointing_times[i]
        if dt_ntime < t:
            break
        CmdTime=t
        index=i
                
    if not quiet:
        print 'requested time:        '+dt_ntime.isoformat(' ')+' UT'
        print 'pointing command time: '+CmdTime.isoformat(' ')+' UT'
    if dt_ntime<pointing_times[sorted_index[0]]:
        if not quiet:print 'first pointing command is after the requested time!'
        return None,None

    if beamtype.upper()=='DIG' and direction.upper()=='Y':
        ang1_deg=a3[index]
        ang2_deg=a4[index]
    else:
        ang1_deg=a1[index]
        ang2_deg=a2[index]


    # determine pointing type: AZEL or J2000
    coordtype=metainfo['hdulist'][hdu].data.field(4)
    if coordtype[index].upper() == 'AZEL':
        if not quiet:print 'pointing commanded in AZEL'
        az_deg=ang1_deg
        el_deg=ang2_deg
    else:
        if not quiet:
            print 'pointing commanded in J2000'
            print 'RA='+str('%.3f degrees' % ang1_deg)
            print 'dec='+str('%.3f degrees' % ang2_deg)
        embraceNancay.date=dt_ntime
        # convert RA degrees to hours
        ra=ang1_deg*24/360.
        # create a source line for pyephem
        line='SOURCE,f,'+str(ra)+','+str(ang2_deg)+',0,2000'
        src=eph.readdb(line)
        src.compute(embraceNancay)
        az_deg=math.degrees(src.az)
        el_deg=math.degrees(src.alt)

    if not quiet:
        print 'az='+str('%.3f degrees' % az_deg)
        print 'el='+str('%.3f degrees' % el_deg)

    az=math.radians(az_deg)
    el=math.radians(el_deg)
    return az,el


