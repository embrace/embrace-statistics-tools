"""
$Id: plot_RFcalibration.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Mon 19 Oct 2015 16:36:45 CEST
$license: GPLv3 or later, see https://www.gnu.org/licenses/gpl-3.0.txt

make a plot showing the RF calibration sequence
"""

# sxA=estats('/data/bigthree/scudata/2015/GPSBIIF-10--GPSBIIF-10_BeamA_ssx_20150909-1447.fits')
# sxA=estats('/data/big/scudata/2013/GPSBIIF-1_BeamA_ssx_20130110-0015.fits')

# tileset=0
try:
    print 'plotting tileset ',tileset
except:
    tileset=0



outputformat='eps'
filename='RFcalibration'
filename2='RFcalibration_driftscan'

#ttl='RF Calibration on GPS BIIF-10, 2015-09-09'
ttl='RF Calibration on GPS BIIF-1, 2013-01-10'

#ttl2='Driftscan on GPS BIIF-10, 2015-09-09'
ttl2='Driftscan on GPS BIIF-1, 2013-01-10'

subtitle=str('Tileset %i' % tileset)

#tileontimes=[59,86,113]
#tileofftimes=[84,111,138]

tileontimes=[437,464,491]
tileofftimes=[462,489,516]

nborders=3
bordercolours=['red','green','black']
#axes=[50,150,7e6,4.5e8]
axes=[430,520,7e6,4.5e8]

# plot linear
y=10**(sxA.Timeline(n=0)/10.)

fig=sxA.openPlot(ttl,filename,subttl=subtitle)
ax1 = fig.add_subplot(111)
ax1.set_xlabel('time / seconds since '+str(sxA.startTime())+' UT')
ax1.set_ylabel('Power / arbitrary linear units')
ax1.axis(axes)

plt.plot(y,'-*',color='blue')

for n in range(nborders):
    t0=tileontimes[n]
    t1=tileofftimes[n]
    plt.plot([t0,t0],[axes[2],axes[3]],color=bordercolours[n],linewidth=2.1,linestyle='dashed')
    plt.plot([t1,t1],[axes[2],axes[3]],color=bordercolours[n],linewidth=2.1,linestyle='dashed')

outputfile=filename+'.'+outputformat
ret=plt.savefig(outputfile,dpi=600,format=outputformat)
plt.show()

# next plot: show the drift scan
fig=sxA.openPlot(ttl2,filename2,subttl=None)
ax1 = fig.add_subplot(111)
ax1.set_xlabel('time / seconds since '+str(sxA.startTime())+' UT')
ax1.set_ylabel('Power / arbitrary linear units')
#axes=[150,sxA.nData(),7e6,2.2e9]
axes=[650,sxA.nData(),7e6,2.2e9]

ax1.axis(axes)
for n in range(16):
    y=10**(sxA.Timeline(n=n)/10.)
    plt.plot(y)

outputfile=filename2+'.'+outputformat
ret=plt.savefig(outputfile,dpi=300,format=outputformat)
ret=plt.savefig(filename2+'.png',dpi=100,format='png')
plt.show()
    
