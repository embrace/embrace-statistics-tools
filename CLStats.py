"""
$Id: CLStats.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Fri Mar 30 12:19:18 CEST 2012

python class to read and plot EMBRACE Crosslet Statistics
this class is called from the generic class embraceStats
it contains methods specific to Crosslet Statistics stuff

$modified: Thu 02 Aug 2012 12:42:35 CEST
  method verify_n() : dummy method to be compatible with embraceStats

$modified: Fri 22 Feb 2013 13:53:39 CET
  method getSBnum()

$modified: Wed 27 Feb 2013 15:47:54 CET
  SBnum() : returns the subband number for a crosslet stats at a given time
  UV() : returns a visibility matrix for a given time

$modified: Mon 04 Mar 2013 16:19:19 CET
  Freq() : returns the frequency of the selected time slice
  
  skymap : working on it... (adapted from matlab script by SJW)

$modified: Mon Mar 11 17:28:27 CET 2013
  more progress on skymap
  added Data() for compatibility with embraceStats (sort of)

$modified: Tue 12 Mar 2013 11:46:36 CET
  skymap() : adding calibration and saving calibration matrix
             in self.PhaseCorrectionMatrix

  Timeline() : power given by 
               the sqrt of the sum of the squares of the diagonal of the acm

$modified: Fri 15 Mar 2013 16:50:44 CET
  skymap() : bug in rfwidth... needs to be in radians
  plotData() : adding vmin,vmax to change colour scaling

$modified: Tue 19 Mar 2013 11:15:39 CET
  skymap() : it looks like I finally got it.

$modified: Wed 20 Mar 2013 17:01:25 CET
  TotalPowerTimeline() : replaces the previous Timeline()
  BeamletTimeline() : for homemade bsx
  Timeline() : with an argument to choose which type of timeline to generate

$modified: Tue 26 Mar 2013 18:20:29 CET
  added label for plots to indicate subband number used for calibration

$modified: Sun 31 Mar 2013 09:46:33 CEST
  skymap(), BeamletTimeline() : adding Henrik's correction for the wobble

$modified: Tue 02 Apr 2013 20:24:56 CEST
  plotData() : can specify sbnum to override what's recorded at ntime (sometimes it's -1)

$modified: Fri 19 Apr 2013 12:38:12 CEST
  clearcal() : clear the PhaseCorrectionMatrix and the calculated beamlet timeline

$modified: Tue 09 Jul 2013 18:14:39 CEST
  BeamletTimeline() : pickle timeline 

$modified: Wed 11 Sep 2013 18:17:54 CEST
  BeamletTimeline() : option to ignore pointing and calculate on boresight

$modified: Wed 02 Oct 2013 15:50:15 CEST
  calibrated_ntime : so that we can label plots with the calibration time
  calibrated() : return whether phase has been calibrated

$modified: Thu 03 Oct 2013 16:27:55 CEST
  PhaseVariationTimeline() : phase difference between tilesets

$modified: Fri 11 Oct 2013 12:08:57 CEST
  PhaseVariationTimeline() : option to use a reference tileset which is not #0

$modified: Tue 29 Oct 2013 11:16:08 CET
  ignorepointing : fixed error.  zenith is at (0,pi/2) and not (0,0) !!

$modified: Thu 14 Nov 2013 17:08:34 CET
  option to plot timeline in linear instead of dBm

$modified: Mon 07 Apr 2014 12:37:10 CEST
  clearcal() : bug fix: added emptysky=None

$modified: Mon 10 Apr 2014 15:46:10 CEST
  skymap() : default calibrate = False is now calibrate = None (+ corresponding changes in functions calling skymap() and in embraceStats)
  	     calibrate==True case is now calibrate == 'normal'

$modified: Mon 10 Apr 2014 18:28:10 CEST
  find_redundant_baselines(): new function for preparing redundancy calibration (find redundant baselines and group them wrt unique visibilities in UV space)

$modified: Mon 11 Apr 2014 15:23:10 CEST
  redundancy_cal(): new function for doing a nonlinear least square based redundancy calibration. Integration to skymap with the keyword calibrate = 'redundancy'. (other choices: calibrate=None [default] or 'normal' )
  Calibration obviously works, but source positioning after redundancy cal still has to be tested.

$modified: Mon 14 Apr 2014 14:45:23 CEST
  mkPlotTitle() : fixed bug where calibration status is determined *after* plot title is generated.
"""

# import all the packages we will need
import os
import datetime as dt
import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import axes3d
import pyfits as fits
import numpy as np
import re
from tools_embraceStats import *
from math import *
import cmath
import pickle as PyPickle

i=complex(0,1)

class CLStats:
    """
    $Id: CLStats.py
    $auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
    $created: Fri Mar 30 12:19:18 CEST 2012

    python class to read and plot EMBRACE Crosslet Statistics
    this class is called from the generic class embraceStats
    it contains methods specific to Crosslet Statistics stuff

    version: Wed 02 Oct 2013 15:50:02 CEST
    """

    # constructor, must pass the metainfo dictionary at creation
    def __init__(self,metainfo):
        self.metainfo=metainfo
        self.PhaseCorrectionMatrix=None
        self.emptysky=None
        self.power_timeline=None
        self.beamlet_timeline=None
        self.phase_timeline=None
        self.ntime_timeline=None
        self.calibratedSBnum=None
        self.calibrated_ntime=None

        # try to read pickles
        pickname=self.metainfo['filename'].replace('.fits','')+'.beamlet-timeline.pickle'
        if os.path.exists(pickname):
            print 'reading pickle file: '+pickname
            ipick=open(pickname,'r')
            self.beamlet_timeline=PyPickle.load(ipick)
            self.ntime_timeline=PyPickle.load(ipick)
            ipick.close()

        pickname=self.metainfo['filename'].replace('.fits','')+'.phase-timeline.pickle'
        if os.path.exists(pickname):
            print 'reading pickle file: '+pickname
            ipick=open(pickname,'r')
            self.phase_timeline=PyPickle.load(ipick)
            self.ntime_timeline=PyPickle.load(ipick)
            ipick.close()
        
        self.power_timeline=unpickle(self.metainfo,'timeline')
        
        return

        
    # destructor
    def __del__(self):
        return

    # verify if n is okay... no real meaning yet for crosslet statistics
    def verify_n(self,n):
        return n

    # return the subband number corresponding to a frequency
    def subbandNumber(self,freq0):
        # the RF centre frequency is in the middle of the 256th counting from 0
        subband0=self.metainfo['RFcentre'] - (self.metainfo['chanwidth']*256)

        f_prev=subband0
        for n in range(1,513):
            f=subband0+n*self.metainfo['chanwidth']
            if freq0 >= f_prev and freq0 <= f:
                return n-1
            f_prev=f

        print "subband number not found!"
        return None

    # phase variation timeline for a given subband, for each crosslet pair
    # keywords bwidth,n, and direction are here only for compatibility with embraceStats()
    def CalculateTimeline(self,freq0,bwidth=0,n=0,direction='X'):
        subband=self.subbandNumber(freq0)
        if subband==None: return
        print "Timeline for subband: ",subband

        # create empty np array to hold crosslet phase info
        X=np.empty((self.metainfo['nRCU'],self.metainfo['nRCU']))

        # create new list to hold timeline data
        xphase=[]

        # fill the array
        n=-1
        for hdu in range(self.metainfo['firstHDU'],len(self.metainfo['hdulist'])):
            sbnum=self.metainfo['hdulist'][hdu].header['SBNUM']
            if isinstance(sbnum,str): sbnum=eval(sbnum)
            if sbnum==subband:
                n+=1
                # go through the crosslets
                for i in range(0,self.metainfo['nRCU']):
                    crosslets=self.metainfo['hdulist'][hdu].data.field(i)
                    for j in range(0,self.metainfo['nRCU']):
                        X[i,j]=atan2(crosslets[j].imag,crosslets[j].real)
                xphase.append(X)
                
        return xphase


    # return the subband number for a given crosslet time dump
    # note that ntime is the ntime_index from embraceStats (ie. offset from firstHDU)
    def SBnum(self,ntime_index):
        nstr=self.metainfo['hdulist'][ntime_index].header['SBNUM']
        if isinstance(nstr,str):
            n=eval(nstr)
        else:
            n=nstr
        if n==65535: 
            n=-1
        return n


    # get the subband number for all time dumps of crosslets
    def getSBnum(self):
        sbnum=[]
        for i in range (self.metainfo['ndata']):
            ntime_index=i+self.metainfo['firstHDU']
            sbnum.append(self.SBnum(ntime_index))
        return sbnum
            

    # return frequency of a slice at a given time
    # note that ntime is the ntime_index from embraceStats (ie. offset from firstHDU)
    # if sbnum is specifically given, then it overrides the sbnum recorded in the FITS file
    def Freq(self,ntime_index=None,sbnum=None):
        if ntime_index==None:ntime_index=self.metainfo['firstHDU']
        # start freq is the offset from the beginning of the RF band
        # but we don't need that info, we use the subband select map instead
        # knowing the RF centre, and that the centre is at subband#256 (numbered from 0)
        # (see also the SBStats frequency scale from FITS CRPIX1 etc)
        rfcentre=self.metainfo['RFcentre']
        # the RF centre frequency is in the middle of the 256th counting from 0
        subband0=rfcentre-(self.metainfo['chanwidth']*256)

        if sbnum==None:sbnum=self.SBnum(ntime_index)
        return subband0+sbnum*self.metainfo['chanwidth']

    
    # return a visibility matrix for a given time
    # note that ntime is the ntime_index from embraceStats (ie. offset from firstHDU)
    def UV(self,ntime_index):
        ntilesets=self.metainfo['nRCU']
        uv=np.empty((ntilesets,ntilesets),dtype=complex)
        for n in self.metainfo['tilesets']:
            for m in self.metainfo['tilesets']:
                uv[n,m]=self.metainfo['hdulist'][ntime_index].data.field(n)[m]

        return uv
    
     
    def find_redundant_baselines(self,Darray,plot_baselines = False):
    	"""
	Find redundant baselines and group them for preparing a redundancy calibration.
	Inputs:
		Darray: the array is made of Darray x Darray antennas (default to Darray = sqrt(nRCU) in embraceStats, implying it's a square array)
		plot: False [default] or True for plotting every different redundant baselines groups on 2D graphs of the array
	Outputs:
		Darray
		n_baseline_redundant_nonunique: total number of redundant baselines
		i_baseline_redundant : only unique visibilities indices, for visibilities that have redundant baselines
		baseline_elements_index : list of indices [i,j] corresponding to two antennas (i.e. between 0 and 15 for tilesets) for each baselines
		XX: meshgrid with x positions of antenna (tilesets)
		YY: same for y position   
    	"""

	import time
	t0 = time.time()
	
	x=np.linspace(0,-Darray+1,Darray) # coordinates for the antenna: fake units, we only care about the global geometry
	y=np.linspace(0,-Darray+1,Darray)

	X,Y=np.meshgrid(x,y)
	XX=np.reshape(X,np.size(X))
	YY=np.reshape(Y,np.size(Y))


	M=[]
	baseline_elements_index=[] # list of lists of 2 indexes corresponding to the two elements indexes of the given baseline (yeah, that's not clear..)
	
	for i in range(np.size(XX,0)):
		for j in range(np.size(XX,0)):
			if j<i:
				M.append([float(XX[i]-XX[j]),float(YY[i]-YY[j])])# each element of the list is a list of 2 numbers representing the baseline vector (ie with same direction as the baseline, and with a norm equal to the distance between the 2 antenna of the baseline). It is used to identify baselines that are actually redundant, i.e. sampling the same visibility in UV space.
				
			
			else:
				M.append([0,0]) # to prevent storing (i,j) AND (j,i) , I put a [0,0] baseline vector for j>=i
		
			baseline_elements_index.append([i,j]) #store indexes of the two antennas for the given baseline

	MM=[] 
	[MM.append(item) for item in M if not item in MM] # no duplicate in MM => unique baselines vector (except for the fake [0,0] at this stage), each one corresponding to a given visibility in UV space

	i_baseline=[]
	list_temp=[]
	for baseline in MM:
		temp=-1
		while temp != []:
			temp_old=temp
			try:
				temp=M.index(baseline,temp_old+1,np.size(M,0))
				list_temp.append(temp)
			except:
				temp=[]
	
		i_baseline.append(list_temp) # i_baseline is a list of lists, each list being a list of baseline indices for a given unique visibility
		list_temp=[]
	
	i_baseline.pop(0) # remove the [0,0] special case in i_baseline
	
	
		
	# i_baseline_redundant: only unique visibilities indices, for visibilities that have redundant baselines
	n_baseline_redundant=0 # number of unique visibilities with redundant baselines
	n_baseline_redundant_nonunique=0 # total number of redundant baselines
	i_baseline_redundant=[]
	for index in i_baseline:
		if len(index)>1:
			i_baseline_redundant.append(index)
			n_baseline_redundant+=1
			n_baseline_redundant_nonunique+=len(index)


	## plot redundant baselines for each visibility? ##
	if plot_baselines:
		
		color_cycle=['b+-','r+-','g+-','m+-','c+-','k+-','y','b+--','r+--','g+--','m+--','c+--','k+--','y+--','b+-.','+r-.','+g-.','m+-.','c+-.','k+-.','y+-.']
		icolor=0
			
		for index in i_baseline_redundant:
			plt.figure()
			plt.hold('True')
			plt.plot(XX,YY,'bo')
			for sub_index in index:
					
					Xtemp1=XX[baseline_elements_index[sub_index][0]]
					Ytemp1=YY[baseline_elements_index[sub_index][0]]
					Xtemp2=XX[baseline_elements_index[sub_index][1]]
					Ytemp2=YY[baseline_elements_index[sub_index][1]]
					
					plt.plot([Xtemp1,Xtemp2],[Ytemp1,Ytemp2],color_cycle[icolor],markersize=10,markeredgewidth=2)
					plt.xlim((-1.2*Darray,0.2*Darray))
					plt.ylim((-1.2*Darray,0.2*Darray))
			icolor+=1
			if icolor>=len(color_cycle):
				icolor=0
	
	t1 = time.time()
	
	print "Redundant baselines computed in "+str(t1-t0)+" s \n" 
			
    	return Darray, n_baseline_redundant_nonunique, n_baseline_redundant, i_baseline_redundant, baseline_elements_index, XX, YY
    
    
    

    # redundancy calibration function
    def redundancy_cal(self,acm, Darray,n_baseline_redundant_nonunique, n_baseline_redundant, i_baseline_redundant, baseline_elements_index, XX, YY, fit_res=None,weight=None,w=None):
    	
    	from scipy import optimize
	import time
	
	t0=time.time()
	
	i=0
	k=0
	PHI_obs=np.zeros((2*n_baseline_redundant_nonunique))
	
	cmap = acm # crosslet matrix
	
	for index in i_baseline_redundant:
		for sub_index in index:
			[i1,i2]=baseline_elements_index[sub_index]
			PHI_obs[2*i]=np.real(cmap[i1,i2])
			PHI_obs[2*i+1]=np.imag(cmap[i1,i2])
			i+=1
		k+=1
	
	# model function		
	def model(params,weight):
		out=np.zeros((2*n_baseline_redundant_nonunique))
		jj=complex(0,1)
		k=0
		i=0
		for index in i_baseline_redundant:
			PHI_tru=params[2*Darray**2+2*k]+jj*params[2*Darray**2+2*k+1]
			for sub_index in index:
				[i1,i2]=baseline_elements_index[sub_index]
				if weight != None: # if 'weight' is given as input, fix the model to be used (default:  *point source* at *center of the field*, see self.skymap())
					PHI_tru=abs(params[2*Darray**2+2*k]+jj*params[2*Darray**2+2*k+1])*np.conjugate(weight[i1,i2]) # constraints the true visibilities to be at given phase according to the 'weight' matrix
				
				g1=params[2*i1]+jj*params[2*i1+1]# complex gain for element indexed as i1
				g2=params[2*i2]+jj*params[2*i2+1]# complex gain for element indexed as i2
				out[2*i]=np.real(g1*np.conjugate(g2)*PHI_tru)
				out[2*i+1]=np.imag(g1*np.conjugate(g2)*PHI_tru)
				i+=1
			k+=1
		return out
		
	def Jmodel(params): #Jacobian of model() for optimize.leasq()
		# Jacobian is a mxn matrix where m = number of model() outputs, n = number of model params
		# Jacobian(output_i,param_j) = Doutput_j/Dparam_j
		# !! NO POINT SOURCE MODEL HERE !! (unlike in model())
		out=np.zeros((2*n_baseline_redundant_nonunique,len(params)))
		jj=complex(0,1)
		k=0
		i=0
		
		for index in i_baseline_redundant:
			PHI_tru=params[2*Darray**2+2*k]+jj*params[2*Darray**2+2*k+1]
			
			for sub_index in index:
				[i1,i2]=baseline_elements_index[sub_index]
				
				g1=params[2*i1]+jj*params[2*i1+1]
				g2=params[2*i2]+jj*params[2*i2+1]
				
				out[2*i,2*i1] = np.real(np.conjugate(g2)*PHI_tru)
				out[2*i,2*i1+1] = np.real(jj*np.conjugate(g2)*PHI_tru)
								
				out[2*i,2*i2] = np.real(g1*PHI_tru)
				out[2*i,2*i2+1] = np.real(g1*np.conjugate(jj)*PHI_tru)
				
				out[2*i,2*Darray**2+2*k]=np.real(g1*np.conjugate(g2))
				out[2*i,2*Darray**2+2*k+1]=np.real(g1*np.conjugate(g2)*jj)
				
				out[2*i+1,2*i1] = np.imag(np.conjugate(g2)*PHI_tru)
				out[2*i+1,2*i1+1] = np.imag(jj*np.conjugate(g2)*PHI_tru)
				
				out[2*i+1,2*i2] = np.imag(g1*PHI_tru)
				out[2*i+1,2*i2+1] = np.imag(g1*np.conjugate(jj)*PHI_tru)
				
				out[2*i+1,2*Darray**2+2*k] = np.imag(g1*np.conjugate(g2))
				out[2*i+1,2*Darray**2+2*k+1] = np.imag(g1*np.conjugate(g2)*jj)
				
				i+=1
			k+=1
				
		return out
		
	# objective function to be minimized in the nonliner leastsquare process
	def objective_f3(params,ydata,weight):
		return abs(ydata)*(ydata-model(params,weight)) # for optimize.leastsq(): classical leastquare cost function weighted by the absolute value of the data (quite empirical but best results so far, could be still better by weighting with S/N instead of S only) 
	
	# Jacobian of objective function
	def Jobjective_f3(params,ydata,weight):
		return -np.dot(np.diag(abs(ydata)),Jmodel(params)) # ydata not used, seems to work well anyway (~computing time/20!)..
	
	jj = complex(0,1)
	
	if fit_res != None: # previous results exist, take it as guess
		guess=fit_res[0]
	else: # previous result don't exist, take predefine guess
		guess=np.zeros(2*Darray**2+2*n_baseline_redundant)
		guess[::2]=1
#		A = np.random.rand(len(guess)/2)
#		phi = np.random.rand(len(guess)/2)*2*pi
#		guess[::2] = np.real(A * np.exp(jj*phi))
#		guess[1::2] = np.imag(A * np.exp(jj*phi))
		if weight != None: # user gave a weight matrix corresponding to weighting of tilesets for the given az,el pointing => guess defined for a point source at az,el
			print "\n redundancy cal with a model constraints (initial guesses + model fixed by 'weight' matrix)\n"
			kk=0
			for index in i_baseline_redundant:
				
				for sub_index in [index[0]]:
					[i1,i2]=baseline_elements_index[sub_index]
		
					guess[2*Darray**2+2*kk]=np.real(weight[i1,i2])
					guess[2*Darray**2+2*kk+1]=-np.imag(weight[i1,i2])
				kk+=1
		else: # no 'weight' => arbitrary initial guesses (see above) + no model constraints
			print "\n redundancy cal without a model constraints, arbitrary initial guesses fixed inside CLStats:redundancy_cal() code \n"
		if w != None: # vector w with complex gain of elements
			guess[::2][0:Darray**2] = np.real(w[:,0])
			guess[1::2][0:Darray**2] = -np.imag(w[:,0])
		
	# do the nonlinear optimization (optimize.leastsq(): Levenberg-Marquardt algorithm 'lmdif', no constraints)
	fit_res=optimize.leastsq(objective_f3,guess,args=(PHI_obs,weight),full_output=1,factor=100,Dfun=Jobjective_f3)
	fit_res=optimize.leastsq(objective_f3,fit_res[0],args=(PHI_obs,weight),full_output=1,factor=100,Dfun=Jobjective_f3)
	
	#import lmfit # for a possible future implementation: levenberg marquardt WITH constraints
	#lmfit.optimize()
	
	print "### Redundancy calibration: results of the fit: \n"
	print(fit_res[3]+"\n")
	
	
	
	#### Here there was some more functions definition for defining constraints on the nonlinear optimization process + some note about simulation results with different optimization methods
	#### I removed them, see python_func/redundancy/find_redundant_baselines_func.py to see them	
	#### in the end this optimization under constraints didn't work well and was complicated, I replaced it with the following hard coded and a posteriori constraints (product(gain)=0, sum(phase)=0, sum(phase*X)=0, sum(phase*Y)=0):
	
		
	
	atemp=0#'x'
	gain_out=np.sqrt(fit_res[atemp][::2]**2+fit_res[atemp][1::2]**2)[0:Darray**2]
	theta_out=np.unwrap(np.angle(fit_res[atemp][::2]+jj*fit_res[atemp][1::2])[0:Darray**2])
	chi2_gain=0
	chi2_theta=0
	eps_gain=np.array(1)
	
	# set product(gain)=1
	gain_out=gain_out/np.product(gain_out)**(1.0/Darray**2)
	
	
	#### EDIT 18/04/2014: I made tests again with phase constraints ON, it actually degrades the pointing.. (I have better results now that guesses are defined based on normal calib + a Jacobian has been added)
	#### EDIT 16/04/2014: I switched off these phase constraints, I don't understand why yet but they are not sufficient for a good pointing (source in the middle of FOV). Instead, if a 'weight' matrix is given to redundancy_cal, it will insert it in the model so that the point source is indeed at the middle of the FOV
	# Remove a linear dependance of phase over (XX,YY) (positions of the tilesets), so that sum(phase)=sum(XX*phase)=sum(YY*phase)=0
	
#	SX=np.sum(XX)
#	SY=np.sum(YY)
#	SXY=np.sum(XX*YY)
#	SX2=np.sum(XX**2)
#	SY2=np.sum(YY**2)
#	SP=np.sum(theta_out)
#	SXP=np.sum(XX*theta_out)
#	SYP=np.sum(YY*theta_out)
#	
#	alpha=SXY-SX*SY/Darray**2
#	beta1=SY2-SY**2/Darray**2
#	beta2=SX2-SX**2/Darray**2
#	gama1=-SYP+SY*SP/Darray**2
#	gama2=-SXP+SX*SP/Darray**2
#	
#	a=(gama2-(gama1*alpha)/beta1)/(alpha**2/beta1-beta2)
#	
#	b=((beta2/beta1)*gama1-(alpha/beta1)*gama2)/(alpha**2/beta1-beta2)
#	
#	c=(SP-a*SX-b*SY)/Darray**2
#	
#	theta_out=theta_out-a*XX-b*YY-c
	
	#############################################################################################
	
	# make a gain and phase correction matrix
	Mcorr = np.zeros((Darray**2,Darray**2),dtype=complex)
	for i in range(Darray**2):
		for j in range(Darray**2):
			Mcorr[i,j] = (1/gain_out[i])*(1/gain_out[j])*np.exp(jj*(-theta_out[i]+theta_out[j]))#

			
	# todo: compare calibrated beam with square aperture model (beam more symmetric with gain correction => closer to the model?)
	
	t1 = time.time()
	
	print "Redundancy cal computed in "+str(t1-t0)+" s\n"
	
	return Mcorr, theta_out, gain_out
        
    
    
    # return a sky map from a correlation matrix
    # adapted from the matlab script by S.J. Wijnholds acm2skymap.m
    #################################################################
    # skymap = acm2skyimag(acm, xpos, ypos, freq, l, m)
    #
    # conversion of ACM to sky image
    #
    # arguments
    # acm    : nelem x nelem x nchannel array correlation matrix
    # xpos   : x-position of antennas (vector of length nelem)
    # ypos   : y-position of antennas (vector of length nelem)
    # freq   : frequencies in Hz (vector of length nchannel)
    # l, m   : l and m points to which to direct the array (vectors)
    #
    # return value
    # skymap : length(l) x length(m) x nchannel matrix containing the
    #          resulting sky maps
    #
    # SJW, 2004
    #
    # modified April 19, 2005 by SJW: correct coordinate conventions
    # modified July 20, 2006 by SJW: optimization for release in LOFAR package
    ######################################################################
    # note that ntime is the ntime_index from embraceStats (ie. offset from firstHDU)
    # calibrate = None [default], 'normal' or 'redundancy'
    def skymap(self,ntime_index,az,el,calibrate=None,npix=101,fov=1.5,sbnum=None,simulate=False):
	
        i=complex(0,1)
	
	
	if simulate==True:
	# simulate a point source at the centre of the FOV
		print "!! Simulating a point source at centre of FOV !!"
		freq=self.Freq(ntime_index,sbnum) # frequency in MHz
        	wvln=speedolight/freq # speed of light given in m MHz
        	k = 2 * pi / wvln
		rfwidth=radians(tilesetBeamSize(freq))		
			
		xpos=np.array([self.metainfo['tileset_positions'][:,0]]).T
		ypos=np.array([self.metainfo['tileset_positions'][:,1]]).T

		# l,m are the direction cosines on the sky
		l=np.empty(npix,dtype=float)
		m=np.empty(npix,dtype=float)
		r=cos(el)
		l0=r*sin(az)
		m0=r*cos(az)
		for pix in range(npix):
		    offset=fov*rfwidth*(-0.5 + float(pix)/npix)
		    l[pix]=l0 + offset
		    m[pix]=m0 + offset
	
		#1st point source, middle of FOV
		wx0 = np.exp(-i * k * xpos * l0)
		wy0 = np.exp(-i * k * ypos * m0)
		w=wx0*wy0

############### Remnant of tests with two simulated sources, I let it here for possible future use #B.C. 2014/07/30		
#		#2nd point source, away from centre of FOV
#		az2 = 0#az + 4.5/180*pi
#		el2 = pi/2#el + 0/180*pi
#		
#		r2=cos(el2)
#		l02=r2*sin(az2)
#		m02=r2*cos(az2)
#		
#		wx02 = np.exp(-i * k * xpos * l02)
#		wy02 = np.exp(-i * k * ypos * m02)
#		w2=0*(wx0)*(wy0)+(wx02)*(wy02)
#		weight2=np.dot(np.conj(w) , w.T)+np.dot(np.conj(w2) , w2.T)
#		
#		acm_simu=np.conj(np.dot(np.conj(w) , w.T))+np.conjugate(np.dot(np.conj(w2) , w2.T))

		acm_simu=np.conj(np.dot(np.conj(w) , w.T))
		
		gain_rand = 1+0*np.random.randn(np.size(acm_simu,0),1) # I chose an arbitrary amplitude for both amplitude and phase noise, to be set here in the code if needed
		phase_rand = 0*pi*np.random.randn(np.size(acm_simu,0),1)
		
		Mnoise = np.zeros(acm_simu.shape,dtype=complex)
		
		for ii in range(gain_rand.size):
			for jj in range(gain_rand.size):
				Mnoise[ii,jj] = gain_rand[ii,0]*gain_rand[jj,0]*np.exp(i*(phase_rand[ii,0]-phase_rand[jj,0]))
		
		acm_simu = acm_simu * Mnoise
		acm = acm_simu
	
	else: # do NOT simulate a point source
		acm=self.UV(ntime_index)
		kk=0
		for k in range(0,1):
			acm = (acm*kk+self.UV(ntime_index+k))/(kk+1)
			kk+=1
		
        # apply Henrik's correction for the wobble
        if self.emptysky!=None:
            acm = acm - self.emptysky

        freq=self.Freq(ntime_index,sbnum) # frequency in MHz
        wvln=speedolight/freq # speed of light given in m MHz
        k = 2 * pi / wvln

        rfwidth=radians(tilesetBeamSize(freq))
        
        ntilesets=self.metainfo['nRCU']

        skymap=np.empty((npix,npix),dtype=complex)

        # xpos, and xypos as column vectors
        xpos=np.array([self.metainfo['tileset_positions'][:,0]]).T
        ypos=np.array([self.metainfo['tileset_positions'][:,1]]).T

        # l,m are the direction cosines on the sky
        l=np.empty(npix,dtype=float)
        m=np.empty(npix,dtype=float)
        r=cos(el)
        l0=r*sin(az)
        m0=r*cos(az)
        for pix in range(npix):
            offset=fov*rfwidth*(-0.5 + float(pix)/npix)
            l[pix]=l0 + offset
            m[pix]=m0 + offset

        #length of l and m
        len_l=len(l)
        len_m=len(m)

        # l, and m as row vectors
        l=np.array([l])
        m=np.array([m])

        # matrix multiplication of n-element column vector and m-element row vector
        # gives matrix of size nxm
        wx = np.exp(-i * k * np.dot(xpos,l.conj()))
        wy = np.exp(-i * k * np.dot(ypos,m.conj()))

        if calibrate == 'normal': # normal calibration: expect a point source at the middle of the field and derive the correponding PhaseCorrectionMatrix
            print "\n Applying \'normal\' Calibration \n"
            wx0 = np.exp(-i * k * xpos * l0)
            wy0 = np.exp(-i * k * ypos * m0)
            w=wx0*wy0
            weight=np.dot(np.conj(w) , w.T)
            self.weight=np.copy(weight) # for comparison later with Henrik
            self.w=np.copy(w)
            phasecorrector = -np.angle(weight)-np.angle(acm)
            self.PhaseCorrectionMatrix = np.exp(i*phasecorrector)
            self.calibratedSBnum=self.SBnum(ntime_index)
            self.calibrated_ntime=ntime_index-self.metainfo['firstHDU']
            self.calibrated_method='normal'
	
	if calibrate == 'redundancy': # redundancy calibration: group baselines by visibility groups (1 visibility group = several baselines measuring the same point in U,V space), derive and solve the corresponding equation system which gives phase and gain correction to be applied. 'weight' is given as an input, it constraints the redundancy model for a point source at middle of the FOV, so that the pointing is OK.
		print "\n Applying \'redundancy\' Calibration \n"
		
		wx0 = np.exp(-i * k * xpos * l0)
            	wy0 = np.exp(-i * k * ypos * m0)
            	w=wx0*wy0
            	weight=np.dot(np.conj(w) , w.T)
		
		self.weight=np.copy(weight)
		self.w=np.copy(w)
		if simulate == True:
			self.weight2=np.copy(weight2)
		
		Darray = int(np.sqrt(len(self.metainfo['tilesets'])))
		#compute redundant baselines groups
		Darray, n_baseline_redundant_nonunique, n_baseline_redundant, i_baseline_redundant, baseline_elements_index, XX, YY = self.find_redundant_baselines(Darray)
		# Do the redundnat calibration # here if weight = None, no constraints, else the model used is based on 'weight' (see initial guesses and model in self.redundancy_cal())
		Mcorr, theta_calib, gain_calib=self.redundancy_cal(acm,Darray, n_baseline_redundant_nonunique, n_baseline_redundant, i_baseline_redundant, baseline_elements_index, XX, YY,weight=weight,w=None)#(gain_rand*np.exp(i*phase_rand)))	
		
		self.PhaseCorrectionMatrix = Mcorr
		self.calibratedSBnum=self.SBnum(ntime_index)
                self.calibrated_ntime=ntime_index-self.metainfo['firstHDU']
		self.calibrated_method='redundancy'
		self.theta_calib = theta_calib
		self.gain_calib = gain_calib
	
        if self.PhaseCorrectionMatrix==None:
            calib_acm = acm
        else:
            calib_acm = acm * self.PhaseCorrectionMatrix # element-wise

        for lidx in range(len_l):
            for midx in range(len_m):
                weight_vector = wx[:, lidx] * wy[:, midx] # element-wise multiplication
                skymap[lidx][midx] = np.dot(np.dot(weight_vector.conj(),calib_acm), weight_vector)


        return skymap

    # for (sort of) compatibility with calling from embraceStats
    def Data(self,ntime_index,az,el,calibrate=None,npix=101,fov=1.5,sbnum=None,simulate=False):
        return self.skymap(ntime_index,az,el,calibrate,npix,fov,sbnum,simulate)
                          
    # make the middle part of a title for a plot
    # n is ntime_index
    ###  direction argument is here for compatibility with the rest of embraceStats
    def mkPlotTitle(self,n=None,direction=None,calibrate=None):
        if calibrate==None:
            caltext=""
            filename='_crosslet'
        else:

            calibratedSBnum=self.SBnum(n)
            caltext="calibrated for subband #"+str(calibratedSBnum)+','
            filename='_calibrated-crosslet'+str(calibratedSBnum)
        if type(calibrate)==str:
            caltext+=' with calibration method: '+calibrate
            filename+='_'+calibrate

        if n!=None:
            ttl=' : '+caltext+' subband #'+str(self.SBnum(n))+' = '+str('%.2f' % self.Freq(n))+' MHz'
        else:
            ttl=' : '+caltext

        return ttl,filename

    def plotData(self,ntime_index,az,el,calibrate=None,pmin=None,pmax=None,npix=101,fov=1.5,sbnum=None,simulate=False):
        skymap=np.real(self.skymap(ntime_index,az,el,calibrate,npix,fov,sbnum,simulate))
        freq=self.Freq(ntime_index,sbnum) # frequency in MHz
        rfwidth=tilesetBeamSize(freq)
        width=fov*rfwidth 
        xtents=[-0.5*width,0.5*width,-0.5*width,0.5*width]
        plt.imshow(skymap,aspect='equal',
                   extent=xtents,
                   vmin=pmin,vmax=pmax)
        plt.xlabel('l / degrees')
        plt.ylabel('m / degrees')
        plt.colorbar()
        pointingtxt='RF pointing az='+str('%.2f el=%.2f' % (math.degrees(az),math.degrees(el)))
        plt.figtext(0.5,0.01,pointingtxt,horizontalalignment='center',fontsize=8)
        return True

    # calculate a timeline using the total power = norm of the diagonal of ACM
    def TotalPowerTimeline(self,linear=False):
        if self.power_timeline!=None:return self.power_timeline,range(self.metainfo['ndata'])

        ntilesets=self.metainfo['nRCU']
        if self.PhaseCorrectionMatrix==None:
            self.PhaseCorrectionMatrix=np.zeros((ntilesets,ntilesets),dtype=float)
        correctionMatrix=np.exp(i*self.PhaseCorrectionMatrix)

        ndata=self.metainfo['ndata']
        p=np.zeros(ndata,dtype=float)
        for n in range(ndata):
            ntime_index=n+self.metainfo['firstHDU']
            acm=self.UV(ntime_index)
            calib_acm = acm * correctionMatrix # element-wise
            p[n]=np.linalg.norm(calib_acm.diagonal())

        # keep timeline for replotting, because it takes a while to calculate
        if linear:
            self.power_timeline=p
        else:
            self.power_timeline=10*np.log10(p)
        pickle(self.metainfo,self.power_timeline,datatype='timeline')
        return self.power_timeline,range(self.metainfo['ndata'])

    # calculate a timeline at a digital pointing (ie. homemade bsx)
    # to have a calibrated version, first one must use self.Data(ntime,calibrate=some_method), where some_method='normal' or 'redundancy'
    def BeamletTimeline(self,digbeam=0,direction='x',
                        sbnum=256,ignore=False,ignorepointing=False,linear=False):
        if self.beamlet_timeline!=None:return self.beamlet_timeline,self.ntime_timeline

        i=complex(0,1)

        rfcentre=self.metainfo['RFcentre']
        # the RF centre frequency is in the middle of the 256th counting from 0
        subband0=rfcentre-(self.metainfo['chanwidth']*256)
        freq=subband0+sbnum*self.metainfo['chanwidth']
        wvln=speedolight/freq # speed of light given in m MHz
        k = 2 * pi / wvln

        rfwidth=radians(tilesetBeamSize(freq))
        ntilesets=self.metainfo['nRCU']

        # xpos, and xypos as column vectors
        xpos=np.array([self.metainfo['tileset_positions'][:,0]]).T
        ypos=np.array([self.metainfo['tileset_positions'][:,1]]).T

        firstHDU=self.metainfo['firstHDU']
        lastHDU=firstHDU+self.metainfo['ndata']

        timeline=[]
        ntimes=[]
        for ntime_index in range(firstHDU,lastHDU):
            ntime=ntime_index-firstHDU

            ### take only the requested subband... unless it's to be ignored
            if not ignore and self.SBnum(ntime_index)!=sbnum:
                #timeline.append(0.0)
                continue

            if ignorepointing:
                az,el=(0.0,pi/2)
            else:
                # get digital pointing (must use ntime and not ntime_index, see tools_embraceStats)
                az,el=pointing(self.metainfo,ntime,digbeam,beamtype='dig',direction=direction,quiet=True)
                if az==None:
                    #timeline.append(0.0)
                    continue
            
            # ntime 
            ntimes.append(ntime)


            # l,m are the direction cosines on the sky
            r=cos(el)
            l0=r*sin(az)
            m0=r*cos(az)
            
            # apply Henrik's correction for the wobble
            if self.emptysky==None:
                acm=self.UV(ntime_index)
            else:
                acm=self.UV(ntime_index) - self.emptysky

            wx0 = np.exp(-i * k * xpos * l0)
            wy0 = np.exp(-i * k * ypos * m0)
            w=wx0*wy0
            weight=np.dot(w.conj() , w.T) # see henrik's calcweights.m


            if self.PhaseCorrectionMatrix==None:
                calib_acm = acm
            else:
                calib_acm = acm * self.PhaseCorrectionMatrix # element-wise

            pix=np.real( np.sum(weight*calib_acm) )
            if linear:
                timeline.append(pix)
            else:
                if pix<=0.0:
                    timeline.append( -10 )
                else:
                    timeline.append( 10*np.log10(pix) )

        self.beamlet_timeline=np.array(timeline)
        self.ntime_timeline=np.array(ntimes)
        # create a pickle file
        pickname=self.metainfo['filename'].replace('.fits','')+'.beamlet-timeline.pickle'
        opick=open(pickname,'w')
        PyPickle.dump(self.beamlet_timeline,opick)
        PyPickle.dump(self.ntime_timeline,opick)
        opick.close()
        return self.beamlet_timeline,self.ntime_timeline

    # plot phase variation with time for each crosslet pair
    def PhaseVariationTimeline(self,sbnum=256,ignore=False,reference=None):
        if self.phase_timeline!=None:return self.phase_timeline,self.ntime_timeline
        if reference==None:reference=0

        i=complex(0,1)

        ntilesets=self.metainfo['nRCU']

        firstHDU=self.metainfo['firstHDU']
        lastHDU=firstHDU+self.metainfo['ndata']

        # first find the ntimes that we want
        ntimes=[]
        for ntime_index in range(firstHDU,lastHDU):
            ntime=ntime_index-firstHDU

            ### take only the requested subband... unless it's to be ignored
            if not ignore and self.SBnum(ntime_index)!=sbnum:
                #timeline.append(0.0)
                continue

            # ntime 
            ntimes.append(ntime)


        # now create an appropriate array and go through again with the desired ntimes
        ntimes=np.array(ntimes)
        timeline=np.zeros( (ntilesets,len(ntimes)) )
        index=0
        for ntime in ntimes:
            ntime_index=ntime+firstHDU

            # apply Henrik's correction for the wobble
            if self.emptysky==None:
                acm=self.UV(ntime_index)
            else:
                acm=self.UV(ntime_index) - self.emptysky

            if self.PhaseCorrectionMatrix==None:
                calib_acm = acm
            else:
                calib_acm = acm * self.PhaseCorrectionMatrix # element-wise

            for n in range(ntilesets):
                phi=degrees(cmath.phase( calib_acm[reference][n] ))
                timeline[n][index]=phi

            index+=1

        self.phase_timeline=timeline
        self.ntime_timeline=ntimes
        # create a pickle file
        pickname=self.metainfo['filename'].replace('.fits','')+'.phase-timeline.pickle'
        opick=open(pickname,'w')
        PyPickle.dump(self.phase_timeline,opick)
        PyPickle.dump(self.ntime_timeline,opick)
        opick.close()
        return self.phase_timeline,self.ntime_timeline

    # adjust phase to make a smooth curve for plotting
    def adjustPhase(self,p):

        adjusted_p=np.zeros( len(p) )
        
        prev_phi=p[0]
        offset=0.
        n=0
        for phi in p:

            delta = prev_phi - phi
            if delta < -180.0:
                offset-=360
            if delta > 180.0:
                offset+=360.0
            
            prev_phi=phi
            adjusted_p[n]=phi+offset
            n+=1
        

        return adjusted_p


    def Timeline(self,plot='BEAMLET',digbeam=0,direction='x',
                 sbnum=256,ignore=False,ignorepointing=False,reference=None,
                 linear=False):
        if plot.upper()=='BEAMLET':
            return self.BeamletTimeline(digbeam=digbeam,direction=direction,sbnum=sbnum,
                                        ignore=ignore,ignorepointing=ignorepointing,
                                        linear=linear)
        if plot.upper()=='PHASE':
            return self.PhaseVariationTimeline(sbnum,ignore,reference)

        return self.TotalPowerTimeline(linear)

    
    def plotTimeline(self,digbeam=0,direction='x',
                     sbnum=256,plot='total power',
                     ignore=False,ignorepointing=False,reference=None,
                     linear=False):
        ntilesets=self.metainfo['nRCU']
        if self.PhaseCorrectionMatrix!=None:
            caltxt=' calibrated at SB#'+str(self.calibratedSBnum)
        else: caltxt=''

        nstyles=len(self.metainfo['linestyles'])



        p,t=self.Timeline(plot,digbeam,direction,sbnum,ignore,ignorepointing,reference,linear)

        if plot.upper()=='PHASE':
            minval=-180
            maxval=180
            for n in range(ntilesets):
                style_index=n
                while style_index >= nstyles:
                    style_index-=nstyles
                newP=self.adjustPhase(p[n])
                if minval>min(newP):minval=min(newP)
                if maxval<max(newP):maxval=max(newP)
                plt.plot(t,newP,
                         label='tileset '+str(n),
                         linestyle=self.metainfo['linestyles'][style_index],
                         color=self.metainfo['colours'][style_index])
            return minval,maxval



        if plot.upper()=='BEAMLET':
            ttl='homemade beamlets from crosslets for SB#'+str(sbnum)+caltxt
        else:
            ttl='crosslets total power'
        minval=min(p)
        maxval=max(p)
        plt.plot(t,p,label=ttl)
        return minval,maxval

    def clearcal(self):        
        self.PhaseCorrectionMatrix=None
        self.beamlet_timeline=None
        self.phase_timeline=None
        self.ntime_timeline=None
        self.calibrated_ntime=None
        self.emptysky=None
        return None

    def calibrated(self):
        if self.PhaseCorrectionMatrix==None:
            return False
        return True


    ### still to come ...
