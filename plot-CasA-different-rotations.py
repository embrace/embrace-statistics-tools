"""
plot all the CasA timelines that were done to see the difference between
different assumed rotation angles of the array

$created: Wed May 30 11:28:57 CEST 2012

$modified: Wed Jun 20 15:25:30 CEST 2012
  added 129.5 rotation

$modified: Thu Jun 28 09:44:50 CEST 2012
  added 131.2857 rotation

$modified: Fri Jun 29 14:59:10 CEST 2012
  adding off-source plot

"""
import os

pngfile='CasA-different-rotation-angles.png'
pngoffsrc='CasA-offsource-different-rotation-angles.png'
rotangs=['039','128','129','129.5','130','131.2857','135','141']


# load files, if necessary
try:
    print "already loaded? filename=",cas['131.2857'].Filename()

except:
    datadir='/scu/data/obs'
    cwd=os.getcwd()
    os.chdir(datadir)
    cas={}
    cas['039']  =estats('CasA_BeamA_bsx_20120525-0602.fits')
    cas['128']  =estats('CasA_BeamA_bsx_20120529-0546.fits')
    cas['129']  =estats('CasA_BeamA_bsx_20120530-0542.fits')
    cas['129.5']=estats('CasA_BeamA_bsx_20120531-0538.fits')
    cas['131.2857']=estats('CasA_BeamA_bsx_20120627-0352.fits')
    cas['130']  =estats('CasA_BeamA_bsx_20120528-0550.fits')
    cas['135']  =estats('CasA_BeamA_bsx_20120526-0558.fits')
    cas['141']  =estats('CasA_BeamA_bsx_20120527-0554.fits')
    os.chdir(cwd)


# assign bandwidth, if necessary
try:
    if isinstance(bwidth,int) or isinstance(bwidth,float):
        print "using bandwidth: ",bwidth
        
except:
    bwidth=59*100.0/512

timeline={}
offsrc={}
maxvals=[]
minvals=[]
ndata=0
for rotang in rotangs:
    timeline[rotang]=cas[rotang].Timeline(1175.6,bwidth)
    offsrc[rotang]=cas[rotang].Timeline(1175.6,bwidth,direction='Y')
    maxvals.append(max(timeline[rotang]))
    minvals.append(min(timeline[rotang]))
    if ndata < cas[rotang].nData():ndata=cas[rotang].nData()



# min/max for plotting
maxval=max(maxvals)
minval=min(minvals)
try:
    if isinstance(pmin,int) or isinstance(pmin,float):
        print "min power: ",minval,' but using: ',pmin
        
except:
    pmin=minval

try:
    if isinstance(pmax,int) or isinstance(pmax,float):
        print "max power: ",maxval,' but using: ',pmax
except:
    pmax=maxval


fig=cas['039'].openPlot('Comparison of CasA tracking with different assumed array rotations',
                        'bogusfilename',
                        subttl='1175.6MHz +/- '+str("%5.2f" % (bwidth/2.0))+'MHz')

for rotang in rotangs:
    plt.plot(timeline[rotang],label='Assumed Rotation: '+rotang)

plt.xlabel('time / seconds')
plt.ylabel('integrated power / dBm')

cas['039'].pltObsModeTimes([pmin,pmax])

axes=[0,ndata,pmin,pmax]
plt.axis(axes)

plt.legend()
plt.savefig(pngfile,format='png',dpi=100)
plt.show()

### plot the off source tracking
fig=cas['039'].openPlot('Comparison of CasA/off-source tracking with different assumed array rotations',
                        'bogusfilename',
                        subttl='1175.6MHz +/- '+str("%5.2f" % (bwidth/2.0))+'MHz')

for rotang in rotangs:
    plt.plot(offsrc[rotang],label='Assumed Rotation: '+rotang)

plt.xlabel('time / seconds')
plt.ylabel('integrated power / dBm')

cas['039'].pltObsModeTimes([pmin,pmax])

axes=[0,ndata,pmin,pmax]
plt.axis(axes)

plt.legend()
plt.savefig(pngoffsrc,format='png',dpi=100)
plt.show()
