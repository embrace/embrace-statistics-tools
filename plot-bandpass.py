"""
$Id: plot-bandpass.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed 20 Feb 2013 16:33:28 CET

Plot bandpass data from ARTEMIS
the data file is first processed by the bandpass tool
e.g.
$ bandpass -t 10 filename.dat > bandpass.dat

it returns ascii list with two columns: freq, intensity
the spectra are separated by #START and #STOP line
"""

import os,numpy as np
def readBandpass(filename):
  if not os.path.exists(filename):
      print 'file not found: '+filename
      return None,None,None

  ifile=open(filename,'r')

  nspectra=0
  I=[]
  freq=[]

  line='start going'
  while line!="":
      line=ifile.readline()
      if line=="":break

      if line=="#START\n":
          this_freq=[]
          this_I=[]
          continue

      if line=="#STOP\n":
          nspectra+=1
          I.append(np.array(this_I))
          freq.append(np.array(this_freq))
          continue
      
      this_I.append(eval(line.split(' ')[1]))
      this_freq.append(eval(line.split(' ')[0]))
      
  ifile.close()
  return I,freq,nspectra

