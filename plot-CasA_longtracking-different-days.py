"""

$Id: plot-CasA_longtracking-different-days.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed 15 Aug 2012 13:18:13 CEST
$modified: Wed 15 Aug 2012 13:19:42 CEST
  copied from plot-CasA-different-days.py
   (last mod: Fri Jul 20 10:24:07 CEST 2012)

plot comparison of CasA long tracking

CasA tracking while visible (above 45deg horizon)
also plotted are the virtual CasA trackings

 - behindCasA
 - acceleratedCasA

$modified: Thu Aug 16 17:27:04 CEST 2012
  load data and delete stats objects to save memory

$modified: Mon 20 Aug 2012 17:30:44 CEST
  added data files from 2012-08-18,20
  and prepared for 2012-08-21

$modified: Wed 22 Aug 2012 11:00:26 CEST
  changed png format size to stretch out the view

$modified: Thu 24 Jan 2013 11:29:19 CET
  added Beam-B CasA longtracking from 2013-01-23

  loadData() must find the data file...
  many data files have been moved to archive on linnaeus
  accessible, if nfs mounted, at /data/archive/embrace-archive/2012

$modified: Mon 28 Jan 2013 17:34:45 CET
  removed Beam-B CasA longtracking from 2013-01-23
  the RF calibration was not correct

$modified: Wed 06 Feb 2013 17:26:25 CET
  CasA with both beam A and beam B on 2013-02-03

$modified: Mon 24 Jun 2013 10:27:16 CEST
  getObsModeTimes() becomes ObsModeTimes()

$modified: Fri 28 Jun 2013 16:27:42 CEST
  startTime becomes startTime()  

$modified: Thu 22 Oct 2015 10:48:41 CEST
  data file location changed on Linnaeus

$modified: Fri 23 Oct 2015 10:58:29 CEST
  plot only the four tracks selected by Henrik for the A&A paper
"""

import os,sys
import numpy as np

# assign selected days
try:
    for day in selected_days:
        print day
except:
    # plot only the four tracks selected by Henrik for the A&A paper
    selected_days=['2012-07-08 CasA','2012-07-25 behind CasA']
    
# assign bandwidth, if necessary
try:
    if isinstance(bwidth,int) or isinstance(bwidth,float):
        print "using bandwidth: ",bwidth
        
except:
    bwidth=55*100.0/512

# some stuff for pretty plots
linestyles=['-','-','-','-','-','-','-',
            '--','--','--','--','--','--','--',
            '-.','-.','-.','-.','-.','-.','-.']
colours=['k','b','g','r','c','m','#4b8db7',
         'k','b','g','r','c','m','#4b8db7',
         'k','b','g','r','c','m','#4b8db7']

def openPlot(ttl,bwidth):
#    figsize=(12.80,9.60)
    figsize=(3*12.80,10.24)
    subttl='1176.45MHz +/- '+str("%5.2fMHz" % (bwidth/2.0))
    fig=plt.figure(figsize=figsize)
    fig.canvas.set_window_title('plt: '+ttl)
    plt.title(ttl,fontsize=36)
    plt.annotate(subttl, fontsize=36, xy=(0.5, 0.95), xycoords='axes fraction')
    plt.xlabel('time / seconds ',fontsize=36)
    plt.ylabel('uncalibrated power / dB',fontsize=36)
    return fig

# load data and delete the estats object to save memory
def loadData(filename,bwidth):
    dirs=['/scu/data/obs/','/data/big/scudata/2012/','/data/big/scudata/2013/']
    for datadir in dirs:
        if os.path.exists(datadir+filename):
            break
    if not os.path.exists(datadir+filename):
        print 'ERROR: cannot find file '+filename
        sys.exit()
        return None, None, None 
        
    bx=estats(datadir+'/'+filename)
    timeline=bx.Timeline(1176.45,bwidth)
    offsrc=bx.Timeline(1176.45,bwidth,direction='y')
    startTimes=bx.ObsModeTimes()
    trackstart=startTimes['j2000trackstart']
    if len(trackstart)==0:trackstart=startTimes['trackstart']
    if len(trackstart) > 0:
        tshift=bx.tot_seconds(trackstart[0]-bx.startTime())
    else:
        tshift=0
    del bx
    return timeline,offsrc,tshift

# load files, if necessary
try:
    firstkey=timeline.keys()[0]
    print "... already loaded? "

except:
    xfactor={}
    tshift={}
    timeline={}
    offsrc={}

    day='2012-07-06 CasA'
    filename='GPSBIIF-2-CasA_BeamA_bsx_20120706-2349.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        xfactor[day]=1

    day='2012-07-08 CasA'
    filename='CasA_BeamA_bsx_20120708-2254.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        xfactor[day]=1

    #### this one failed
    #day='2012-07-21 behind CasA'
    #filename='behindCasA_BeamA_bsx_20120721-0107.fits'

    day='2012-07-25 behind CasA'
    filename='behindCasA_BeamA_bsx_20120725-0051.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        xfactor[day]=1

    #### this one failed
    #day='2012-07-28 behind CasA (Tobia mode)'
    #filename='behindCasA_BeamA_bsx_20120728-0040.fits'

    day='2012-07-29 behind CasA (Tobia mode)'
    filename='behindCasA_BeamA_bsx_20120729-0036.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        xfactor[day]=1

    day='2012-07-30 behind CasA (Tobia mode)'
    filename='behindCasA_BeamA_bsx_20120730-0032.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        xfactor[day]=1

    #### there was a bug in the acceleratedCasA_longtracking.py script
    #day='2012-08-14 20x CasA AZEL'
    #filename='GPSBIIF-1-CasA_BeamA_bsx_20120814-1240.fits'
    #if day in selected_days:
    #   timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
    #   xfactor[day]=20

    day='2012-08-14 CasA track by AZEL'
    filename='GPSBIIF-2-CasA_BeamA_bsx_20120814-2108.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        xfactor[day]=1

    #day='2012-08-15 10x CasA track by AZEL'
    #filename='GPSBIIF-2-CasA_BeamA_bsx_20120815-2104.fits'
    #if day in selected_days:
    #    timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
    #    xfactor[day]=10

    day='2012-08-17 CasA (after RSP conf mod)'
    filename='GPSBIIF-2-CasA_BeamA_bsx_20120817-1959.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        xfactor[day]=1

    #day='2012-08-20 4x CasA track by AZEL'
    #filename='GPSBIIF-1-CasA_BeamA_bsx_20120820-1137.fits'
    #if day in selected_days:
    #   timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
    #   xfactor[day]=4

    #day='2012-08-21 1x CasA track by AZEL'
    #filename='GPSBIIF-2-CasA_BeamA_bsx_20120820-2044.fits'
    #if day in selected_days:
    #   timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
    #   xfactor[day]=1

    day='2012-08-21 20x CasA track by AZEL'
    filename='GPSBIIF-1-CasA_BeamA_bsx_20120821-1225.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        xfactor[day]=20

    day='2012-08-21 100x CasA track by AZEL'
    filename='GPSBIIF-2-CasA_BeamA_bsx_20120821-2039.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        xfactor[day]=100

    #### RF cal was not correct
    #day='2013-01-23 CasA Beam-B'
    #filename='CasA_BeamB_bsx_20130123-0922.fits'
    #if day in selected_days:
    #   timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
    #   xfactor[day]=1

    day='2013-02-03 CasA'
    filename='CasA_BeamA_bsx_20130203-0839.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        xfactor[day]=1

    day='2013-02-03 CasA Beam-B (shifted 17.3dB)'
    filename='CasA_BeamB_bsx_20130203-0839.fits'
    if day in selected_days:
        timeline[day],offsrc[day],tshift[day]=loadData(filename,bwidth)
        timeline[day]=timeline[day]-17.3
        xfactor[day]=1

    #### some manual adjustments:  line up the curves at the end
    #### this corresponds to setting time 
    #### because trackings started at various times after rise time
    #### but all ended at setting time
    dayref='2012-07-08 CasA'
    day='2012-07-06 CasA'
    if day in selected_days:
        tshift[day]=len(timeline[day])-len(timeline[dayref])

    day='2012-08-14 CasA track by AZEL'
    if day in selected_days:
        tshift[day]=len(timeline[day])-len(timeline[dayref])

    day='2012-08-17 CasA (after RSP conf mod)'
    if day in selected_days:
        tshift[day]=len(timeline[day])-len(timeline[dayref])

    #day='2012-08-20 4x CasA track by AZEL'
    #tshift[day]=len(timeline[day])-(len(timeline[dayref])/xfactor[day])

    #day='2012-08-21 1x CasA track by AZEL'
    #tshift[day]=len(timeline[day])-(len(timeline[dayref])/xfactor[day])

    day='2012-08-21 20x CasA track by AZEL'
    if day in selected_days:
        tshift[day]=len(timeline[day])-(len(timeline[dayref])/xfactor[day])

    day='2012-08-21 100x CasA track by AZEL'
    if day in selected_days:
        tshift[day]=len(timeline[day])-(len(timeline[dayref])/xfactor[day])

days=timeline.keys()
days.sort()

pngfile='CasA_longtracking-'+str("%02d" % len(days))+'days.png'
pngoffsrc='CasA_longtracking-offsource-'+str("%02d" % len(days))+'days.png'
pngonoff='CasA_longtracking-ON-and-OFF-'+str("%02d" % len(days))+'days.png'

# assign the timelines in dB
maxvals=[]
minvals=[]
ndata=0
for day in selected_days:
    """
    if not day in timeline.keys():
        tmp=cas[day].Timeline(1176.45,bwidth)
        #for i in range(cas[day].nData()):tmp[i]=10**(tmp[i]/10.0)
        timeline[day]=tmp
        tmp=cas[day].Timeline(1176.45,bwidth,direction='Y')
        #for i in range(cas[day].nData()):tmp[i]=10**(tmp[i]/10.0)
        offsrc[day]=tmp
    """        
    maxvals.append(max(timeline[day]))
    minvals.append(min(timeline[day]))
    if ndata < len(timeline[day]):ndata=len(timeline[day])


# min/max for plotting
maxval=max(maxvals)
minval=min(minvals)
try:
    if isinstance(pmin,int) or isinstance(pmin,float):
        print "min power: ",minval,' but using: ',pmin
        
except:
    pmin=minval

try:
    if isinstance(pmax,int) or isinstance(pmax,float):
        print "max power: ",maxval,' but using: ',pmax
except:
    pmax=maxval

fig=openPlot('Comparison of CasA tracking on different days',bwidth)
plt.axis([0,ndata,pmin,pmax])

style_index=0
for day in selected_days:
    t=[]
    for i in range(len(timeline[day])):
        t.append(xfactor[day]*(i-tshift[day]))
    plt.plot(t,timeline[day],label=day,
             linestyle=linestyles[style_index],
             color=colours[style_index])
    style_index+=1


#cas[days[0]].pltObsModeTimes([pmin,pmax])
plt.legend(prop={'size':16},loc='upper right',bbox_to_anchor=(0, 0, 1, 1))
plt.savefig(pngfile,format='png',bbox_inches='tight')
plt.show()

### and do the off source tracking
fig=openPlot('Comparison of CasA/off-source tracking on different days',bwidth)
plt.axis([0,ndata,pmin,pmax])

style_index=0
for day in selected_days:
    t=[]
    for i in range(len(timeline[day])):
        t.append(xfactor[day]*(i-tshift[day]))
    plt.plot(t,offsrc[day],label=day,
             linestyle=linestyles[style_index],
             color=colours[style_index])
    style_index+=1


#cas[days[0]].pltObsModeTimes([pmin,pmax])
plt.legend(prop={'size':16},loc='upper right',bbox_to_anchor=(0, 0, 1, 1))
plt.savefig(pngoffsrc,format='png',bbox_inches='tight')
plt.show()

# for the A&A paper, plot ON and OFF tracking together, correcting for time offset
fig=openPlot('Comparison of CasA ON and OFF-source tracking on different days',bwidth)
ax=fig.add_subplot(111)
ax.axis([0,ndata,pmin,pmax])
ax.tick_params(axis='x', labelsize=36)
ax.tick_params(axis='y', labelsize=36)

style_index=0
for day in selected_days:
    t=[]
    for i in range(len(timeline[day])):
        t.append(xfactor[day]*(i-tshift[day]))
    toff=np.array(t)-720
    plt.plot(t,timeline[day],label=day+' ON source',
             linestyle='-',
             color=colours[style_index])
    plt.plot(toff,offsrc[day],label=day+' OFF source',
             linestyle='--',
             color=colours[style_index])
    style_index+=1


#cas[days[0]].pltObsModeTimes([pmin,pmax])
plt.legend(prop={'size':24},loc='upper right',bbox_to_anchor=(0, 0, 1, 1))
plt.savefig(pngonoff,format='png',bbox_inches='tight')
plt.show()
