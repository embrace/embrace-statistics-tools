ttl='Spectra for RCU '+str(rcu)
fig=plt.figure()
fig.canvas.set_window_title(ttl)
ax=fig.add_subplot(111, projection='3d')
ax.set_xlabel('Frequency / MHz')
ax.set_zlabel('uncalibrated Power / dB')
ax.set_ylabel('Time / secs')

for ntime in range(1,a.ndata+1):
    p=a.Data(rcu,ntime)
    ax.plot(a.freq, p, zs=float(ntime), zdir='y', c='r')

#ax.set_xlim3d(a.minfreq, a.maxfreq)
#ax.set_zlim3d(0, a.ndata)
#ax.set_ylim3d(50, 100)

plt.show()
