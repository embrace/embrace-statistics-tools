"""
$Id: mkAnimationSlides.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Mon 09 Sep 2013 15:26:25 CEST

plot images to make an animation of the wobble

using data from pulsar tracking on 2013-09-06
cx already loaded
cx=estats('scudata/Sun--PSRB1133+16_BeamA_csx_20130906-1044.fits')

using data from M33 which is cleaner.  the B1133 data had the sun

cx=estats('scudata/M33_BeamA_csx_20130801-0025.fits')
calibration was done earlier.
suncx=estats('/data/archive/embrace-archive/2013/Sun_BeamA_csx_20130731-1319.fits')
and the phase correction matrix was pickled

$modified: Tue 29 Oct 2013 12:13:57 CET
  redoing boresight pointing after fixing error.  i.e. boresight is (0,pi/2) and *not* (0,0)
  see mods in embraceStats.py and CLStats.py

$modified: Wed 30 Oct 2013 10:44:29 CET
  animation for GPS drift scan


"""
datafile='M33_BeamA_csx_20130801-0025.fits'
datafile='GPSBIIF-2_BeamA_csx_20131029-1504.fits'
datafile='GPSBIIF-2_BeamA_csx_20131030-1459.fits'
datafile='PSRB0329+54_BeamA_csx_20131029-2351.fits'
archive_dir='/data/archive/embrace-archive/2013/'

import os,gc
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from embraceStats import embraceStats as estats
import pickle

os.chdir(os.environ['HOME']+'/data')

if os.path.exists(datafile):
    cx=estats(datafile)
elif os.path.exists(archive_dir+datafile):
    cx=estats(archive_dir+datafile)
else:
    print 'data file not found: ',datafile
    quit()

# for ignorepointing, and without phase calibration to compare to Benj. 2013-10-22
# https://confluence.obs-nancay.fr/pages/viewpage.action?pageId=13107372
# comment the following for no phase calibration

'''
ipick=open('PhaseCorrectionMatrix.pickle','r') # saved earlier from suncx
pcm=pickle.load(ipick)
ipick.close()
cx.sx.PhaseCorrectionMatrix=pcm
cx.sx.calibratedSBnum=256
'''

# for ignore pointing, do not apply Henrik's empty sky algo
'''
ipick=open('emptysky.pickle','r')
emptysky=pickle.load(ipick)
ipick.close()
cx.sx.emptysky=emptysky
'''

#cwd=os.environ['HOME']+'/data/wobble-animation_20130801/boresight'
cwd=os.environ['HOME']+'/data/b0329_tracking_animation'
if not os.path.exists(cwd):
    print 'cannot go to working directory: '+cwd
    quit()
os.chdir(cwd)

'''
# for M33 tracking...
vmin=1e7
vmax=1e8

vmin=6e8
vmax=1e9

# zenith pointing, no calibration
vmin=6e8
vmax=2e9

# zenith pointing, phase calibrated
vmin=6e8
vmax=1e9
'''

# for GPS drift scan, zenith pointing, no phase calibration
vmin=6e8
vmax=2e9

#istart=1798 # for M33 tracking
#istart=17663 # pick up from crash
#iend=22498
istart=0
iend=cx.nData()
itime=istart
frame_period=5
while itime < iend:
    cx.plotData(itime,pmin=vmin,pmax=vmax,xwin=False,ignorepointing=True)
    itime+=frame_period
    gc.collect()



