"""
$Id: BLStats.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed Mar 21 17:00:24 CET 2012

python class to read and plot EMBRACE Beamlet Statistics
this class is called from the generic class embraceStats
it contains methods specific to Beamlet Statistics stuff

$modified: Thu Mar 29 15:21:32 CEST 2012
 adding error handling for the case when no digital beams are defined
  SSMAP()

$modified: Wed Apr 11 15:54:21 CEST 2012
  on/off plotting

$modified: Wed Jun 13 12:21:22 CEST 2012
  removed CalculateTimeline().  It is now done in class embraceStats

  method verify_n() calls DigitalBeam() for compatibility between SBStats

$modified: Mon Jul  2 14:43:48 CEST 2012
  method rawout() to dump to simple ascii file

$modified: Mon Jul 16 10:39:04 CEST 2012
  handle minvals error when all values are zero

$modified: Wed 20 Mar 2013 14:50:54 CET
  moving a number of methods to tools, but still callable here

  nBeamlets()
  SSMAP()
  DigitalBeam()

$modified: Tue 18 Jun 2013 14:33:32 CEST
  Timeline for big file, see also tools_embraceStats.py  

$modified: Mon 24 Jun 2013 15:23:43 CEST
  read more pickles:  timeline

$modified: Tue 25 Jun 2013 14:30:32 CEST
  removed StartFreq() (see BLStats_20130624-2024.py in the deprecated folder)

$modified: Wed 02 Oct 2013 16:31:12 CEST
  calibrated() : for compatibility with embraceStats

$modified: Thu 14 Nov 2013 16:46:31 CET
  plotData() and plotTimeline() : option for linear axis

$modified: Mon 24 Aug 2015 10:58:13 CEST
  mkPlotTitle() : bug fix.  title for plotting all digital beams

"""

# import all the packages we will need
import os,gc
import datetime as dt
import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import axes3d
import pyfits as fits
import numpy as np
import re
from tools_embraceStats import *

class BLStats:
    """
    $Id: BLStats.py
    $auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
    $created: Wed Mar 21 17:00:24 CET 2012

    python class to read and plot EMBRACE Beamlet Statistics
    this class is called from the generic class embraceStats
    it contains methods specific to Beamlet Statistics stuff

    version: 2013 June 24
    """

    # constructor, must pass the metainfo dictionary at creation
    def __init__(self,metainfo):
        self.metainfo=metainfo

         # try to read pickle files if they exist
        self.dynamicspecarray=unpickle(self.metainfo)

        # assign an empty list for the dynamic spec arrays for X and Y of each Digital Beam
        # but only if the unpickling was unsuccessful
        if self.dynamicspecarray==None:
            self.dynamicspecarray=[]
            for i in range(self.metainfo['nDigBeams']):
                self.dynamicspecarray.append([None,None])

        
            
        # do the same for timeline
        # try to read a previously calculated timeline
        self.timeline=unpickle(self.metainfo,datatype='timeline')
        if self.timeline==None:
            self.timeline=[]
            for i in range(self.metainfo['nDigBeams']):
                self.timeline.append({})
                self.timeline[i]['X']=None
                self.timeline[i]['Y']=None
        
        return

        
    # destructor
    def __del__(self):
        return

    # read the SSMAP for beamlet/frequency assignments
    def SSMAP(self,digbeam=0):
        return SSMAP(self.metainfo,digbeam)

    # this is used to check input argument for a digital beam direction
    # give direction as either 'X' or 'Y', or 'x' or 'y', or 1 or 2
    # and return an integer index 0 or 1 for 'X' or 'Y'
    def DigitalBeamDirectionIndex(self,direction):
        if isinstance(direction,int):
            direction_index=direction-1
            if direction_index<0 or direction_index>1:
                print "given direction was: ",direction
                print "please choose a direction: either 'x' or 'y'"
                return None
        
        if isinstance(direction,str):
            if direction=='x' or direction=='X':
                direction_index=0
            elif direction=='y' or direction=='Y':
                direction_index=1
            else:                
                print "given direction was: ",direction
                print "please choose a direction: either 'x' or 'y'"
                return None
        return direction_index

    def DigitalBeamDirection(self,direction):
        direction_index=self.DigitalBeamDirectionIndex(direction)
        if direction_index==0:
            return 'X'
        return 'Y'

    # return the number of beamlets for a digital beam
    def nBeamlets(self,digbeam=0):
        return nBeamlets(self.metainfo,digbeam)

    # this is used to check input argument for max digital beams
    # digbeam counts from 0
    def DigitalBeam(self,digbeam):
        return DigitalBeam(self.metainfo,digbeam)

    # an alias for DigitalBeam
    def verify_n(self,n):
        return DigitalBeam(self.metainfo,n)

    # frequency axis for beamlet statistics
    def Freq(self,digbeam=0):
        freq=[]
        # make sure digbeam is a valid number
        digbeam=self.DigitalBeam(digbeam)

        # start freq is the offset from the beginning of the RF band
        # but we don't need that info, we use the subband select map instead
        # knowing the RF centre, and that the centre is at subband#256 (numbered from 0)
        # (see also the SBStats frequency scale from FITS CRPIX1 etc)
        rfcentre=self.metainfo['RFcentre']
        print "   rfcentre=",rfcentre
        # the RF centre frequency is in the middle of the 256th counting from 0
        subband0=rfcentre-(self.metainfo['chanwidth']*256)
        print "   subband0=",subband0
        nbeamlets=self.nBeamlets(digbeam)
        print "   nbeamlets=",nbeamlets

        # if no digital beams defined
        # assume value for plotting purposes
        if digbeam==None:
            for i in range(0,186): # plot all beamlets
                freq.append(subband0+i*self.metainfo['chanwidth'])

        else:
            ssmap=self.SSMAP(digbeam)
            for beamlet in ssmap:
                subbandNumber=eval(beamlet.split(':')[1])
                freq.append(subband0+subbandNumber*self.metainfo['chanwidth'])

        return freq

    # assign data for a digital beam, and return as a numpy array
    def Data(self,ntime_index,digbeam=0,direction='x',linear=False):
        # make sure digbeam is a valid number
        digbeam=self.DigitalBeam(digbeam)

        # check which direction of the digital beam
        direction_index=self.DigitalBeamDirectionIndex(direction)
        if direction_index==None: return None
        
        # if no digbeams defined, return data from all beamlets
        if digbeam==None:
            p=np.empty(186)
            for i in range(0,186):
                beamlet_number=i
                lane_index=2*(beamlet_number/62)
                index=direction_index+lane_index
                dBm=self.metainfo['hdulist'][ntime_index].data.field(index)[beamlet_number]
                if linear:
                    p[i]=10**(dBm/10.0)
                else:
                    p[i]=dBm
            return p

        # get the ssmap so we know which beamlets to use
        ssmap=self.SSMAP(digbeam)
        nbeamlets=self.nBeamlets(digbeam)

        # now fill the data array
        # beamlets 0-61 are taken from the first lane (RSP0) : x-direction, y-direction -> index 0,1
        # beamlets 62-123 are taken from the second lane (RSP1) : x-direction, y-direction -> index 2,3
        # beamlets 124-185 are taken from the third lane (RSP2) : x-direction, y-direction -> index 4,5
        # we use integer division by 62 (number of beamlets per lane)
        p=np.empty(nbeamlets)
        i=0
        for beamlet in ssmap:
            beamlet_number=eval(beamlet.split(':')[0])
            lane_index=2*(beamlet_number/62)
            index=direction_index+lane_index
            dBm=self.metainfo['hdulist'][ntime_index].data.field(index)[beamlet_number]
            if linear:
                p[i]=10**(dBm/10.0)
            else:
                p[i]=dBm
            i+=1
        
        return p


    # make an array for a dynamic spectrum of one digital beam
    def mkDynamicSpecArray(self,digbeam,direction):
        digbeam=self.DigitalBeam(digbeam)
        print "filling array for Digital Beam ",digbeam," Direction ",direction

        # check which direction of the digital beam
        direction_index=self.DigitalBeamDirectionIndex(direction)
        if direction_index==None: return None

        X = np.empty( (self.metainfo['ndata'],self.nBeamlets(digbeam)) )
        for i in range(0,self.metainfo['ndata']):
            ntime_index=i+self.metainfo['firstHDU']
            X[i,]=self.Data(ntime_index,digbeam,direction)

        return X

    # return a dynamic spec array for plotting
    def DynamicSpec(self,n=0,direction='x'):
        digbeam=self.DigitalBeam(n)

        # check which direction of the digital beam
        direction_index=self.DigitalBeamDirectionIndex(direction)
        if direction_index==None: return None

        if self.dynamicspecarray[digbeam-1][direction_index]==None:
            self.dynamicspecarray[digbeam-1][direction_index]=self.mkDynamicSpecArray(digbeam,direction)
            # write a pickle file with the dynamic spec array so we don't have to make it again
            pickle(self.metainfo,self.dynamicspecarray)

        return self.dynamicspecarray[digbeam-1][direction_index]

    # dump data to a simple ascii file
    def rawout(self,n=0):
        digbeam=self.DigitalBeam(n)

        for direction in ['X','Y']:
            dat=self.DynamicSpec(digbeam,direction)
            fname=self.metainfo['filename'].replace('.fits','.raw-'+direction+'.dat')
            print "writing data to file: ",fname
            handle=open(fname,'w')

            for j in range(self.metainfo['ndata']):
                vals=""
                for i in range(self.nBeamlets(digbeam)):
                    vals+=str(dat[j][i])+' '
                handle.write(vals+'\n')
            
            handle.close()
        return True
        

    # make the middle part of a title for a plot
    def mkPlotTitle(self,n=0,direction='x'):
        digbeam=self.DigitalBeam(n)
        if n==None:
            ttl=' : All Digital Beams'
            filename='_AllDigBeams'
            return ttl,filename

        ttl=' : Digital Beam #'+str(digbeam)
        filename='_Dig'+str("%02d" % digbeam)
        if direction!=None:
            dirstr=self.DigitalBeamDirection(direction)
            ttl+=' Direction-'+dirstr
            filename+='_Dir'+dirstr

        return ttl,filename

    # plot a spectrum from a digital beam, at a given time
    def plotData(self,ntime,n=0,direction='x',linear=False):
        digbeam=self.DigitalBeam(n)
        data=self.Data(ntime,digbeam,direction,linear)
        if data==None: return False
        freq=self.Freq(digbeam)
        if freq==None: return False


        ret=plt.plot(freq,data,
                     linestyle=self.metainfo['linestyles'][0],
                     color=self.metainfo['colours'][0])
        return ret


    # calculate the timeline, the work is done in tools_embraceStats
    def Timeline(self,freq0,bwidth=0.0,nchans=None,n=None,direction='x',linear=False):
        dirstr=self.DigitalBeamDirection(direction)
        digbeam=self.DigitalBeam(n)
        if self.timeline[digbeam][dirstr]==None:
            self.timeline[digbeam][dirstr]=CalculateTimeline(self,freq0,bwidth,nchans,n,direction,linear)
            pickle(self.metainfo,self.timeline,datatype='timeline')
        else:
            print 'WARNING: plotting timeline that was stored in memory'
        return self.timeline[digbeam][dirstr]

    # plot the timelines for all digital beams, both directions
    def plotTimeline(self,freq0,bwidth,nchans=None,onoff=False,linear=False):

        nstyles=len(self.metainfo['linestyles'])
        style_index=0
        directions=['X','Y']
        minvals=[]
        maxvals=[]


        if onoff:
            for digbeam in range(0,self.metainfo['nDigBeams']):
                pon=self.Timeline(freq0,bwidth,nchans,n=digbeam,direction='X',linear=linear)
                poff=self.Timeline(freq0,bwidth,nchans,n=digbeam,direction='Y',linear=linear)
                power=pon-poff
                minvals.append(min(power))
                maxvals.append(max(power))

                style_index=digbeam
                while style_index >= nstyles: style_index-=nstyles
                ret=plt.plot(power,
                             label='Digital Beam#'+str(digbeam),
                             linestyle=self.metainfo['linestyles'][style_index],
                             color=self.metainfo['colours'][style_index])

        else: #not on/off
            for digbeam in range(0,self.metainfo['nDigBeams']):
                for direction in ['X','Y']:
                    direction_index=self.DigitalBeamDirectionIndex(direction)
                    power=self.Timeline(freq0,bwidth,nchans,n=digbeam,direction=direction,linear=linear)
                    # for min power, ignore zeroes, but if there are only zeros, return zero
                    try: minvals.append(min(power[power>0.0]))
                    except: minvals.append(0.0)
                    maxvals.append(max(power))

                    ret=plt.plot(power,
                                 label='Digital Beam#'+str(digbeam)\
                                     +' direction '+directions[direction_index],
                                 linestyle=self.metainfo['linestyles'][style_index],
                                 color=self.metainfo['colours'][style_index])
                    style_index+=1
                    while style_index >= nstyles: style_index-=nstyles


        return min(minvals),max(maxvals)

    # clear calculated results
    def clearcal(self):        
        for digbeam in range(0,self.metainfo['nDigBeams']):
            self.timeline[digbeam]['X']=None
            self.timeline[digbeam]['Y']=None
        return None

    # return calibration status (for compatibility with embraceStats)
    def calibrated(self):
        return True
