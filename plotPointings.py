"""
$Id: plotPointings.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed Nov 30 04:53:17 CET 2011

plot azimuth and elevation from the pointing commands list

$modified: Mon Dec  5 17:39:33 CET 2011
 get the pointing info from the FITS header
 this will go into the BLStats class
 ie. testing here.
"""

import sys,os
import datetime as dt
import re
import matplotlib.pyplot as plt

def tot_seconds(delta):
    tsecs=delta.days*24*3600 + delta.seconds + delta.microseconds*1e-6
    return tsecs

def getPointings(bsx):
    t=[]
    az1=[]
    el1=[]
    az2=[]
    el2=[]
    key_index=-1
    for key in bsx.hdulist[0].header.ascard.keys():
        key_index+=1
        match=re.search('^DI01A[0-9]*',key)
        if match:
            az1.append(float(bsx.hdulist[0].header[key_index]))
            continue

        match=re.search('^DI11A[0-9]*',key)
        if match:
            el1.append(float(bsx.hdulist[0].header[key_index]))
            continue

        match=re.search('^DI02A[0-9]*',key)
        if match:
            az2.append(float(bsx.hdulist[0].header[key_index]))
            continue

        match=re.search('^DI12A[0-9]*',key)
        if match:
            el2.append(float(bsx.hdulist[0].header[key_index]))
            continue

        match=re.search('^POINTIME',key)
        if match:
            datestr=bsx.hdulist[0].header[key_index]
            if len(datestr)>20:
                t.append(dt.datetime.strptime(datestr,bsx.strpfmt_long))
            else:
                t.append(dt.datetime.strptime(datestr,bsx.strpfmt))
            continue

    return t,az1,el1,az2,el2
    


def readPointings(filename):

    if not os.path.exists(filename):
        print "file not found: ",filename
        return None,None,None,None,None

    t=[]
    az1=[]
    el1=[]
    az2=[]
    el2=[]

    ifile=open(filename,'r')
    cmd=ifile.readline()
    while cmd!="":
        match=re.search('^start=',cmd)
        if match:
            tstr=cmd[6:25]
            t.append(dt.datetime.strptime(tstr,'%Y/%m/%d %H:%M:%S'))

            match=re.search('angle1=.*?,',cmd)
            az1.append(float(match.group()[7:-1]))

            match=re.search('angle2=.*?[,\n]',cmd)
            el1.append(float(match.group()[7:-1]))

            match=re.search('angle3=.*?,',cmd)
            if match:
                az2.append(float(match.group()[7:-1]))

            match=re.search('angle4=.*',cmd)
            if match:
                el2.append(float(match.group()[7:]))

        cmd=ifile.readline()
    ifile.close()

    if len(t)==0:t=None
    if len(az1)==0:az1=None
    if len(el1)==0:el1=None
    if len(az2)==0:az2=None
    if len(el2)==0:el2=None
    return t,az1,el1,az2,el2

def plotPointings(entity,title=None,label=None,file2=None,label2=None):

    if isinstance(entity,BLStats):
        cmd_time,az1,el1,az2,el2=getPointings(entity)
        filename=entity.filename.replace('.fits','')
    else:
        cmd_time,az1,el1,az2,el2=readPointings(entity)
        filename=entity.replace('.txt','')

    if cmd_time==None: return
    if title==None:title='Commanded Angles'
    if label==None:label=filename

    t0=cmd_time[0]
    tdelta=[]
    for t in cmd_time:
        tdelta.append(tot_seconds(t-t0))

    cmd_time2=None
    if file2!=None:
        cmd_time2,f2_az1,f2_el1,f2_az2,f2_el2=readPointings(file2)
        if cmd_time2!=None:
            if label2==None:label2=file2
            tdelta2=[]
            for t in cmd_time2:
                tdelta2.append(tot_seconds(t-t0))


    pngfile=filename+'_azimuth.png'
    plt.figure(1,figsize=(12.80,9.60))
    plt.title(title)
    plt.xlabel('time / seconds since '+t0.strftime('%Y-%m-%d %H:%M:%S.%f UT'))
    plt.ylabel('commanded azimuth angle / degrees')

    plt.plot(tdelta,az1,'x',color='blue',label=label)
    if cmd_time2!=None:
        plt.plot(tdelta2,f2_az1,'2',color='green',label=label2)

    plt.legend()
    plt.savefig(pngfile,format='png',dpi=100)

    pngfile=filename+'_elevation.png'
    plt.figure(2,figsize=(12.80,9.60))
    plt.title(title)
    plt.xlabel('time / seconds since '+t0.strftime('%Y-%m-%d %H:%M:%S.%f UT'))
    plt.ylabel('commanded elevation angle / degrees')

    plt.plot(tdelta,el1,'x',color='blue',label=label)
    if cmd_time2!=None:
        plt.plot(tdelta2,f2_el1,'2',color='green',label=label2)

    plt.legend()
    plt.savefig(pngfile,format='png',dpi=100)
    plt.show()

    return
        

