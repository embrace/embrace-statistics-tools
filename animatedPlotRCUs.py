# sequentially plot all data
# method taken from the matplotlib example: animate_decay_tk_blit.py
# make sure hdulist is already assigned!
# and also rcu!

import pyfits as fits
import matplotlib
import matplotlib.pyplot as plt
import sys
print "defining my routines",
execfile('scripts/try.py')
print "done"
print "opening fits file",
#hdulist=fits.open('gps100930_sigtest_12rcu.fits')
print "done"
ndata=len(hdulist)-1
data=hdulist[1].data
nchans=len(data) # this should always be 512 for EMBRACE

nrcu=len(data[0])
print "number of RCUs: ",nrcu

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylim(60, 90)
ax.set_xlim(0, 512)
#ax.grid()
    
#background = fig.canvas.copy_from_bbox(ax.bbox)

print "defining animate",
def animate(*args):
    #freq=SBStatsFreq(hdulist)
    freq=np.array(range(0,512))
    p=np.array(range(0,512))
    line, = ax.plot(freq, p)
    for i in range(1,ndata):
        
        #for rcu in [1]:
        p=SBStatsData(hdulist,rcu,i)
        line.set_ydata(p)
        animate.cnt+=1
        if(animate.cnt>=10000):
            print "exiting after loop limit"
            sys.exit()
        fig.canvas.draw()

animate.cnt=0
print "done"
win = fig.canvas.manager.window
fig.canvas.manager.window.after(10, animate)
print "executing plot.show()",
plt.show()
print "done"
