'''
$Id: plot-CasA_longdrift.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Tue 02 Jul 2013 10:15:44 CEST

over plot CasA long drift scans

$modified: Thu 18 Jul 2013 14:56:27 CEST
  added drift scan from 2013-07-17

$modified: Mon 21 Oct 2013 13:02:14 CEST
  added drift scan from 2013-10-17

'''
from embraceStats import embraceStats as estats

fitsfiles=['Sun--CasA_BeamA_bsx_20130615-1150.fits',
           'Sun--CasA_BeamA_bsx_20130621-1151.fits',
           'Sun--CasA_BeamA_bsx_20130717-1219.fits',
           'Sun--CasA_BeamB_bsx_20130717-1219.fits',
           'CasA_BeamA_bsx_20131017-1829.fits']


'''
# load files
beamletstats=[]
for F in fitsfiles:
    beamletstats.append(estats(F))
'''

pngfile='CasA-longdrift-overlapped.png'
boffset=17.4

# setup plot
fig=beamletstats[0].openPlot('CasA overlaid drift scans',
                             'blah filename')

# find CasA drift peaks
mint=[]
maxt=[]
minvals=[]
maxvals=[]
for bx in beamletstats:
    minvals.append(min(bx.Timeline()))
    maxvals.append(max(bx.Timeline()))
    peakcount=0
    for dtpeak in bx.ObsModeTimes()['xpeak']:
        if peakcount==0: # first peak is the calibration
            peakcount+=1
            continue
        peaksecs=bx.tot_seconds(dtpeak - bx.startTime())
        t=np.array( range(bx.nData()) ) - peaksecs
        mint.append(t[0])
        maxt.append(t[bx.nData()-1])
        lbl='t=0 -> '+dtpeak.isoformat(' ')+' UT, Beam-'+bx.RFbeam()
        if bx.RFbeam()=='B':
            lbl+=' (offset)'
            plt.plot(t,bx.Timeline()-boffset,label=lbl)
        else:
            plt.plot(t,bx.Timeline(),label=lbl)
        peakcount+=1

# min/max seconds adjusted to be centred at CasA drift peak
minsecs=min(mint)
maxsecs=max(maxt)

# min/max power
minval=min(minvals)
maxval=max(maxvals)

ax = fig.add_subplot(111)
ax.set_xlabel('time / seconds since first drift peak')
ax.set_ylabel('integrated power / dBm')

axes=[minsecs,maxsecs,minval,maxval]
try:
    if isinstance(pmin,float) or isinstance(pmin,int):
        axes[2]=pmin
except:
    pass

try:
    if isinstance(pmax,float) or isinstance(pmax,int):
        axes[3]=pmax
except:
    pass

ax.axis(axes)
plt.legend(prop={'size':8},loc='upper right',bbox_to_anchor=(0, 0, 1.05, 1.05))
plt.savefig(pngfile,format='png')
plt.show()




