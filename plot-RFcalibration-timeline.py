"""
$Id: plot-RFcalibration-timeline.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Mon 28 Jan 2013 12:31:32 CET

plot the timeline from the RF calibration with lines and points
zoomed in to show the separation of measurements

$modified: Mon 24 Jun 2013 10:28:29 CEST
  getObsModeTimes() becomes ObsModeTimes()

"""

import os,sys
from embraceStats import embraceStats as estats

try:
    if isinstance(sx,estats):pass
except:
    print "please load the data in variable sx"
    quit()

try:
    if isinstance(pmin,float) or isinstance(pmin,int):
        pass
except:
    pmin=75

try:
    if isinstance(pmax,float) or isinstance(pmax,int):
        pass
except:
    pmax=90


startTimes=sx.ObsModeTimes()
RFcalstart=sx.tot_seconds(startTimes['RFcalstart'][0]-sx.startTime())
RFcalend=sx.tot_seconds(startTimes['digcalstart'][0]-sx.startTime())
try:
    if isinstance(tmin,float) or isinstance(tmin,int):
        pass
except:
    tmin=RFcalstart

try:
    if isinstance(tmax,float) or isinstance(tmax,int):
        pass
except:
    tmax=RFcalend

try:
    if isinstance(rcu,int):pass
except:
    rcu=0



p=sx.Timeline(n=rcu)
pngfile=sx.Filename().replace('.fits','')\
    +'_RFcalibration-timeline_RCU'+str(rcu)+'.png'

ttl='Beam-'+sx.RFbeam()+': RF Calibration Timeline '+sx.Title()+' rcu='+str(rcu)
subttl=''

fig=plt.figure(figsize=sx.metainfo['figsize'])
fig.canvas.set_window_title('plt: '+ttl) 
ax1 = fig.add_subplot(111)

plt.suptitle(ttl,fontsize=16)
plt.title(subttl,fontsize=14)
plt.plot(p)
plt.plot(p,'ro')
plt.axis([tmin,tmax,pmin,pmax])

plt.xlabel('time / seconds since '+sx.startTime().isoformat(' ')+' UT')
plt.ylabel('power / dBm')
sx.pltObsModeTimes([pmin,pmax])

plt.savefig(pngfile,format='png')
plt.show()




