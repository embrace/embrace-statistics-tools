"""
plot comparison of CasA tracking
$modified: Mon May 14 13:20:20 CEST 2012
  added run from 2012-05-11
"""
cas1=estats('CasA_bsx_20111116-1933.fits')
cas2=estats('CasA_bsx_20111117-1929.fits')
cas3=estats('CasA_bsx_20111207-1710.fits')
cas4=estats('CasA_bsx_20111208-1706.fits')
cas5=estats('CasA_BeamA_bsx_20120427-0752.fits')
cas6=estats('CasA_BeamA_bsx_20120511-0657.fits')
p1=cas1.Timeline(1175.6,6)
p2=cas2.Timeline(1175.6,6)
p3=cas3.Timeline(1175.6,6)
p4=cas4.Timeline(1175.6,6)
p5=cas5.Timeline(1175.6,6)
p6=cas6.Timeline(1175.6,6)


ndata=max([cas1.nData(),cas2.nData(),cas3.nData(),cas4.nData(),cas5.nData(),cas6.nData()])

fig=plt.figure(figsize=cas1.metainfo['figsize'])
plt.axis([0,ndata,83,86])
plt.title('CasA track, drift, track, drift')
plt.xlabel('time / seconds ')
plt.ylabel('uncalibrated power / dB')
plt.plot(p1,label='2011-11-16')
plt.plot(p2,label='2011-11-17')
plt.plot(p3,label='2011-12-07')
plt.plot(p4,label='2011-12-08')
plt.plot(p5,label='2012-04-27')
plt.plot(p6,label='2012-05-11')
plt.legend()
plt.savefig('CasA-6days.png',format='png')
plt.show()
