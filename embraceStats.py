"""
$Id: embraceStats.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed Mar 21 11:26:28 CET 2012 from classBLstats.py

python class to read and plot EMBRACE Statistics
this is a general class to read all types of stats file (ssx,bsx,csx)
the header is the same for all types.

$modified: Wed Mar 21 11:26:28 CET 2012
  copied from classBLstats.py
  for earlier modification logs, see file classBLStats_20120316-1655.py

  new class variable "metainfo" which is a dictionary of meta data garnered from header etc.
  this is passed to the classes defined for each type of statistics data (ssx,bsx,csx)

$modified: Thu Mar 22 11:48:20 CET 2012
  nDigBeams joins the metainfo dictionary
  hdulist joins the metainfo dictionary

  trying to get some kind of standardization ...
  method nDigitalBeams() becomes assign_nDigitalBeams()
  method assignNRCU() becomes assign_nRCU()

$modified: Fri Mar 23 12:16:09 CET 2012
  struggling along

$modified: Tue Mar 27 10:37:51 CEST 2012
  a lot of the stuff I did with Beam-A and Beam-B has to be deleted
  there will be separate files for Beam-A and Beam-B data.
  It's not all in one file.
  just in case, go back and look at embraceStats_20120326-1058.py

$modified: Wed Mar 29 ......... CEST 2012
  stuff related to BLStats

$modified: Fri Mar 30 12:26:04 CEST 2012
  beginning to implement crosslet statistics

$modified: Wed Apr 11 15:17:41 CEST 2012
  debugging:  problems with reading old files
  on/off plotting

$modified: Fri Apr 27 13:57:31 CEST 2012
  new method: elevation() read elevation angles from header
  these are the *planned* pointings

  new method: DigbeamHDU()

$modified: Thu May  3 16:04:24 CEST 2012
  new method: FirstHDU()

$modified: Mon May 14 12:06:35 CEST 2012
  changed y-axis label units from dB to dBm

$modified: Fri May 25 09:09:29 CEST 2012
  optional subtitle to plots

$modified: Wed Jun 13 12:05:06 CEST 2012
  CalculateTimeline() is moved here because it's mostly the same in both BLStats and SBStats
  also:  There was an erroneous ntime-1 in the BLStats version.  Why didn't it crash?

  no, it goes in tools_embraceStats because it needs to be called from BLStats and SBStats

  new method: verify_n() to check for selection of digbeam or rcu

$modified: Fri Jun 22 13:43:07 CEST 2012
  filename now refers to the "basename" (ie. path stripped off).

$modified: Mon Jul  2 14:37:40 CEST 2012
  new class: rawout() for writing data to simple text file for Henrik

$modified: Wed Jul  4 14:38:27 CEST 2012
  class str2dt(), more robust way to read times and convert to datetime
  rather than guessing according to string length
  NOTE:  there was already readTime() !

$modified: Mon Jul 16 13:59:19 CEST 2012
  added offstart to getObsModeTimes() and pltObsModeTimes()
  changed return value to a dictionary.  comma separated variables was too long.

$modified: Thu 02 Aug 2012 13:23:14 CEST
  assignTitle() : to deal with the problem with obstag doesn't match the obs
                  (ie. calibration run before source observing)

$modified: Mon 13 Aug 2012 14:41:40 CEST
  plot the grid in plotData()

$modified: Thu 16 Aug 2012 15:41:52 CEST
  getObsModeTimes() : allow for 'starting' as well as 'beginning'

$modified: Mon 27 Aug 2012 17:05:31 CEST
  bug fix in read pointing for RF.  RFBeam should be RFbeam

  new method: printPointing():  print pointing to screen

$modified: Tue 28 Aug 2012 11:38:03 CEST
  added 'empty sky' for off pointing in getObsModeTimes()
  join long comment lines by eliminating "&'" in getObsModeTimes()

$modified: Wed 29 Aug 2012 15:26:23 CEST
    str2dt() made a bit more robust with regex to remove unwanted text.

$modified: Tue 08 Jan 2013 12:10:25 CET
  methods to assign and to return RFcalfreq
  method to return RFcentre
  plotTimeline() : frequency and bandwidth is optional, plot RFcalfreq by default

$modified: Fri 25 Jan 2013 11:21:03 CET
  plotTimeline() : grid is optional

$modified: Fri 22 Feb 2013 14:47:50 CET
  plotSBnum() : plot sub band number for crosslet stats

$modified: Wed 27 Feb 2013 15:46:50 CET
  SBnum() : returns the subband number for a crosslet stats at a given time
  UV() : return visibility matrix for a given time
  metainfo['tilesets'] : the tilesets used in the observation, numbered from 0 (usually 0-15)

$modified: Fri 01 Mar 2013 15:22:53 CET
  nRCU() : return number of RCU
  assignTilesetPositions() : relative physical location of tilesets

$modified: Sun 03 Mar 2013 16:06:46 CET
  metainfo['RFbeamHDU']

$modified: Mon 04 Mar 2013 12:35:26 CET
  RFpointing() : return pointing requested for a given time

  removed all remaining instances of readTime() and replaced with str2dt()

$modified: Mon 11 Mar 2013 17:11:35 CET
  plotData() : mods to handle plotting image from crosslet stats

$modified: Tue 12 Mar 2013 12:00:12 CET
  plotData() and Data() : mods to handle calibration with crosslet stats 

$modified: Fri 15 Mar 2013 16:48:01 CET
  plotData() : added pmin and pmax
               implementing for CSX
               still to do for BSX and SSX

$modified: Tue 19 Mar 2013 11:19:22 CET
  SBnum() : fixing inconsistencies with ntime and ntime_index
  RFpointing() : bug because loop increments even after break

  nBeamlets() : return number of beamlets for a digital beam
                this already appears in BLStats
                I need to clean up

                other duplicates:  digbeamHDU(), 

$modified: Wed 20 Mar 2013 12:07:43 CET
  moving stuff to tools_embraceStats

  embraceNancay for ephem calculations

$modified: Sun 31 Mar 2013 19:35:31 CEST
  ignore keyword added to Timeline for CSX timeline of homemade BSX

$modified: Tue 02 Apr 2013 20:29:47 CEST
  plotData() : can specify sbnum to override what's recorded at ntime in CSX (sometimes it's -1)

$modified: Fri 19 Apr 2013 12:12:35 CEST
  pointing() : deleted here.  It had already been moved to tools_embraceStats on  20 March

  clearcal() : clear the calibration in CSX

  SBnum2Freq() : return the frequency for a given subband number

$modified: Mon 17 Jun 2013 12:10:22 CEST
  added filesize to metainfo, which will be used to check for memory limits on DynamicSpec etc

$modified: Mon 24 Jun 2013 10:00:50 CEST
  obsModeTimes added to metainfo:  getObsModeTimes() becomes assignObsModeTimes

  pickle stuff: metainfo and timeline
  in case of big files, try to read pickles only... this doesn't work because there's too much access to hdulist
   
$modified: Wed 26 Jun 2013 13:07:11 CEST
  metainfo now has the headerlist
  this is to help some of the difficulties with big files

  all references to hdulist.header are replaced by headerlist

$modified: Fri 28 Jun 2013 07:53:32 CEST
  after numerous attempts reading big files...
  memmap=True is removed:  it causes falling over with "too many open files"

$modified: Fri 28 Jun 2013 16:20:56 CEST
  headerlist:  this didn't work out the way I hoped.  It uses too much memory.
  going back to the earlier implementation.  
    i.e. headerlist replaced with self.metainfo['header'].header

  startTime() : now a method instead of a local variable
  startTime saved in metainfo:  
    this is so we have it for big files when we read only the pickles

  Timestamp : now saved in metainfo, as with startTime etc

$modified: Tue 23 Jul 2013 13:16:45 CEST
  nDigitalBeams() : return number of digital beams
  other modifications for compatibility with multiple digital beams...

$modified: Tue 27 Aug 2013 14:22:12 CEST
  UV() : argument to CLStats must be ntime_index, not ntime  
  plotTimeline() : bug fix: reference to self.readPointing which was moved 
                   to tools_embraceStats some time ago

$modified: Mon 09 Sep 2013 14:54:43 CEST
  plotData() : option xwin, plot to screen by default
               set xwin=False when doing plotData in a loop to make an animation

               change to subtitle.  full title was sloshing over the edge

$modified: Tue 10 Sep 2013 17:23:24 CEST
  plotData() : option to ignore pointing, we want to see if the wobble pattern is static
  Data()     : as above            

$modified: Wed 11 Sep 2013 18:16:58 CEST
  option to ignorepointing for timeline for csx 
  default timeline for csx is beamlet rather than total power

$modified: Thu 03 Oct 2013 16:27:30 CEST
  modifications to do plots of phase difference between tilesets

$modified: Fri 11 Oct 2013 12:25:25 CEST
  PlotTimeline() : and associates, plotting phase difference between tilesets, not necessarily with ref tileset=0

$modified: Mon 21 Oct 2013 10:42:13 CEST
  Data() : missing npix added for crosslet stats

$modified: Tue 29 Oct 2013 11:16:08 CET
  ignorepointing : fixed error.  zenith is at (0,pi/2) and not (0,0) !!

$modified: Thu 07 Nov 2013 16:01:33 CET
  reading old files (version < 1.1) : fixed bug on RFcentre and RFcalfreq

$modified: Thu 14 Nov 2013 16:46:31 CET
  plotData() and plotTimeline() : option for linear axis

$modified: Tue 08 Apr 2014 11:14:02 CEST
  assignSSMAP() : to read old files

$modified: Mon 14 Apr 2014 14:45:23 CEST
  plotData() : fixed bug where calibration status is determined *after* plot title is generated.

$modified: Thu 02 Oct 2014 12:22:37 CEST
  plotTimeline() : tmin, tmax keywords

$modified: Wed 08 Oct 2014 14:53:46 CEST
  TimestampOffset() : determine offset from the expected timestamp
                      this is to verify that the timestamp is incrementing as expected
  plotTimestampOffset() : self explanatory

$modified: Tue 20 Jan 2015 10:38:52 CET
  PyFITS:  fixing deprecation warnings for cards etc (as of PyFITS Version 3.1)

$modified: Tue 10 Mar 2015 16:05:49 CET
  assignObsModeTimes() : bug (or change) in the way pyfits handles repeated key names (ie. COMMENT)

$modified: Mon 24 Aug 2015 10:52:37 CEST
  plotTimeline() : leading 0 on frequency in filename  

$modified: Wed 26 Aug 2015 17:21:32 CEST
  pyFits: deprecated "ascardlist" and "key" replaced by "cards" and "keyword"

$modified: Thu 27 Aug 2015 15:02:11 CEST
  pyFits: use memmap=False in read to avoid "too many open files" error

$modified: Thu 12 Nov 2015 12:21:46 PST
  savefig: bbox "tight" to remove whitespace

$modified: Tue 26 Jul 2016 15:28:32 CEST
  assignRFcentre: fix so we can read old files (eg. Picard GLONASS of 2011-01-05)

"""

# import all the packages we will need
import os
import datetime as dt
import matplotlib.pyplot as plt
import math
#from matplotlib.backends.backend_agg import FigureCanvasAgg # for plotting to file without showing to screen
#from mpl_toolkits.mplot3d import axes3d
import pyfits as fits
import numpy as np
import re
import ephem as eph
from BLStats import BLStats
from SBStats import SBStats
from CLStats import CLStats
from tools_embraceStats import *

class embraceStats:
    """
    $Id: embraceStats.py
    $auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
    $created: Wed Mar 21 11:26:28 CET 2012 from classBLstats.py

    python class to read and plot EMBRACE Statistics
    this is a general class to read all types of stats file (ssx,bsx,csx)
    the header is the same for all types.

    """

    # constructor
    def __init__(self,filename=None):

        # initialize metainfo dictionary
        self.metainfo={}

        # the pointing text files have time given in UT
        # timestamps for files between 2011-09-15 and 2011-11-18 are in localtime
        # figure out the time difference to UT
        delta_ut2local=dt.datetime.now()-dt.datetime.utcnow()
        ut2local_hours=round((delta_ut2local.seconds+delta_ut2local.microseconds/1e6)/3600)
        self.metainfo['ut2local']=dt.timedelta(hours=ut2local_hours)

        # resolution for figures saved as images in pixels/dpi.  dpi=100
        self.metainfo['figsize']=(12.80,9.60)

        self.metainfo['linestyles']=['-','-','-','-','-','-','--','--','--','--','--','--','-.','-.','-.','-.','-.','-.']

        self.metainfo['colours']=['b','g','r','c','m','k','b','g','r','c','m','k','b','g','r','c','m','k']

        # bandpass info

        # nchans can be determined from the length of the SSX data
        # but it should always be 512 for EMBRACE
        self.metainfo['nchans']=512 

        # RF bandwidth can be determined from the BANDWIDT keyword in the SSX primary header
        # but it should always be 100MHz for EMBRACE
        self.metainfo['RFbandwidth']=100.0

        # channel width in MHz for EMBRACE
        # this can also be read from the CDELT1 keyword in the SSX primary header
        self.metainfo['chanwidth']=self.metainfo['RFbandwidth']/self.metainfo['nchans'] 

        self.metainfo['title']='No Title'


        # read a file if given
        if filename != None:
            self.read(filename)
        else:
            self.metainfo['filename']="No_Filename.fits"
        
        return

    # destructor
    def __del__(self):
        self.close()
        return

    # close a fits file
    def close(self):
        if self.metainfo['hdulist'] != None:
            self.metainfo['hdulist'].close()
        return


    # read a fits file
    def read(self,filename):
        self.metainfo['filename']=os.path.basename(filename)
        self.metainfo['fullpathname']=filename

        if not os.path.exists(filename):
            print "file does not exist: ",filename
            print "looking for pickles of that name"
            metainfo=unpickle(self.metainfo,datatype='metainfo')
            if metainfo!=None:
                self.metainfo=metainfo
                self.assignStatisticsObject()
                return True
            return False


        self.metainfo['filesize']=os.path.getsize(filename)

        if self.metainfo['filesize'] > bigfilelimit:
            print 'woaaah:  big file. trying to read pickles instead of reading the big file.'
            metainfo=unpickle(self.metainfo,datatype='metainfo')
            if metainfo!=None:
                self.metainfo=metainfo
                self.assignStatisticsObject()
                return True


        self.metainfo['hdulist']=fits.open(filename,mode='readonly',memmap=False)

        self.assignParameters()
        try:
            pickle(self.metainfo,datatype='metainfo')
        except:
            print "could not write metainfo pickle"
        return True


    # determine first HDU with stats data, and the type of stats data
    def assignFirstHDU(self):
        if self.metainfo['fitsversion']<=1.1:
            self.metainfo['firstHDU']=1

            if 'VERSION' in self.metainfo['hdulist'][0].header.keys():
                cardcomment=self.metainfo['hdulist'][0].header['VERSION'].comment
                if cardcomment.find('SSX')>=0:
                    self.metainfo['statstype']='SSX'
                elif cardcomment.find('BSX')>=0:
                    self.metainfo['statstype']='BSX'
                elif cardcomment.find('CSX')>=0:
                    self.metainfo['statstype']='CSX'
                else:
                    self.metainfo['statstype']='UNKNOWN Statistics'
            else: # try to guess from the filename
                if self.metainfo['filename'].find('ssx')>=0:
                    self.metainfo['statstype']='SSX'
                elif self.metainfo['filename'].find('bsx')>=0:
                    self.metainfo['statstype']='BSX'
                elif self.metainfo['filename'].find('csx')>=0:
                    self.metainfo['statstype']='CSX'
                else:
                    self.metainfo['statstype']='UNKNOWN Statistics'
            return

        gotit=False
        i=0
        hdutype='UNKNOWN'
        while not gotit and i < self.metainfo['nHDU']-1:
            i+=1
            hdutype=self.metainfo['hdulist'][i].header['HDUTYPE']
            if hdutype == 'BSX' or hdutype == 'SSX' or hdutype == 'CSX':
                gotit=True

        self.metainfo['firstHDU']=i
        self.metainfo['statstype']=hdutype

        if not gotit:
            print "ERROR: no EMBRACE statistics data"
        else:
            print self.metainfo['filename']+" contains EMBRACE statistics of type "+self.metainfo['statstype']
        return gotit

    # assign the start time of the observation
    def assignStartTime(self):
        self.metainfo['startTime']=self.Timestamp(0)
        return True

    # assign ndata: number of data points
    def assign_nData(self):
        self.metainfo['ndata']=self.metainfo['nHDU']-self.metainfo['firstHDU']
        if self.metainfo['ndata'] <= 0:
            print "ERROR: no data"
            return False

        return True

    # determine the HDU with RF Beam info (normally, it's hdu=1, hdu=0 is the primary header)
    def assignRFbeamHDU(self):
        self.RFbeamHDU=0
        self.RFbeamDefined=True
        self.metainfo['RFbeam']='0'
        if self.metainfo['fitsversion']<=1.1:
            self.metainfo['RFbeamHDU']=0
            return False

        self.RFbeamDefined=False
        i=0
        while not self.RFbeamDefined and i<self.metainfo['nHDU']-1:
            i+=1
            if self.metainfo['hdulist'][i].header['HDUTYPE'] == 'BEAM':
                if self.metainfo['hdulist'][i].header['BEAMTYPE'] == 'RF':
                    self.RFbeamHDU=i
                    self.metainfo['RFbeam']=self.metainfo['hdulist'][i].header['FOV']
                    if self.RFbeamHDU!=0:
                        self.RFbeamDefined=True

        if not self.RFbeamDefined:
            print "WARNING: didn't find RF beam information"

        self.metainfo['RFbeamHDU']=self.RFbeamHDU
        return self.RFbeamDefined


    # assign number of RCUs connected
    def assign_nRCU(self):
        if 'TILESETS' in self.metainfo['hdulist'][0].header.keys():
            self.metainfo['nRCU']=len(eval(self.metainfo['hdulist'][0].header['TILESETS']))
            return True
        
        print "ERROR: no information on the number of tilesets"
        self.metainfo['nRCU']=0
        return False


    # find the number of defined digital beams
    def assign_nDigitalBeams(self):
        hdu=self.RFbeamHDU
        if 'NDIGBEAM' in self.metainfo['hdulist'][hdu].header.keys():
            ndigbeams=self.metainfo['hdulist'][hdu].header['NDIGBEAM']
            if isinstance(ndigbeams,str):
                self.metainfo['nDigBeams']=eval(ndigbeams)
            else:
                self.metainfo['nDigBeams']=ndigbeams
            return True

        # there's only a few files that don't have NDIGBEAM defined...

        # find the index number of the last defined digital beam
        hdu_index=self.metainfo['firstHDU']-1
        if hdu_index<=0:
            print "ERROR:  no digital beam headers!"
            self.metainfo['nDigBeams']=0
            return False

        # make sure there is a BEAMINDX keyword
        if not 'BEAMINDX' in self.metainfo['hdulist'][hdu_index].header.keys():
            print "ERROR: No BEAMINDX defined!"
            self.metainfo['nDigBeams']=0
            return False

        # make sure this is a digital beam header
        if (not 'BEAMTYPE' in self.metainfo['hdulist'][hdu_index].header.keys())\
                or (self.metainfo['hdulist'][hdu_index].header['BEAMTYPE']!='DIGITAL'):
            print "ERROR: can't find the last digital beam header"
            self.metainfo['nDigBeams']=0
            return False

        self.metainfo['nDigBeams']=self.metainfo['hdulist'][hdu_index].header['BEAMINDX']+1

        return True

    # read the RF centre frequency
    def assignRFcentre(self):
        if not self.RFbeamDefined:
            self.metainfo['RFcentre']=0.0
            return False

        if self.metainfo['fitsversion']<=1.1:
            keyval=readKeywordValue(self.metainfo['hdulist'],'RF-FREQ')
            if keyval==None:
                keyval=readKeywordValue(self.metainfo['hdulist'],'FREQ')
            if keyval==None:
                return False
            self.metainfo['RFcentre']=eval(keyval)/1.0e6
            return True

        self.metainfo['RFcentre']=self.metainfo['hdulist'][self.RFbeamHDU].data.field(6)[0]/1.0e6
        
        return True

    # read the RF calibration frequency
    def assignRFcalfreq(self):
        if not self.RFbeamDefined:
            self.metainfo['RFcalfreq']=0.0
            return False

        if self.metainfo['fitsversion']<=1.1:
            print "WARNING:  I don't think I remember how to read the Calibration Frequency from old files"
            calfreq=readKeywordValue(self.metainfo['hdulist'],'CALFREQ')
            if calfreq==None:
                self.metainfo['RFcalfreq']=self.metainfo['RFcentre']
                return False
            self.metainfo['RFcalfreq']=eval(calfreq)
            return True

        self.metainfo['RFcalfreq']=self.metainfo['hdulist'][self.RFbeamHDU].data.field(8)[0]
        
        return True

    # assign observation title
    # sometimes the obstag is not an appropriate title
    # because it might be the source name for the subsequent observation
    # after calibration
    def assignTitle(self):
        #self.metainfo['title']=self.metainfo['hdulist'][0].header['TITLE']
        ttl=self.Filename().split('_')[0]
        self.metainfo['title']=ttl
        return True

    # assign the tileset positions
    #### see also tileset_positions.py
    def assignTilesetPositions(self):
        rotang_deg=131.2857 # measured mechanically and astronomically
                            # measured by Laurence Alsac 2012-06-22
                            # confirmed by Henrik ...
        rotang=math.radians(rotang_deg)

        len_units=" m"
        vivaldi_width=0.125

        tile_width=vivaldi_width*6/math.sqrt(2)
        tileset_width=4*tile_width
        array_width=4*tileset_width

        vivaldi_maxno_NS=23+1+23+1+23+1+23 # see Chenanceau notebook 2012-05-22
        vivaldi_maxseparation_NS=vivaldi_maxno_NS-1

        vivaldi_maxno_EW=4*24
        vivaldi_maxseparation_EW=vivaldi_maxno_EW-1


        # position matrix for 4x4 tileset array
        # 0,0 is lower left, and rotating by rotang
        tileset_positions=np.empty((self.nRCU(),2),dtype=float)
        tilesetno=0
        for i in range(4):
            y=i*tileset_width
            for j in range(4):
                x=j*tileset_width
                x_prime=x*math.cos(rotang)-y*math.sin(rotang)
                y_prime=x*math.sin(rotang)+y*math.cos(rotang)
                tileset_positions[tilesetno][0]=x_prime
                tileset_positions[tilesetno][1]=y_prime
                tilesetno+=1

        self.metainfo['tileset_positions']=tileset_positions
        return

    # assign the statistics object
    def assignStatisticsObject(self):
        # create a specific statistics object
        if self.metainfo['statstype']=='SSX':
            self.sx=SBStats(self.metainfo)
        elif self.metainfo['statstype']=='BSX':
            self.sx=BLStats(self.metainfo)
        elif self.metainfo['statstype']=='CSX':
            self.sx=CLStats(self.metainfo)
        else:
            print "don't know how to handle statistics of type: "+self.metainfo['statstype']
            self.sx=None
            return False

        return True

    # assign subband select map for all digital beams
    def assignSSMAP(self):
        if self.metainfo['nDigBeams']<=0:
            print "ERROR: no digital beams defined"
            self.metainfo['SSMAP']=None
            return False

        self.metainfo['SSMAP']=[]

        if self.metainfo['fitsversion']<=1.1:
            self.metainfo['SSMAP'].append(eval(readKeywordValue(self.metainfo['hdulist'],'SSMAP',0)))
            return True
        
        for digbeam in xrange(self.metainfo['nDigBeams']):
            hdu=digbeamHDU(self.metainfo,digbeam)
            if hdu==0: 
                self.metainfo['SSMAP'].append([None])
            else:
                ssmapstr=readKeywordValue(self.metainfo['hdulist'],'SSMAP',hdu)
                if ssmapstr.find(',')<0: # only one beamlet
                    self.metainfo['SSMAP'].append([ssmapstr])
                else:
                    self.metainfo['SSMAP'].append(eval(ssmapstr))
        return True

    # assign timestamp of a given data point as a datetime object
    # time is returned in UT, so an appropriate conversion is made if necessary
    # depending on the version of the fits file
    def assignTimestamps(self):
        self.metainfo['Timestamp']=[]
        ntime=0
        while ntime < self.nData():
            ntime_index=ntime+self.metainfo['firstHDU']

            timestamp=self.metainfo['hdulist'][ntime_index].header['TIME']
            if self.metainfo['isUT_timestamp']:
                dt_timestamp=str2dt(timestamp)
            else:
                dt_timestamp=str2dt(timestamp)-self.metainfo['ut2local']
                
            self.metainfo['Timestamp'].append(dt_timestamp)
            ntime+=1
        return True


    # assign parameters that are needed
    def assignParameters(self):
        # check the FITS file version number, to be used to determine time format
        if 'VERSION' in self.metainfo['hdulist'][0].header:
            self.metainfo['fitsversion']=self.metainfo['hdulist'][0].header['VERSION']
        else:
            self.metainfo['fitsversion']=0.0

        # total number of HDUs
        self.metainfo['nHDU']=len(self.metainfo['hdulist'])

        # first HDU with statistics data
        self.assignFirstHDU()

        # total number of statistics data
        if not self.assign_nData(): return False

        # for files before 2011-09-15, the timestamp is in UT
        # and then, for files after 2011-11-18, the timestamp is again in UT
        # default is for VERSION>=1.1
        self.strpfmt="%Y-%m-%d %H:%M:%SZ"
        self.strpfmt_long="%Y-%m-%d %H:%M:%S.%fZ"
        self.isUT_timestamp=True            
        if self.metainfo['fitsversion']<1.0:
            self.strpfmt="%Y-%m-%d %H:%M:%S.%f"
            self.isUT_timestamp=True
        elif self.metainfo['fitsversion']<1.1:
            self.strpfmt="%Y-%m-%d %H:%M:%S"
            self.isUT_timestamp=False

        self.metainfo['isUT_timestamp']=self.isUT_timestamp

        # assign timestamps
        self.assignTimestamps()

        # HDUs with the RF beam info
        self.assignRFbeamHDU()

        # project title
        self.assignTitle()

        # time of first data point
        self.assignStartTime()

        # number of RCUs active
        self.assign_nRCU()

        # number of digital beams
        self.assign_nDigitalBeams()

        # RF centre frequency
        self.assignRFcentre()

        # RF calibration frequency
        self.assignRFcalfreq()

        # time axis (number of integrations)
        self.mintime=0
        self.maxtime=self.metainfo['ndata']
        
        # tilesets used (counting from 0)
        tilesets=[x-1 for x in eval(self.metainfo['hdulist'][0].header['TILESETS'])]
        self.metainfo['tilesets']=tilesets

        # tileset positions
        self.assignTilesetPositions()

        # create the statistics object
        self.assignStatisticsObject()

        # starting times of different observation phases
        self.assignObsModeTimes()

        # subband select map for all digital beams
        self.assignSSMAP()

        return

    #################################################################
    # methods for returning values

    def nDigitalBeams(self):
        return self.metainfo['nDigBeams']

    def SSMAP(self,digbeam=0):
        # make sure digbeam is a valid number
        digbeam=DigitalBeam(self.metainfo,digbeam)
        if digbeam==None: return None
        return self.metainfo['SSMAP'][digbeam]

    # return the start time
    def startTime(self):
        return self.metainfo['startTime']

    # return the number of RCU
    def nRCU(self):
        return self.metainfo['nRCU']

    # return the RF centre frequency
    def RFcentre(self):
        return self.metainfo['RFcentre']

    # return the RF calibration frequency
    def RFcalfreq(self):
        return self.metainfo['RFcalfreq']

    # return observation title
    def Title(self):
        return self.metainfo['title']

    # return first HDU with data
    def FirstHDU(self):
        return self.metainfo['firstHDU']

    # return filename
    def Filename(self):
        return self.metainfo['filename']

    # return filesize
    def Filesize(self):
        return self.metainfo['filesize']

    # return full path name to file
    def FullFilename(self):
        return self.metainfo['fullpathname']

    # return which RFbeam A or B
    def RFbeam(self):
        return self.metainfo['RFbeam']

    # return the number of data points
    def nData(self):
        return self.metainfo['ndata']

    # return the statistics type
    def statsType(self):
        return self.metainfo['statstype']

    # verify that a selection is valid for RCU or DigBeam
    def verify_n(self,n):
        return self.sx.verify_n(n)

    # return hdu index for a digital beam
    def DigbeamHDU(self,digbeam=0):
        return digbeamHDU(self.metainfo,digbeam)

    # return the number of beamlets for a digital beam
    def nBeamlets(self,digbeam=0):
        return nBeamlets(self.metainfo,digbeam)
    
    # return FITS header
    def header(self,keyword="ALL",hdu=0):
        if self.metainfo['hdulist']==None:
            return 'header unavailable.  You must have read the pickle files only'

        if isinstance(keyword,int):
            hdu=keyword
            keyword="ALL"

        if hdu>=self.metainfo['nHDU']:
            return 'ERROR: given hdu is out of range: '+str(hdu)

        if keyword=="ALL":
            return self.metainfo['hdulist'][hdu].header
        
        if keyword in self.metainfo['hdulist'][hdu].header.keys():
            return self.metainfo['hdulist'][hdu].header[keyword]

        return 'keyword not found: '+keyword

    # write FITS header to file
    def header2file(self,filename='NONE',hdu=0):
        if filename=='NONE':
            filename=self.metainfo['filename'].replace('.fits','_header.txt')
        ofile=open(filename,'w')
        ofile.write(str(self.header('ALL',hdu)))
        ofile.close()
        print 'header has been written to text file : '+filename
        return

    # print out FITS comments
    def comments(self):
        keys=self.metainfo['hdulist'][0].header.keys()
        index=-1
        for key in keys: 
            index+=1
            if key!='COMMENT': continue
            comment=self.metainfo['hdulist'][0].header[index]
            print comment

        return

    # readjust given index to an index for the FITS file and
    # check input argument for max ntime
    def nTime(self,ntime):
        return nTime(self.metainfo,ntime)

    # return time of a given data point as a datetime object
    # time is returned in UT, so an appropriate conversion is made if necessary
    # depending on the version of the fits file
    def Timestamp(self,ntime):
        return Timestamp(self.metainfo,ntime)

    # return the frequency for a given subband number
    def SBnum2Freq(self,sbnum):
        return SBnum2Freq(self.metainfo,sbnum)

    # assign a frequency axis
    # for beamlet statistics, n=digbeam
    # for subband statistics, n=rcu
    # for crosslet statistics, n=ntime (a single frequency)
    def Freq(self,n=0):
        if self.statsType()=='CSX':
            ntime_index=self.nTime(n)
            return self.sx.Freq(ntime_index)

        print "assigning frequency axis"
        return self.sx.Freq(n)


    # assign data and return as a numpy array
    # for beamlet statistics, n=digbeam
    # for subband statistics, n=rcu
    # for crosslets, we need the az,el pointing
    # calibrate = None [default], 'normal' or 'redundancy'
    def Data(self,ntime,n=0,direction='x',calibrate=None,ignorepointing=False,npix=101,linear=False,simulate=False):
        # check if requested time is okay
        ntime_index=self.nTime(ntime)
        if ntime_index==None: return None
        if self.statsType()!='CSX':
            return self.sx.Data(ntime_index,n,direction,linear)

        if ignorepointing:
            az,el=(0.0,math.pi/2)
            print 'pointing ignored.  assuming zenith pointing'
        else:
            az,el=self.RFpointing(ntime)
            if az==None:
                print 'no pointing information available. assuming zenith pointing'
                az,el=(0.0,math.pi/2)
        return self.sx.Data(ntime_index,az,el,calibrate,npix=npix,simulate=simulate)

    # dump data to simple ascii file
    def rawout(self):
        return self.sx.rawout()

     # make the first part of the title and filename for plotting
    def mkPlotTitle(self):
        ttl=self.Title()\
            +' : Beam '+self.metainfo['RFbeam']
        # remove the Beam ID from the filename and put it back using internal info.
        filename=self.metainfo['filename'].replace('_BeamA','').replace('_BeamB','').replace('.fits','')
        filename+='_Beam'+self.metainfo['RFbeam']

        return ttl,filename

    # open a new plot window
    def openPlot(self,ttl,filename,subttl=None,figsize=None):
        if figsize==None:
            fig=plt.figure(figsize=self.metainfo['figsize'])
            fig.canvas.set_window_title('plt: '+ttl) 
            plt.suptitle(ttl,fontsize=16)
            if isinstance(subttl,str):
                plt.title(subttl,fontsize=14)
            return fig

        fig=plt.figure(figsize=figsize)
        fig.canvas.set_window_title('plt: '+ttl) 
        plt.suptitle(ttl,fontsize=6)
        if isinstance(subttl,str):
            plt.title(subttl,fontsize=4)

        return fig


    # plot a spectrum from a digital beam, at a given time
    # for beamlet statistics, n=digbeam
    # for subband statistics, n=rcu
    # calibrate = None [default], 'normal' or 'redundancy'
    def plotData(self,ntime,n=None,
                 direction='x',
                 subtitle=None,
                 pmin=None,
                 pmax=None,
                 calibrate=None,
                 npix=101,
                 fov=1.5,
                 sbnum=None,
                 ignorepointing=False,
                 linear=False,
                 xwin=True,
                 simulate=False):
        # check if requested time is okay
        ntime_index=self.nTime(ntime)
        if ntime_index==None: return None        
        
        if linear: power_unit='mW'
        else: power_unit='dBm'

        if ignorepointing:
            az,el=(0.0,math.pi/2)
            print 'ignoring pointing information.  assuming zenith pointing'
        else:
            az,el=self.RFpointing(ntime)
            if self.statsType()=='CSX' and az==None:
                print 'no pointing information.  assuming zenith pointing'
                az,el=(0.0,math.pi/2)
	
        ttl,filename=self.mkPlotTitle()
        # make the middle part of the filename
        if self.statsType()=='CSX':
            ttl2,filename2=self.sx.mkPlotTitle(ntime_index,direction,calibrate)
        else:
            ttl2,filename2=self.sx.mkPlotTitle(n,direction)
        if subtitle==None:
            subtitle=ttl2
        else:
            ttl+=ttl2
        filename+=filename2

        # make the last part of the title and filename
        ttl+=' at integration #'+str(ntime)+' ('+str(self.Timestamp(ntime))+' UT)'
        filename+='_time'+str("%05d" % ntime)+'.png'

        # make the plot
        if xwin:
            fig=self.openPlot(ttl,filename,subttl=subtitle)
        else:
            fig=self.openPlot(ttl,filename,subttl=subtitle,figsize=(4.0,3.0))
            

        if self.statsType()=='CSX':
            ret=self.sx.plotData(ntime_index,az,el,calibrate,pmin,pmax,npix,fov,sbnum,simulate)
        else:
            plt.xlabel('frequency / MHz')
            plt.ylabel('power / '+power_unit)
            ret=self.sx.plotData(ntime_index,n,direction,linear)
            plt.grid()
	
        # finish the plot and save to a file
        ret=plt.savefig(filename,format='png',dpi=100,bbox_inches='tight')
        if xwin: 
            ret=plt.show()
        else:
            ret=plt.clf()

        return ret

    # make an average dynamic spectrum
    # mostly useful for subband stats,
    # maybe for beamlet stats, should average over direction instead?
    def avgDynamicSpec(self):        
        n=0
        N=self.verify_n(n)
        if N==None:return None
        start=self.sx.DynamicSpec(N)
        # make a new array to hold the average.
        avg=np.empty(start.shape) 
        avg+=start
        # free up some memory
        del start
        n+=1
        N=self.verify_n(n)
        while not N==None:
            avg+=self.sx.DynamicSpec(N)
            n+=1
            N=self.verify_n(n)
            
        return avg/n
    
    
    # plot a dynamic spectrum
    # for beamlet statistics, n=digbeam
    # for subband statistics, n=rcu
    def plotDynamicSpec(self,n=0,direction='x',vmin=None,vmax=None,show=True,subtitle=None):

        if n=='avg':
            X=self.avgDynamicSpec()
        else:
            X=self.sx.DynamicSpec(n,direction)
        if X==None: return False

        # assign vmin, if none given.  ignore zero values.
        if vmin==None: vmin=min(X[X>0.0])
        freq=self.Freq(n)
        minfreq=min(freq)
        maxfreq=max(freq)

        ttl,filename=self.mkPlotTitle()
        # make the middle part of the filename
        ttl2,filename2=self.sx.mkPlotTitle(n,direction)
        ttl+=ttl2
        filename+=filename2
        filename+='.png'

        ret=self.openPlot(ttl,filename,subttl=subtitle)
        plt.xlabel('frequency / MHz')
        plt.ylabel('time / seconds since '+str(self.startTime()))
        plt.imshow(X,aspect='auto',
                   vmin=vmin,vmax=vmax,
                   extent=(minfreq,maxfreq,self.maxtime,self.mintime))
        plt.colorbar()            
        ret=plt.savefig(filename,format='png',dpi=100,bbox_inches='tight')
        if show: plt.show()
        return ret


    # return the timeline data for a given frequency range and digital beam/RCU
    # this is calculated in tools_embraceStats
    # for beamlet statistics, n=digbeam
    # for subband statistics, n=rcu
    def CalculateTimeline(self,freq0=None,bwidth=None,nchans=None,n=None,direction='x'):
        # if freq0 not given, use RFcalfreq
        if freq0==None:
            freq0=self.RFcalfreq()

        # if bandwidth not given, use 0
        if bwidth==None:
            bwidth=0.0

        return self.sx.Timeline(freq0,bwidth,nchans,n,direction)

    # the work is done in CalculateTimeline() above for BSX and SSX
    # for CSX, call sx.Timeline() directly
    def Timeline(self,freq0=None,bwidth=None,nchans=None,n=0,direction='x',
                 plot='beamlet',sbnum=256,ignore=False,ignorepointing=False,reference=None):
        if self.statsType()=='CSX':
            return self.sx.Timeline(plot,n,direction,sbnum,ignore,ignorepointing,reference)

        return self.CalculateTimeline(freq0,bwidth,nchans,n,direction)


    # read the time of the expected peak of the driftscan
    # it's to be found either in the digital beam pointing text file
    # or it may be a comment in the FITS file itself
    # it might also be found in the submitlog
    def expectedPeakTime(self,direction='x'):
        if direction=='x' or direction=='X' or direction==0:
            peaktxt='! peak should be at '
        else:
            peaktxt='! the y-direction should have a peak at '

        peakinfo=None
        peakinfolist=[]

        # try to read from the digital beam text pointing file
        digfilename=self.metainfo['filename'].replace('_bsx_','_digital_').replace('.fits','.txt')
        if(os.path.exists(digfilename)):
            dighandle=open(digfilename,'r')
            lines=dighandle.readlines()
            for line in lines:
                if line.find(peaktxt)==0:
                    peakinfo=line
                    peakinfolist.append(peakinfo)
                    print "found expected peak time in file: ",digfilename
            dighandle.close()

        # try to read from the FITS file comments 
        keys=self.metainfo['hdulist'][0].header.keys()
        index=0
        for key in keys: 
            if key=='COMMENT':
                comment=self.metainfo['hdulist'][0].header[index]
                if comment.find(peaktxt)==0:
                    peakinfo=comment
                    peakinfolist.append(peakinfo)
                    print "found expected peak time in FITS file",self.metainfo['filename']
            index+=1

        if len(peakinfolist)==0:
            print "did not find expected peak time for direction ",direction
            return None

        drift_t=[]
        dt_startobs=self.startTime()
        for peakinfo in peakinfolist:
            tstr=peakinfo.replace(peaktxt,'')
            tstr=tstr[0:19]
            dtpeak=dt.datetime.strptime(tstr,"%Y-%m-%d %H:%M:%S")
            dtdelta=dtpeak-dt_startobs
            drift_t.append(float(dtdelta.seconds+dtdelta.microseconds/1e6))
        return drift_t

    # plot the expected peak times read from the digital pointing files
    def plotExpectedPeakTimes(self,drift_ty):
        Xdrift_t=self.expectedPeakTime(direction='x')
        Ydrift_t=self.expectedPeakTime(direction='y')

        style_index=0
        if Xdrift_t!=None:
            for drift_t in Xdrift_t:

                drift_tx=[drift_t,drift_t]
                plt.plot(drift_tx,drift_ty,
                         linestyle=self.metainfo['linestyles'][style_index],
                         color=self.metainfo['colours'][style_index])

                plt.text(drift_t,drift_ty[0],
                         'expected time of maximum',
                         fontsize=14,
                         verticalalignment='bottom',
                         rotation='vertical')
                style_index+=1

        if Ydrift_t != None:
            for drift_t in Ydrift_t:
                drift_tx=[drift_t,drift_t]
                plt.plot(drift_tx,drift_ty,
                         linestyle=self.metainfo['linestyles'][style_index],
                         color=self.metainfo['colours'][style_index])
        return


    # return the total span of time in seconds given a timedelta
    def tot_seconds(self,delta):
        tsecs=delta.days*24*3600 + delta.seconds + delta.microseconds*1e-6
        return tsecs

    # get the start times of different observation modes from the FITS comments
    # this also finds expected peak times, duplicating expectedPeakTimes()
    def assignObsModeTimes(self):
        # return a dictionary of lists with the obs mode times
        obsmodetimes=dict.fromkeys([
                'trackstart',
                'trackend',
                'RFcalstart',
                'digcalstart',
                'xpeak',
                'ypeak',
                'driftstart',
                'j2000trackstart',
                'offstart'])
        for key in obsmodetimes.keys():obsmodetimes[key]=[]

        # from the FITS comments
        # make a loop to read all the  times
        keys=self.metainfo['hdulist'][0].header.keys()
        cards=self.metainfo['hdulist'][0].header.cards
        index=-1
        for card in cards: 
            index+=1
            if card.keyword!='COMMENT': continue

            comment=self.metainfo['hdulist'][0].header[index].replace("&'",'')

            match=re.search('^.*tracking by J2000 at ',comment)
            if match:
                datestr=comment.replace(match.group(),"")
                obsmodetimes['j2000trackstart'].append(str2dt(datestr))
                continue

            match=re.search('^.*tracking.*(start|beginn)ing at ',comment)
            if match:
                datestr=comment.replace(match.group(),"")
                obsmodetimes['trackstart'].append(str2dt(datestr))
                continue
        
            match=re.search('^.*tracking.*ending at ',comment)
            if match:
                datestr=comment.replace(match.group(),"")
                obsmodetimes['trackend'].append(str2dt(datestr))
                continue

            match=re.search('^.*RF calibration beginning at ',comment)
            if match:
                datestr=comment.replace(match.group(),"")
                obsmodetimes['RFcalstart'].append(str2dt(datestr))
                continue

            match=re.search('^.*digital calibration beginning at ',comment)
            if match:
                datestr=comment.replace(match.group(),"")
                obsmodetimes['digcalstart'].append(str2dt(datestr))
                continue

            match=re.search('^.*drift scan beginning at ',comment)
            if match:
                datestr=comment.replace(match.group(),"")
                obsmodetimes['driftstart'].append(str2dt(datestr))
                continue

            match=re.search('^.*peak should be at ',comment)
            if match:
                datestr=comment.replace(match.group(),"")
                obsmodetimes['xpeak'].append(str2dt(datestr))
                continue

            match=re.search('^.*the y-direction should have a peak at ',comment)
            if match:
                datestr=comment.replace(match.group(),"")
                obsmodetimes['ypeak'].append(str2dt(datestr))
                continue

            match=re.search('^.*OFF position starting at ',comment)
            if match:
                datestr=comment.replace(match.group(),"")
                obsmodetimes['offstart'].append(str2dt(datestr))
                continue

            match=re.search('^.*pointing at empty sky.*starting at ',comment)
            if match:
                datestr=comment.replace(match.group(),"")
                obsmodetimes['offstart'].append(str2dt(datestr))
                continue

        self.metainfo['ObsModeTimes']=obsmodetimes
        return True

    # return ObsModeTimes
    def ObsModeTimes(self):
        return self.metainfo['ObsModeTimes']

    # plot a label marking an event...
    def pltTimeLabel(self,t,label=None,y=None):
        if y==None: y=[80,120]
        if label==None:label=str(t)

        text_y=y[1]-0.1*(y[1]-y[0])
        text_size=10
        if isinstance(t,dt.datetime):
            delta=t-self.startTime()
            tsecs=self.tot_seconds(delta)
        else:
            tsecs=int(t)

        plt.plot([tsecs,tsecs],y,color='grey')
        plt.text(tsecs+10,text_y,label,fontsize=text_size,rotation=90,va='top')
        return True


    # plot the time boundaries of the different phases of an observation
    def pltObsModeTimes(self,y=[80,120]):

        text_y=y[1]-0.1*(y[1]-y[0])
        text_size=10
        
        for t in self.ObsModeTimes()['trackstart']:
            delta=t-self.startTime()
            tsecs=self.tot_seconds(delta)    
            plt.plot([tsecs,tsecs],y,color='grey')
            plt.text(tsecs+10,text_y,'tracking by AZEL',fontsize=text_size,rotation=90,va='top')

        for t in self.ObsModeTimes()['trackend']:
            delta=t-self.startTime()
            tsecs=self.tot_seconds(delta)    
            plt.plot([tsecs,tsecs],y,color='grey',linestyle='--')

        for t in self.ObsModeTimes()['RFcalstart']:
            delta=t-self.startTime()
            tsecs=self.tot_seconds(delta)    
            plt.plot([tsecs,tsecs],y,color='green',linestyle=':')
            plt.text(tsecs+10,text_y,'RF calibration',fontsize=text_size,rotation=90,va='top')

        for t in self.ObsModeTimes()['digcalstart']:
            delta=t-self.startTime()
            tsecs=self.tot_seconds(delta)    
            plt.plot([tsecs,tsecs],y,color='green',linestyle=':')
            plt.text(tsecs+10,text_y,'digital calibration',fontsize=text_size,rotation=90,va='top')

        for t in self.ObsModeTimes()['xpeak']:
            delta=t-self.startTime()
            tsecs=self.tot_seconds(delta)    
            plt.plot([tsecs,tsecs],y,color='green',linestyle='--')
            plt.text(tsecs+10,text_y,'expected peak',fontsize=text_size,rotation=90,va='top')

        for t in self.ObsModeTimes()['ypeak']:
            delta=t-self.startTime()
            tsecs=self.tot_seconds(delta)    
            plt.plot([tsecs,tsecs],y,color='green',linestyle='--')
            plt.text(tsecs+10,text_y,'expected peak (y-direction)',fontsize=text_size,rotation=90,va='top')

        for t in self.ObsModeTimes()['driftstart']:
            delta=t-self.startTime()
            tsecs=self.tot_seconds(delta)    
            plt.plot([tsecs,tsecs],y,color='green',linestyle='--')
            plt.text(tsecs+10,text_y,'drift',fontsize=text_size,rotation=90,va='top')

        for t in self.ObsModeTimes()['j2000trackstart']:
            delta=t-self.startTime()
            tsecs=self.tot_seconds(delta)    
            plt.plot([tsecs,tsecs],y,color='grey')
            plt.text(tsecs+10,text_y,'tracking by J2000',fontsize=text_size,rotation=90,va='top')

        for t in self.ObsModeTimes()['offstart']:
            delta=t-self.startTime()
            tsecs=self.tot_seconds(delta)    
            plt.plot([tsecs,tsecs],y,color='green',linestyle='--')
            plt.text(tsecs+10,text_y,'off source',fontsize=text_size,rotation=90,va='top')

        return

    # plot timeline for a given frequency range, for all Digital Beams
    #  frequency range is given as centre frequency (variable: freq0) 
    #  and a total bandwidth (variable: bwidth)
    # we plot for all RCUs or for all digital beams.  ie. there is no 'n' argument
    # the 'drift' keyword is a legacy.  
    #   force drift=True if you want to read the pointing files
    def plotTimeline(self,freq0=None,bwidth=None,nchans=None,
                     plot='total power',digbeam=0,direction='x',sbnum=256,
                     ignore=False,ignorepointing=False,reference=None,
                     onoff=False,
                     pmin=None,pmax=None,drift=False,
                     tmin=None,tmax=None,
                     elevation=False,amin=None,amax=None,subtitle=None,
                     grid=False,linear=False):

        if linear: power_unit='mW'
        else: power_unit='dBm'


        # if freq0 not given, use RFcalfreq
        if freq0==None:
            freq0=self.RFcalfreq()

        # if bandwidth not given, use 0
        if bwidth==None:
            bwidth=0.0

        # title for plot
        ttl,filename=self.mkPlotTitle()

        # make the middle part of the title and filename
        # all digital beams, both directions are plotted.  give "None" as direction, and digbeam
        ttl2,filename2=self.sx.mkPlotTitle(n=None,direction=None)
        ttl+=ttl2
        filename+=filename2

        # make the last part of the title and filename
        if self.statsType()=='CSX' and plot.upper()=='PHASE':
            if reference==None:reference=0
            ttl+=': Phase Difference to Tileset '+str(reference)
            filename+='_phasetimeline'
            ylabel='phase / degrees  (adjusted to avoid jumps from -180 to 180)'
        else:
            ttl+=': Integrated power'
            filename+='_timeline'
            ylabel='integrated power / '+power_unit

        if onoff:
            ylabel='ON-OFF power / '+power_unit

        # find the true bandwidth to be used (adjusted by channel width)
        if self.statsType()!='CSX':
            start_chan,end_chan,nbwidthchans=bandwidthChannels(self.sx,freq0,bwidth,nchans,None)
            bwidth=nbwidthchans*self.metainfo['chanwidth']
            ttl+=' at '+str("%6.1f" % freq0)+'MHz  +/-'+str("%7.2f" % (bwidth/2.0))+'MHz'
            filename+=str("%06.1f" % freq0)
        else:
            filename+=str("%06.1f" % self.SBnum2Freq(sbnum))
            

        if onoff: filename+='_ONOFF'
        filename+='.png'

        fig=self.openPlot(ttl,filename,subttl=subtitle)
        ax1 = fig.add_subplot(111)

        ax1.set_xlabel('time / seconds since '+str(self.startTime())+' UT')
        ax1.set_ylabel(ylabel)


        if self.statsType()=='CSX':
            minval,maxval=self.sx.plotTimeline(digbeam,direction,sbnum,plot,ignore,ignorepointing,reference)
        else:
            minval,maxval=self.sx.plotTimeline(freq0,bwidth,nchans,onoff,linear)            

        axes=[0,self.metainfo['ndata'],minval,maxval]
        if isinstance(pmin,float) or isinstance(pmin,int):
            axes[2]=pmin
        if isinstance(pmax,float) or isinstance(pmax,int):
            axes[3]=pmax
        if isinstance(tmin,float) or isinstance(tmin,int):
            axes[0]=tmin
        if isinstance(tmax,float) or isinstance(tmax,int):
            axes[1]=tmax
        ax1.axis(axes)

        if self.statsType()=='CSX' and self.calibrated():
            label='calibrated at '+isodate(self.Timestamp(self.sx.calibrated_ntime))
            self.pltTimeLabel(self.sx.calibrated_ntime,label=label,y=[axes[2],axes[3]])
        
        # legacy method.
        # try read expected peak time from the digital pointing text file
        # later, we will try also from the FITS comments
        if drift:
            self.PlotExpectedPeakTimes([axes[2],axes[3]])

        # plot the vertical lines giving the time boundaries for the different obs modes
        # the times are given in the FITS file primary header comments
        # this possibly repeats the drift scan boundaries already plotted from the digital pointing files
        # (for historical reasons)
        self.pltObsModeTimes(axes[2:])
            
        plt.legend(prop={'size':12},loc='upper right',bbox_to_anchor=(0, 0, 1.05, 1.05))

        if elevation:
            cmd_time,az1,el1,az2,el2=readPointing()
            # time in seconds from first time
            tdelta=[]
            for t in cmd_time:
                tdelta.append(self.tot_seconds(t-self.startTime()))

            ax2 = ax1.twinx()
            minval=min(el1)
            maxval=max(el1)
            axes=[0,self.nData(),minval,maxval]
            if isinstance(amin,float) or isinstance(amin,int):
                axes[2]=amin
            if isinstance(amax,float) or isinstance(amax,int):
                axes[3]=amax
            ax2.axis(axes)


            ax2.plot(tdelta, el1, linestyle='', color=self.metainfo['colours'][2], marker='.')
            ax2.set_ylabel('elevation / degrees', color='k')
            for tl in ax2.get_yticklabels():
                tl.set_color('k')

        if grid: plt.grid()
        ret=plt.savefig(filename,format='png',bbox_inches='tight')
        ret=plt.show()
        return ret
    
    def printPointing(self,digbeam=0,beamtype='dig'):
        t,a1,a2,a3,a4=readPointing(self.metainfo,digbeam,beamtype)
        i=0
        for tpointing in t:
            print isodate(tpointing),\
                str(" %7.3f," % a1[i]),\
                str(" %7.3f," % a2[i]),\
                str(" %7.3f," % a3[i]),\
                str(" %7.3f" % a4[i])
            i+=1
        print '\n'
        return

    # alias to return pointing for RF beam
    def RFpointing(self,ntime):
        return pointing(self.metainfo,ntime,beamtype='RF')

    # alias to return pointing for dig beam
    def digpointing(self,ntime,digbeam=0,direction='x'):
        return pointing(self.metainfo,ntime,digbeam,'dig',direction)

    # return the subband number for a crosslet statistics
    def SBnum(self,ntime):
        if self.statsType()!='CSX':return None

        # check if requested time is okay
        ntime_index=self.nTime(ntime)
        if ntime_index==None: return None        

        return self.sx.SBnum(ntime_index)
    


    # plot the sub band numbers that are returned in crosslet statistics
    def plotSBnum(self,pmin=0,pmax=511):
        if self.statsType()!='CSX':return None
        sbnum=self.sx.getSBnum()

        # title for plot
        ttl,filename=self.mkPlotTitle()
        filename+='_subband-number.png'
        subtitle='Sub band number for crosslet statistics'
        fig=self.openPlot(ttl,filename,subttl=subtitle)
        plt.xlabel('time / seconds since '+self.startTime().isoformat(' ')+' UT')
        plt.ylabel('sub band number')
        plt.plot(sbnum)
        self.pltObsModeTimes([pmin,pmax])
        axes=[0,self.metainfo['ndata'],pmin,pmax]
        ax1 = fig.add_subplot(111)
        ax1.axis(axes)

        ret=plt.savefig(filename,format='png',bbox_inches='tight')
        ret=plt.show()
        return ret

    # return a visibility matrix for a given time
    def UV(self,ntime):
        if self.statsType()!='CSX':return None

        # check if requested time is okay
        ntime_index=self.nTime(ntime)
        if ntime_index==None: return None        

        return self.sx.UV(ntime_index)
    
    #find redundant baselines and group them for redundancy calibration
    def find_redundant_baselines(self, Darray = None,plot_baselines = False):
    	"""
	Find redundant baselines and group them for preparing a redundancy calibration.
	Inputs:
		Darray: the array is made of Darray x Darray antennas (default to Darray = sqrt(nRCU) in embraceStats, implying it's a square array)
		plot: False [default] or True for plotting the different redundant baselines groups on a 2D graph of the array
	Outputs:
		n_baseline_redundant_nonunique: total number of redundant baselines
		i_baseline_redundant : only unique visibilities indices, for visibilities that have redundant baselines
		baseline_elements_index : list of indices [i,j] corresponding to two antennas (i.e. between 0 and 15 for tilesets) for each baselines   
    	"""
    	
    	if self.statsType()!='CSX': return None
    	
    	if Darray == None:
    		Darray = int(np.sqrt(len(self.metainfo['tilesets'])))
    		
    	out = self.sx.find_redundant_baselines(Darray,plot_baselines)
    	
    	return out 
    
    # do a redundancy calibration of amplitude gains and phases
    def redundancy_cal(self,acm,Darray, n_baseline_redundant_nonunique, n_baseline_redundant, i_baseline_redundant, baseline_elements_index, XX, YY, fit_res=None, weight=None):
	"""
	Redundancy calibration function, to be used in skymap() as calibrate='redundancy' besides of 'normal' calibration.
	The redundancy calibration is realized through a non linear least square method (optimize.leastsq())
	It calculates the amplitude gain and phase correction, store it in a complex matrix, and put this complex matrix in cx.sx.PhaseCorrectionMatrix
	Inputs:
		acm: crosslet matrix
		Darray, n_baseline_redundant_nonunique, n_baseline_redundant, i_baseline_redundant, baseline_elements_index, XX, YY: outputs of find_redundant_baselines()
		fit_res: None [default] or existing solution from a previous optimize.leastsq() to set the guesses of the nonlinear optimization
		weight: weight matrix for digital beam pointing, if != None it will be used to constraint the redundancy model to a point source at the middle of FOV
	Outputs:
		Correction matrix to be put in cx.sx.PhaseCorrectionMatrix
	"""    
	if self.statsType()!='CSX': return None
	    
	Mcorr = self.sx.redundancy_cal(acm,Darray, n_baseline_redundant_nonunique, n_baseline_redundant, i_baseline_redundant, baseline_elements_index, XX, YY, fit_res=None)
	self.sx.PhaseCorrectionMatrix = Mcorr
	return Mcorr
    
    # clear calculated results
    def clearcal(self):
        return self.sx.clearcal()

    # check if calibrated
    def calibrated(self):
        return self.sx.calibrated()

    # return offset from expected timestamp
    def TimestampOffset(self):
        deltaT=np.zeros(self.nData())
        for n in range(self.nData()):
            deltaT[n]=tot_seconds(self.Timestamp(n)-self.startTime())-n
        if max(deltaT)>0 or min(deltaT)<0:
            print "WARNING: time anomaly"
        return deltaT

    # plot offset from expected timestamp
    def plotTimestampOffset(self,tmin=None,tmax=None,omin=None,omax=None):
        pngfile=self.Filename().replace('.fits','')+'_TimestampOffset.png'
        ttl='Offset from expected Timestamp: '+self.Filename().replace('.fits','')
        fig=self.openPlot(ttl,pngfile)
        plt.ylabel('timestamp offset / seconds')
        plt.xlabel('time / seconds since '+isodate(self.startTime()))
        deltaT=self.TimestampOffset()
        if tmin==None:tmin=0
        if tmax==None:tmax=self.nData()
        if omin==None:
            omin=min(deltaT)
            if abs(omin)<2:omin=-2
        if omax==None:
            omax=max(deltaT)
            if abs(omax)<2:omax=2
        axes=[tmin,tmax,omin,omax]

        ax=fig.add_subplot(111)
        ax.axis(axes)
        plt.plot(deltaT)
        ret=plt.savefig(pngfile,format='png',bbox_inches='tight')
        ret=plt.show()
        
        return ret
