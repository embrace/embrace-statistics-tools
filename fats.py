'''
$Id: fats.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed 26 Jun 2013 10:33:47 CEST

methods for reading very big FITS files without overloading the memory

pyfits seems to load the whole thing, despite what they say

so don't use pyfits.open

as much as possible, work is done by child labourers, who return memory

$modified: Thu 25 Jul 2013 14:58:39 CEST
  abandoning any processing.  just find/print headers,
  find a logical place to split the file

'''
import os
import pyfits as fits

'''
hdulist=None
nHDU=None
headers=None
filename=None
ifile=None

def __init__():
    return

def __del__():
    return

def _open(_filename):
    ifile=open(_filename,'r')
    return ifile

'''

def loadheader(ifile):
    '''
    read and load header.  assume ifile is in the correct location
    '''
    #print 'loading header at position: ',ifile.tell()
    card=None
    key=None
    header={}
    comment={}
    value={}
    card=ifile.read(80)
    while key!='END' and card!='':
        #print card
        key=card.split('=')[0].strip()
        if key=='END': break

        if card.startswith('COMMENT')\
                or card.startswith('HISTORY')\
                or card.startswith('CONTINUE'):
            card=ifile.read(80)
            continue

        if card.find('=')<0:
            card=ifile.read(80)
            continue

        # check for string value
        strstart=card.find("'")
        if strstart>=0:
            strend=card.rfind("'")
            val=card[strstart+1:strend]
            tmp=card[strend:]

        else:
            tmp=card.split('=')[1].split('/')
            val=tmp[0].strip()
            
            if val=='T': val=True
            elif val=='F': val=False
            else: val=eval(val)
            tmp=card.split('=')[1]

        if tmp.find('/')>=0:
            com=tmp.split('/')[1]
        else:
            com=''

        value[key]=val
        comment[key]=com
        card=ifile.read(80)

    return value,comment

    
def findheader(ifile):
    card=ifile.read(80)
    key=card.split('=')[0].strip()
    while card!='' and key!='SIMPLE' and key!='XTENSION':
        card=ifile.read(80)
        key=card.split('=')[0].strip()

    ifile.seek(-80,1)
    headerpos=ifile.tell()
    #print "found header at position: ",headerpos
    return headerpos 


def getheaderlist(filename):
    if not os.path.exists(filename):
        print "error! file not found: ",filename
        return

    filesize=os.path.getsize(filename)
    ifile=open(filename,'r')
    headerpos=[]
    firstHDU=None
    while ifile.tell() < filesize:
        findheader(ifile)
        headerpos.append(ifile.tell())
        value,comment=loadheader(ifile)
        if not 'HDUTYPE' in value.keys():
            print 'no HDUTYPE in header #',len(headerpos)-1
            continue
        if firstHDU==None:
            hdutype=value['HDUTYPE'].strip()
            if hdutype=='BSX'\
                    or hdutype=='SSX'\
                    or hdutype=='CSX':
                firstHDU=len(headerpos)-1
                print 'first beam data HDU: ',len(headerpos)-1,' begins at position: ',headerpos[len(headerpos)-1]
        

    ifile.close()
    print "got header list: number of headers=",len(headerpos)
    return headerpos,firstHDU
