#!/usr/bin/env python
'''
$Id: plot_longdrift.py
$auth: Steve Torchinsky <steve.torchinsky@obspm.fr>
$created: Wed 30 Nov 2016 10:18:25 CET
$license: GPLv3 or later, see https://www.gnu.org/licenses/gpl-3.0.txt

          This is free software: you are free to change and
          redistribute it.  There is NO WARRANTY, to the extent
          permitted by law.

read a fits file and produce pickles, and merge pickles
this script will be called in a loop by a bash shell in order to clear memory after each operation
                                        SO CRAP.

other defs here to work with the data afterwards
this was developed for the CygA driftscan 23/24 November 2016 to compare to PAON4

'''
import sys,os
import pickle as PyPickle
import glob

from tools_embraceStats import *
from embraceStats import embraceStats as estats

def usage():
    print "usage: ",sys.argv[0]," --action=ACTION [filename]"
    print "ACTION=help (print this message, -h and --help will also work)"
    print "ACTION=loadnpickle (load a fits file and create corresponding pickles)"
    print "ACTION=define (just import the definitions.  this is the default action)"
    
    return None


# parse arguments
def parseargs():
    ''' parse the arguments
    '''
    arguments={}
    arguments['filename']='__None__'
    arguments['action']='define'
    for arg in sys.argv[1:]:
        if arg.find("--action=")==0:
            action=arg.split('=')[1]
            arguments['action']=action
            continue
 
        if os.path.exists(arg):
            arguments['filename']=arg
            continue

        if arg=="-h" or arg=="--help":
            arguments['action']='help'
            

    return arguments


def loadnpickle(arguments):
    ''' load a bunch of EMBRACE fits files and pickle them
    '''
    sx=estats(arguments['filename'])
    if sx.statsType()=='SSX':
        for n in range(16):
            X=sx.sx.DynamicSpec(n)
        ret=pickle(sx.metainfo,data=sx.sx.dynamicspecarray,datatype='dynamicspecarray')

    return True


def readpickle(rootname):
    ''' read an EMBRACE pickle
    '''

    picklefile=rootname+'.metainfo.pickle'
    if not os.path.exists(picklefile):
        print 'file not found: ',picklefile
        metainfo=None
    else:
        print "reading pickle file: ",picklefile
        ipickle=open(picklefile,'r')
        metainfo=PyPickle.load(ipickle)
        ipickle.close()

    picklefile=rootname+'.pickle'
    if not os.path.exists(picklefile):
        print 'file not found: ',picklefile
        data=None
    else:
        print "reading pickle file: ",picklefile
        ipickle=open(picklefile,'r')
        data=PyPickle.load(ipickle)
        ipickle.close()


    return metainfo,data
        

def readpickles(globpattern):
    ''' read a bunch of EMBRACE pickles
    '''

    dynspecs=[]
    metainfo=[]

    files=glob.glob(globpattern+".pickle")
    files.sort()
    for F in files:
        fname=F.replace('.pickle','')
        info,data=readpickle(fname)
        dynspecs.append(data)
        metainfo.append(info)

        
    return metainfo,dynspecs
    
def mergeDynspecs(dynspecs):
    ''' take the dynamic specs read from the pickle split and merge them together
    the argument is a list of a list of dynamic spectra (one for each RCU, for each pickle)
    '''

    # the return will be the average dynamic spectrum of the 16 RCU
    npickles=len(dynspecs)
    nrcu=16

    avgDynspecs=[]
    
    for n in range(npickles):
        shape=dynspecs[n][0].shape
        avgX=np.zeros(shape)
        for m in range(nrcu):
            avgX=avgX+dynspecs[n][m]
        avgX=avgX/nrcu
        avgDynspecs.append(avgX)


    # now we want to concatenate all the avg dynspecs into one massive dynspec.
    totalavgDynspec=avgDynspecs[0]
    for n in 1+np.array(range(npickles-1)):
        X=np.concatenate((totalavgDynspec,avgDynspecs[n]))
        totalavgDynspec=X

        
    return totalavgDynspec
    

def readmergedpickles(filename=None):
    if filename==None:filename='Sun--CygA_BeamA_ssx_20161123-1126'
    metainfo=None
    statstype=None
    X=None
    
    picklename=filename+'.metainfo.pickle'
    if not os.path.exists(picklename):
        picklename=filename+'.0000.metainfo.pickle'
    if not os.path.exists(picklename):
        print 'could not find pickle file for metainfo: '+picklename
    else:
        ipick=open(picklename,'r')
        metainfo=PyPickle.load(ipick)
        ipick.close()

        
    if metainfo != None:
        statstype=metainfo['statstype']

    if statstype=='SSX':
        picklename=filename+'.total-avg-dynspec.pickle'
    else:
        picklename=filename+'.pickle'

    if not os.path.exists(picklename):
        print 'could not find pickle file for dynamic spectrum: '+picklename
    else:
        ipick=open(picklename,'r')
        X=PyPickle.load(ipick)
        ipick.close()

    return metainfo,X

def plotAvgDynamicSpec(metainfo,X,satellites=None,vmin=None,vmax=None):
    statstype=metainfo['statstype'].lower()
    startTime=metainfo['startTime']
    ndata=X.shape[0]
    endTime=startTime+dt.timedelta(seconds=ndata)

    startFreq=metainfo['RFcentre']-50
    endFreq=startFreq+100
    
    plt_extents=[startFreq,endFreq,mdates.date2num(endTime),mdates.date2num(startTime)]
    src=metainfo['filename'].split('_')[0].split('--')[1]

    pngname='EMBRACE-Nancay_driftscan-'+src+'_'+statstype+'_'+metainfo['startTime'].strftime('%Y%m%dT%H%M')+'.png'
    ttl='EMBRACE@Nancay: driftscan of '+src+' average of all tilesets'


    # start plotting
    fig=plt.figure(figsize=(8.0,12.80))
    fig.canvas.set_window_title('plt: '+ttl)
    ax = fig.add_subplot(111)
    plt.suptitle(ttl,fontsize=16)
    ax.imshow(X,extent=plt_extents,aspect='auto',vmin=vmin,vmax=vmax)
    ax.yaxis_date()
    date_format = mdates.DateFormatter('(%d) %H:%M:%S')
    ax.yaxis.set_major_formatter(date_format)
    ax.set_xlabel('Frequency / MHz')
    ax.set_ylabel('Date (2016-11-23/24)')

    if not satellites==None:
        for passage in satellites:
            plt.text(metainfo['RFcentre']+7,passage['date'],passage['satellite'],
                     fontsize=14,
                     verticalalignment='center')


    plt.savefig(pngname,format='png',dpi=100,bbox_inches='tight')
    plt.show()
    return

def plot_avgDynamicSpec_20161123():
    """
    do all the necessary to plot the dynamic spectrum from 2016/11/23-24
    """
    metainfo,X=readmergedpickles(filename='Sun--CygA_BeamA_ssx_20161123-1126')
    if metainfo==None:return

    passages=[]
    #passages.append({'date':str2dt('2016-11-23 14:15'),'satellite':'GPS BIIF-7'})
    passages.append({'date':str2dt('2016-11-23 16:58'),'satellite':'Galileo GSAT0209'})
    passages.append({'date':str2dt('2016-11-23 19:08'),'satellite':'Galileo GSAT0205'})
    #passages.append({'date':str2dt('2016-11-24 01:52'),'satellite':'GPS BIIRM-5'})
    passages.append({'date':str2dt('2016-11-24 03:39'),'satellite':'Galileo GSAT0204'})
    #passages.append({'date':str2dt('2016-11-24 09:48'),'satellite':'GPS BIIR-3'})
    #passages.append({'date':str2dt('2016-11-24 12:17'),'satellite':'GPS BIIF-8 (?})'})
    passages.append({'date':str2dt('2016-11-24 17:41'),'satellite':'Galileo GSAT0103'})
    passages.append({'date':str2dt('2016-11-24 19:57'),'satellite':'Galileo GSAT0210'})
    passages.append({'date':str2dt('2016-11-24 09:10'),'satellite':'Galileo GSAT0201'})

    plotAvgDynamicSpec(metainfo,X,satellites=passages,vmin=70,vmax=80)
    return metainfo,X,passages

def plot_satImpactParameter_20161123(metainfo,X):
    startTime=metainfo['startTime']
    endTime=startTime+dt.timedelta(seconds=X.shape[0])
    tracks=[]
    for sat in ['GSAT0209','GSAT0205','GSAT0204','GSAT0103','GSAT0210','GSAT0201','GSAT0203']:
        tracks.append(sattrack(sat,start=startTime,end=endTime))

    trace=X[:,147]
    dates=[]
    for n in range(X.shape[0]):
        dates.append(startTime+dt.timedelta(seconds=n))

    driftscan={'date':dates,'power':trace,'label':'EMBRACE@Nancay driftscan of CygA at 1278.75MHz'}
    plot_satImpactParameter(tracks,driftscan=driftscan)
    return tracks

if __name__=="__main__":
    arguments=parseargs()
    if arguments['action']=='loadnpickle':
        loadnpickle(arguments)
    elif arguments['action']=='help':
        usage()
        
        


        
    
