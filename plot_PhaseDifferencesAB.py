'''
$Id: plot_PhaseDifferencesAB.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Fri 11 Oct 2013 11:04:39 CEST

plot phase differences during satellite tracking
all of B, and one of A

data: 
GPSBIIF-2--GPSBIIF-2_BeamA_csx_20131002-1545.fits
GPSBIIF-2--GPSBIIF-2_BeamB_csx_20131002-1545.fits

trying to find the tileset positions to use

normal:

12 13 14 15
 8  9 10 11
 4  5  6  7
 0  1  2  3

A1 looks like B2
A4 looks like B8

A5 looks like B10
A8 looks like B12?
A9 looks like B14

it looks like the distances are doubles?!!

'''
refA=0
refB=0

fudge=1

import matplotlib.pyplot as plt
from datefunctions import *


pngfile=cxA.Filename().replace('.fits','').replace('BeamA','')+'_ABphaseComparison.png'

if cxA.sx.phase_timeline==None:
    print "calculating timeline"
    cxA.plotTimeline(plot='phase')

if cxB.sx.phase_timeline==None:
    print "calculating timeline"
    cxB.plotTimeline(plot='phase')



# find start of tracking
trackstart_ntime=int(tot_seconds(cxA.ObsModeTimes()['trackstart'][0] - cxA.startTime()))+1

ntimeA_index=0
for i in cxA.sx.ntime_timeline:
     if i==trackstart_ntime:
         break
     ntimeA_index+=1
print "ntimeA_index=",ntimeA_index

ntimeB_index=0
for i in cxB.sx.ntime_timeline:
     if i==trackstart_ntime:
         break
     ntimeB_index+=1
print "ntimeB_index=",ntimeB_index


# plot all phases that should correspond, all on one plot
# i.e. A0-1/B0-1 etc
fig=plt.figure(figsize=(13.66,7.68))
ttl=str('Phase differences A - %3.1f x B' % fudge)
fig.canvas.set_window_title('plt: '+ttl) 
plt.suptitle(ttl)
for n in range(16):
    a=cxA.sx.adjustPhase(cxA.sx.phase_timeline[n])
    a_track=a[ntimeA_index:]
    b=cxB.sx.adjustPhase(cxB.sx.phase_timeline[n])
    b_track=b[ntimeB_index:]
    delta=a_track-fudge*b_track
    mean=delta.mean()
    while mean < -360:
        mean+=360
    while mean > 360:
        mean-=360
    plt.plot(delta,label=str('%i-%i: %8.2f' % (refA,n,mean)),
                 linestyle=cxB.metainfo['linestyles'][n],
                 color=cxB.metainfo['colours'][n])



plt.legend()
plt.savefig(pngfile,format='png',dpi=100)
plt.show()

"""
# plot comparison of all B phases to each A phase
for nA in range(16):
    a=cxA.sx.adjustPhase(cxA.sx.phase_timeline[nA])
    fig=plt.figure(figsize=(13.66,7.68))
    ttl=str('A: %i-%i' % (refA,nA))
    fig.canvas.set_window_title('plt: '+ttl) 
    plt.title(ttl)
    a_track=a[ntimeA_index:]
    for nB in range(16):
    
        b=cxB.sx.adjustPhase(cxB.sx.phase_timeline[nB])
        b_track=b[ntimeB_index:]
        plt.plot(a_track-2*b_track,label=str('B: %i-%i' % (refB,nB)),
                 linestyle=cxB.metainfo['linestyles'][nB],
                 color=cxB.metainfo['colours'][nB])

    plt.legend()


    plt.show()


"""
