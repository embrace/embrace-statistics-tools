"""
plot the subband number in a crosslets file
"""
sbnum=[]
for i in range (3,len(csx.metainfo['hdulist'])):
    nstr=csx.metainfo['hdulist'][i].header['SBNUM']
    if isinstance(nstr,str):
        n=eval(nstr)
    else:
        n=nstr
    if n==65535: 
        print "found n=65535"
        n=-1
    sbnum.append(n)
