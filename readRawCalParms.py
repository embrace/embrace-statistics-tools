#!/usr/bin/env python
"""
$Id: readRawCalParms.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Fri 21 Sep 2012 15:53:59 CEST

read data from LCU output of digital calibration parameters
normally the file is called outputCalibrationParameters_BeamA.dat

$modified: Mon 05 Aug 2013 17:00:51 CEST
  I was filtering zeros, but we need to keep them to create the
  calibration.txt file which will be loaded by BeamServer
  see on LCU: BeamServer-I?.conf

  output in a format compatible with calibration.txt
  i.e. 7 columns (not sure why)
  (see mail 2014-05-09)

$modified: Thu 10 Jul 2014 08:56:22 CEST
  output format should be binary, apparently.
  (conversation with Taff 2014-07-09)  

$modified: Wed 16 Jul 2014 20:48:09 CEST
  defining function for easier use in interactive mode

"""
import os,sys,struct
import numpy as np
# filename should be given as an argument
# unless we're running in interactive mode with execfile

def readRawCalParms(filename=None):
    if filename==None or not isinstance(filename,str):
        print "usage: readRawCalParms(<filename>)"
        return None
    if not os.path.exists(filename):
        print "file not found: ",filename
        return None


    fin=open(filename,'rb')
    # fout=open(filename+'.txt','w')

    counter=0
    datlist=[]
    packed_datlist=[]
    try:
        packed=fin.read(8)
        while packed != "":
            packed_datlist.append(packed)
            val=struct.unpack('<d',packed)[0]
            datlist.append(val)
            # print '%08i %7.3f' % (counter,val)
            # if not val==0.0:fout.write(str(val)+'\n')
            packed=fin.read(8)
            counter+=1

    finally:
        fin.close()
        # fout.close()

    return np.array(datlist)

def writeRawCalParms(dat,filename=None):
    if filename==None or not isinstance(filename,str):
        print "usage: writeRawCalParms(<dat array>,<filename>)"
        return None

    filecounter=0

    basename=os.path.basename(filename)
    index=0
    index_end=0

    while index<len(dat):
        oname=basename+"."+str("%04i" % filecounter)+".calibration.dat"
        ofile=open(oname,'w')

        counter=0
        while counter<2*512*24 and index<len(dat):
            ofile.write(packed_dat[index])
            index+=1
            counter+=1
        ofile.close()

        filecounter+=1

    return True

def writeTextCalParms(dat,filename=None):
    if filename==None or not isinstance(filename,str):
        print "usage: writeRawCalParms(<dat array>,<filename>)"
        return None

    while index<len(dat):
        counter=0
        oname=basename+"."+str("%04i" % filecounter)+".calibration.txt"
        ofile=open(oname,'w')
        ofile.write('512 x 24 x 2\n[')

        columncounter=0
        while counter<2*512*24 and index<len(dat):
            while columncounter<7 and counter<2*512*24 and index<len(dat):
                ofile.write('   '+str("%010.6f" % dat[index]))
                index+=1
                counter+=1
                columncounter+=1
            if columncounter==7: ofile.write('\n')
            columncounter=0

        ofile.write(' ]\n')
        ofile.close()
        filecounter+=1

    return True

def reformRawCalParms(parms):
    '''
    reform the cal parms array for plotting
    '''
    setsize=2*24*512 # 2-directions, 24-tilesets, 512 subbands
    parms_len=len(parms)
    if parms_len != setsize:
        print 'please give a single calibration set'
        print 'it should be an array of length: ',setsize
        return None

    parms_XY=parms.reshape(24*512,2)

    # take only the X direction. the Y is just a repeat
    parms_X=parms_XY[:,0]

    # reshape to get by tileset
    reformed_parms=parms_X.reshape(512,24)
    
    #reordered_parms=reformed_parms[:,0]
    #for n in range(1,96):
    #    reordered_parms=np.append(reordered_parms,reformed_parms[:,n])
        
    
    return reformed_parms #reordered_parms

def separateRawCalParms(parms):
    '''
    separate a large block of cal parms into individual sets
    '''
    setsize=2*24*512 # 2-directions, 24-tilesets, 512 subbands

    nparms=len(parms)
    nsets=nparms/setsize

    parms_by_set=parms.reshape(nsets,setsize)
    return parms_by_set
    


