#!/usr/bin/env python
"""
$Id: readRawStats.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Mon 13 Aug 2012 12:33:43 CEST

Read raw data output from the rspctl tool

see email from Dion: Thu, 2 Aug 2012 14:10:56
also, extracted to dion_diagnostic.sh

"""
import os,sys,struct

# filename should be given as an argument
# unless we're running in interactive mode with execfile


try:
    if isinstance(filename,str): print "trying to open file: ",filename
except:
    nargs=len(sys.argv)
    if nargs<2:
        print "usage: ",sys.argv[0]," <filename>"
        quit()

    filename=sys.argv[1]
    #print "filename given as argument: ",filename

if not os.path.exists(filename):
    print "file not found: ",filename
    quit()

fin=open(filename,'rb')

dat=[]
try:
    packed=fin.read(8)
    while packed != "":
        val=struct.unpack('<d',packed)[0]
        dat.append(val)
        packed=fin.read(8)

finally:
    fin.close()
    if len(dat)>0: print max(dat)



