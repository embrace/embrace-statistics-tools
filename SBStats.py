"""
$Id: SBStats.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Thu Mar 22 15:53:29 CET 2012

python class to read and plot EMBRACE Subband Statistics
this class is called from the generic class embraceStats
it contains methods specific to Subband Statistics stuff

$modified: Wed Jun 13 12:21:22 CEST 2012
  removed CalculateTimeline().  It is now done in class embraceStats

  method verify_n() calls RCU() for compatibility between BLStats

$modified: Fri 20 Jul 2012 17:06:14 CEST
  handle error for min vals when all values are zero

$modified: Tue 18 Jun 2013 14:39:35 CEST
  thinking about timeline for big files:  not yet implemented.
  clearcal() : clear calculated timeline
  CalculateTimeline() changed to Timeline()

$modified: Wed 02 Oct 2013 16:31:12 CEST
  calibrated() : for compatibility with embraceStats

$modified: Thu 14 Nov 2013 17:03:30 CET
  Data() plotData() etc : option to plot linear instead of dBm
"""

# import all the packages we will need
import os,sys
import datetime as dt
import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import axes3d
import pyfits as fits
import numpy as np
import re
import pickle
from tools_embraceStats import *

class SBStats:
    """
    $Id: SBStats.py
    $auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
    $created: Thu Mar 22 15:53:29 CET 2012

    python class to read and plot EMBRACE Subband Statistics
    this class is called from the generic class embraceStats
    it contains methods specific to Subband Statistics stuff

    """

    # constructor, must pass the metainfo dictionary at creation
    def __init__(self,metainfo):
        self.metainfo=metainfo

         # try to read a pickle file if it exists
        self.dynamicspecarray=unpickle(self.metainfo)

        # assign an empty list for the dynamic spec arrays of the RCUs
        # but only if the unpickling was unsuccessful
        if self.dynamicspecarray==None:
            self.dynamicspecarray=[]
            for i in range(self.metainfo['nRCU']):
                self.dynamicspecarray.append(None)

        self.assign_Freq()

        self.timeline=None

        return
        
    # destructor
    def __del__(self):
        return

    # check that the given RCU argument is okay
    def RCU(self,rcu):
        if rcu==None: rcu=0
        if rcu>=self.metainfo['nRCU'] or rcu < 0:
            print "that RCU is out of range"
            return None
        return rcu

    # an alias for the same thing.
    def verify_n(self,n):
        return self.RCU(n)

    # define frequency axis for subband statistics
    # n is a dummy argument to be compatible with the method of the same name in BLStats
    def Freq(self,n=0):
        return self.freq

    # assign the frequency axis
    def assign_Freq(self):
        freq=[]

        # this keyword is usually in the file, but we don't use it if the standard WCS keywords are found
        if 'BANDWIDT' in self.metainfo['hdulist'][0].header.keys():
            bwidth=float(self.metainfo['hdulist'][0].header['BANDWIDT'])
        else:
            bwidth=self.metainfo['RFbandwidth']

        # get centre frequency from the primary header
        # this is really SKYFREQ (ie. local rest frame frequency)
        # PR uses FREQOL as the keyword, 
        # but in fact the local oscillator is set at -SKYFREQ+BASEBAND+LO1
        #   where: BASEBAND = 150MHz (ie. band from 100MHz to 200MHz)
        #          LO1 = 2850MHz
        # for example: for SKYFREQ=1220MHz, LO2 is set to 1780MHz. (LO1 is fixed at 2850MHz)
        # CHANGE: Fri Oct  8 18:07:51 CEST 2010
        # PR has replaced FREQOL with CRVAL1 etc following the WCS convention
        # I still want this to work with old files for now...
        if 'CUNIT1' in self.metainfo['hdulist'][0].header:
            print "getting frequency info from WCS FITS header keywords"
            cenfreq=float(self.metainfo['hdulist'][0].header['CRVAL1'])            
            chanspace=float(self.metainfo['hdulist'][0].header['CDELT1'])
            if self.metainfo['hdulist'][0].header['CUNIT1']=="Hz":
                cenfreq=cenfreq/1000000. # frequency in MHz
                chanspace=chanspace/1000000.
            # subtract one from CRPIX1 because the FITS header numbers from 1, but python numbers from 0
            cenpix=self.metainfo['hdulist'][0].header['CRPIX1']-1
            # lower bandwidth from start to cenpix
            lbwidth=cenpix*chanspace
            
        elif 'FREQOL' in self.metainfo['hdulist'][0].header: # old files use FREQOL keyword
            print "getting frequency info from FREQOL keyword"
            cenfreq=float(self.metainfo['hdulist'][0].header['FREQOL'])/1000000 # in MHz
            chanspace=bwidth/float(self.metainfo['nchans']-1) # should be 100MHz bandwidth
            cenpix=255
            lbwidth=bwidth/2
        elif 'FREQ' in self.metainfo['hdulist'][0].header: # middle files use FREQ keyword
            print "getting frequency info from FREQ keyword"
            cenfreq=float(self.metainfo['hdulist'][0].header['FREQ'])/1000000.0 # in MHz
            chanspace=bwidth/float(self.metainfo['nchans']-1) # should be 100MHz bandwidth
            cenpix=255
            lbwidth=bwidth/2
        else:
            print "frequency info not in file, assuming centre frequency 0MHz"
            cenfreq=0.0
            chanspace=bwidth/self.metainfo['nchans']
            cenpix=255
            lbwidth=bwidth/2

        for i in range(0,self.metainfo['nchans']):
            freq.append(i*self.metainfo['chanwidth']+self.metainfo['RFcentre']-lbwidth)

        self.freq=freq                
        return True

    # make an array of spectra for an RCU
    # ntime: data starts at index 1, because 0 is the primary header
    # direction is a dummy argument to be compatible with the method of the same name in BLStats
    def Data(self,ntime,n=0,direction='x',linear=False):
        rcu=self.RCU(n)
        if rcu==None: return None
        p=self.metainfo['hdulist'][ntime].data.field(rcu)
        if linear: return 10**(p/10.0)
        return p

    # make the middle part of a title for a plot
    def mkPlotTitle(self,n=None,direction=None):
        if isinstance(n,int):
            ttl=': RCU '+str("%2d" % n)
            filename='_RCU'+str("%02d" % n)
        else:
            ttl=': all RCUs'
            filename='_tilesets'
        return ttl,filename

    # plot all RCUs for a given time
    # rcu=None means all rcu's... it's logical
    def plotData(self,ntime,n=None,direction=None,linear=False):
        if n==None:
            startRCU=0
            endRCU=self.metainfo['nRCU']
        else:
            startRCU=n
            endRCU=n+1

        for rcu in range(startRCU,endRCU):
            ret=plt.plot(self.Freq(rcu),self.Data(ntime,rcu,linear=linear),
                         label='RCU '+str(rcu),
                         linestyle=self.metainfo['linestyles'][rcu],
                         color=self.metainfo['colours'][rcu])
        plt.legend()
        return ret

    # return a dynamic spec array from an RCU.
    # calculate if necessary, otherwise take it from memory
    def DynamicSpec(self,n=0,direction='x'):
        rcu=self.RCU(n)
        if rcu==None: return None
        if self.dynamicspecarray[rcu]==None:
            self.dynamicspecarray[rcu]=self.mkDynamicSpecArray(rcu)            

        return self.dynamicspecarray[rcu]

    # make an array for a dynamic spectrum of one RCU
    def mkDynamicSpecArray(self,n,direction='x'):
        rcu=self.RCU(n)
        if rcu==None: return None
        print "assigning data array for rcu ",rcu," ...",
        sys.stdout.flush()

        X = np.empty( (self.metainfo['ndata'],self.metainfo['nchans']) )
        for i in range(self.metainfo['ndata']):
            ntime_index=i+self.metainfo['firstHDU']
            X[i]=self.Data(ntime_index,rcu)

        print " done"
        return X

    # alias to calculate the timeline, the work is done in tools_embraceStats
    # n=RCU, direction is a dummy for compatibility calling from embraceStats
    def Timeline(self,freq0,bwidth=0.0,nchans=None,n=None,direction='x',linear=False):
        return CalculateTimeline(self,freq0,bwidth,nchans,n,direction,linear)

    # plot the timelines for all RCUs
    # onoff keyword is for compatibility with Beamlet Stats plot
    def plotTimeline(self,freq0,bwidth,nchans=None,onoff=False,linear=False):

        nstyles=len(self.metainfo['linestyles'])
        minvals=[]
        maxvals=[]

        for rcu in range(0,self.metainfo['nRCU']):
            power=self.Timeline(freq0,bwidth,nchans,n=rcu,linear=linear)
            # for min power, ignore zeroes, but if there are only zeros, return zero
            try: minvals.append(min(power[power>0.0]))
            except: minvals.append(0.0)
            maxvals.append(max(power))
            style_index=rcu
            while style_index >= nstyles:
                style_index-=nstyles

            ret=plt.plot(power,
                         label='RCU '+str(" %2d" % rcu),
                         linestyle=self.metainfo['linestyles'][style_index],
                         color=self.metainfo['colours'][style_index])
        return min(minvals),max(maxvals)

    # clear calculated results
    def clearcal(self):        
        self.timeline=None
        return None

    # return calibration status (for compatibility with embraceStats)
    def calibrated(self):
        return True
