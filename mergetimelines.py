#!/usr/bin/python
'''
$Id: mergetimelines.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Fri 28 Jun 2013 07:57:53 CEST

merge Timelines from a split big file

$modified: Fri 26 Jul 2013 16:27:33 CEST
  reworked to allow for multiple digital beams

'''

import sys,os,pickle
from embraceStats import embraceStats as estats
import glob

if len(sys.argv) < 2:
    print "usage: "+sys.argv[0]+" <filename>"
    quit()

def splitfilename(filename,n):
    return filename.replace('.fits','')+str('.%04i.fits' % n)


def mergetimelines(filename):
    basename=os.path.basename(filename)
    globname=basename.replace('.fits','')+'*fits'
    files=glob.glob(globname)
    nfiles=len(files)
    if nfiles==0:
        print "no files found for basename: ",basename
        return None

    bx=estats()
    ndigbeams=None
    for n in xrange(nfiles):
        fname=splitfilename(basename,n)
        if not os.path.exists(fname):
            print "file not found: ",fname
            break

        print fname
        bx.read(fname)

        # check if this is the first time through the loop
        if ndigbeams==None:
            ndigbeams=bx.nDigitalBeams()
            timeline=[]
            for n in range(ndigbeams):
                timeline.append({})
                timeline[n]['X']=[]
                timeline[n]['Y']=[]


        for direction in ['X','Y']:
            for n in range(ndigbeams):
                p=bx.Timeline(direction=direction,n=n)
                timeline[n][direction].extend(p)
        bx.close()

    picklename=basename.replace('.fits','')+'.timeline.pickle'
    opickle=open(picklename,'w')
    pickle.dump(timeline,opickle)
    opickle.close()

    return

if __name__=="__main__":
    if len(sys.argv) < 2:
        print "usage: "+sys.argv[0]+" <filename>"
    else:
        mergetimelines(sys.argv[1])

