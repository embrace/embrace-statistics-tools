"""
polynomial fit to CasA tracking data from sep/oct 2011
"""
import numpy as np

if not isinstance(cas1,estats):
    cas1=estats('CasA_bsx_20110930-2337.fits')
    cas2=estats('CasA_bsx_20111001-2333.fits')
    cas3=estats('CasA_bsx_20111002-2330.fits')


"""
# On/Off
day1=cas1.Timeline(1175.6,6,direction='X')-cas1.Timeline(1175.6,6,direction='Y')
day2=cas2.Timeline(1175.6,6,direction='X')-cas2.Timeline(1175.6,6,direction='Y')
day3=cas3.Timeline(1175.6,6,direction='X')-cas3.Timeline(1175.6,6,direction='Y')
ylabel="On-Off / dB"
axes=[0,len(day1),-1.5,1.0]
"""

# On only
day1=cas1.Timeline(1175.6,6,direction='X')
day2=cas2.Timeline(1175.6,6,direction='X')
day3=cas3.Timeline(1175.6,6,direction='X')
ylabel="uncalibrated power / dB"
axes=[0,len(day1),83,85]

avg=(day1[0:7195]+day2+day3[0:7195])/3
wiggle=avg[2396:4799]
x=range(0,len(wiggle))
ans=np.polynomial.polynomial.Polynomial.fit(x,wiggle,12)


xyfit=ans.linspace(n=500)
ttl="CasA tracking and drift scans, 3 subsequent days"
filename="CasA-3days.png"
fig=plt.figure(figsize=(12.80,9.60))
fig.canvas.set_window_title(ttl) 
plt.title(ttl)

plt.plot(xyfit[0]+2396,xyfit[1],label="polynomial fit")
#plt.plot(wiggle)
plt.plot(day1,label="30 Sep 2011")
plt.plot(day2,label="1 Oct 2011")
plt.plot(day3,label="2 Oct 2011")
plt.xlabel("time / seconds")
plt.ylabel(ylabel)
plt.axis(axes)
plt.legend()
ret=plt.savefig(filename,format='png',dpi=100)
plt.show()


filename="CasA-3days_fitted.png"
day1adjust=day1[0:7195]
day2adjust=day2
day3adjust=day3[0:7195]
#xyfit=ans.linspace(n=2396+4799,domain=[-2396,4799])
xyfit=ans.linspace(n=2396+4799)

# find maximum of first drift
maxvals=[
    max(day1[0:2396]),
    max(day2[0:2396]),
    max(day3[0:2396])]
drift1avg=(maxvals[0]+maxvals[1]+maxvals[2])/3



for i in range(0,2396):
    day1adjust[i]=day1adjust[i]-drift1avg
    day2adjust[i]=day2adjust[i]-drift1avg
    day3adjust[i]=day3adjust[i]-drift1avg


for i in range(2396,4799):
    day1adjust[i]=day1adjust[i]-wiggle[i-2396]
    day2adjust[i]=day2adjust[i]-wiggle[i-2396]
    day3adjust[i]=day3adjust[i]-wiggle[i-2396]

# find maximum of second drift
maxvals=[
    max(day1[4799:7195]),
    max(day2[4799:7195]),
    max(day3[4799:7195])]
drift2avg=(maxvals[0]+maxvals[1]+maxvals[2])/3

for i in range(4799,7195):
    day1adjust[i]=day1adjust[i]-drift2avg
    day2adjust[i]=day2adjust[i]-drift2avg
    day3adjust[i]=day3adjust[i]-drift2avg


# raise everything to have a zero level
for i in range(0,7195):
    day1adjust[i]=day1adjust[i]+1.2
    day2adjust[i]=day2adjust[i]+1.2
    day3adjust[i]=day3adjust[i]+1.2


ttl="CasA tracking and drift scans, 3 subsequent days"
fig=plt.figure(figsize=(12.80,9.60))
fig.canvas.set_window_title(ttl) 
plt.title(ttl)

#plt.plot(xyfit[0],xyfit[1],label="polynomial fit")
#plt.plot(wiggle)
plt.plot(day1adjust,label="30 Sep 2011")
plt.plot(day2adjust,label="1 Oct 2011")
plt.plot(day3adjust,label="2 Oct 2011")
plt.xlabel("time / seconds")
plt.ylabel(ylabel)
axes=[0,len(day1),0,1.5]
plt.axis(axes)
plt.legend()
ret=plt.savefig(filename,format='png',dpi=100)
plt.show()

