"""
plot the corners of EMBRACE
from the measurements by Laurence Alsac
see document 2012-06-22
"""
import math

pngfile='embrace-rotation.png'

we_x=[-298.737,-310.706]
we_y=[-60.173,-59.396]
tantheta=(we_y[1]-we_y[0])/(we_x[1]-we_x[0])
theta=math.degrees(math.atan(tantheta))
rotation_correction=135+math.degrees(math.atan(tantheta))

sn_x=[-304.339,-305.107]
sn_y=[-53.81,-65.775]

border_x=[sn_x[0],we_x[0],sn_x[1],we_x[1],sn_x[0]]
border_y=[sn_y[0],we_y[0],sn_y[1],we_y[1],sn_y[0]]

fig=plt.figure(figsize=(12.80,9.60))
fig.canvas.set_window_title('plt: EMBRACE rotation') 
plt.title('EMBRACE rotation',fontsize=14)

plt.axes().set_aspect('equal')
plt.plot(we_x,we_y)
plt.plot(sn_x,sn_y)
plt.plot(border_x,border_y)
plt.savefig(pngfile,format='png')
plt.show()
