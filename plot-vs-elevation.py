"""
plot versus sky angle
"""
import sys
try:
    if not isinstance(sunb,estats):
        print "please assign sunb"
        sys.exit()
except:
    print "please assign sunb"
    sys.exit()

import ephem as eph
from ObsFunctions import ObsFunctions
obsfunc=ObsFunctions()
obsfunc.sateph=eph.Sun(obsfunc.embraceNancay)

firstHDU=sunb.FirstHDU()
maxhdu=len(sunb.metainfo['hdulist'])



altlist=[]
azlist=[]
t=[]
for i in range(0,sunb.nData()):
    obsfunc.embraceNancay.date=sunb.Timestamp(i)
    obsfunc.sateph.compute(obsfunc.embraceNancay)
    az=obsfunc.rad2deg*float(obsfunc.sateph.az)
    alt=obsfunc.rad2deg*float(obsfunc.sateph.alt)

    t.append(sunb.Timestamp(i))
    azlist.append(az)
    altlist.append(alt)

# from the SciPy cookbook...

from pylab import *

gaussian = lambda x: 3*exp(-(30-x)**2/20.)

data = gaussian(arange(100))



X = arange(data.size)
x = sum(X*data)/sum(data)
width = sqrt(abs(sum((X-x)**2*data)/sum(data)))

max = data.max()

fit = lambda t : max*exp(-(t-x)**2/(2*width**2))

plot(fit(X))

show()
