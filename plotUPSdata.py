"""
$Id: plotUPSdata.py
$auth: Steve Torchinsky <steve.torchinsky@obspm.fr>
$created: Thu 28 Jul 2016 16:49:11 CEST
$license: GPLv3 or later, see https://www.gnu.org/licenses/gpl-3.0.txt

          This is free software: you are free to change and
          redistribute it.  There is NO WARRANTY, to the extent
          permitted by law.

read data from the big UPS and plot
it's in CSV format
http://realtime.obs-nancay.fr/cacti2/graph.php?local_graph_id=62&rra_id=all
"""
import os,sys
from datefunctions import *


def plotUPScsv(t,p):
    ttl='EMBRACE Container UPS Load'
    fig=plt.figure(figsize=(12.80,9.60))
    fig.canvas.set_window_title('plt: '+ttl)
    plt.xlabel('start date: '+t[0].strftime('%Y-%m-%d %H:%M:%S'))
    plt.ylabel('% capacity')
    plt.plot(t,p)
    plt.title(ttl)

    datefmt='%Y%d%mT%H%M%S'
    outname='EMBRACE_UPS_Load_'+t[0].strftime(datefmt)+'-'+t[-1].strftime(datefmt)+'.png'
    plt.savefig(outname,format='png',dpi=100,bbox_inches='tight')
    plt.show()
    return

def readUPScsv(filename):
    if not os.path.exists(filename):
        print 'file not found: ',filename
        return None

    h=open(filename,'r')
    contents=h.read()
    h.close()

    lines=contents.split('\n')
    pairs=[]
    for val in lines[10:-1]:
        pairs.append(eval(val))

    t=[]
    p=[]
    for val in pairs:
        t.append(str2dt(val[0]))
        if val[1]=="NaN":
            p.append(0.0)
        else:
            p.append(eval(val[1]))

    plotUPScsv(t,p)
    return t,p
