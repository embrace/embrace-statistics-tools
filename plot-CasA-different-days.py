"""
plot comparison of CasA tracking

$Id: plot-CasA-different-days.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>

$modified: Mon May 14 13:20:20 CEST 2012
  added run from 2012-05-11

$modified: Fri Jun 29 10:59:01 CEST 2012
  changed name of script from plot-CasA-5days.py

  load data only if not already loaded

$modified: Wed Jul  4 10:16:10 CEST 2012
  CasA pure tracking from 2012-07-04, no drift scans

$modified: Mon Jul 16 15:37:32 CEST 2012
  CasA pure tracking from 2012-07-12, but that was at 1420MHz
  commented out

$modified: Thu 19 Jul 2012 10:14:28 CEST
  behind CasA pure tracking.  NO!  it was the wrong frequency
  I introduced a bug in ObsFunctions when I added NAVSTAR63

$modified: Fri 20 Jul 2012 10:03:00 CEST
  behind CasA pure tracking from 2012-07-20

"""

import os
import numpy as np

# load files, if necessary
try:
    print cas['2012-06-29'].Filename(),"... already loaded? "

except:
    datadir='/scu/data/obs'
    cas={}
    cas['2011-11-16']=estats(datadir+'/CasA_bsx_20111116-1933.fits')
    cas['2011-11-17']=estats(datadir+'/CasA_bsx_20111117-1929.fits')
    cas['2011-12-07']=estats(datadir+'/CasA_bsx_20111207-1710.fits')
    cas['2011-12-08']=estats(datadir+'/CasA_bsx_20111208-1706.fits')
    cas['2012-04-27']=estats(datadir+'/CasA_BeamA_bsx_20120427-0752.fits')
    cas['2012-05-11']=estats(datadir+'/CasA_BeamA_bsx_20120511-0657.fits')
    cas['2012-06-27']=estats(datadir+'/CasA_BeamA_bsx_20120627-0352.fits')
    cas['2012-06-29']=estats(datadir+'/CasA_BeamA_bsx_20120629-0344.fits')
    cas['2012-07-04 no drift scans']=estats(datadir+'/CasA_BeamA_bsx_20120704-0324.fits')
    cas['2012-07-04 anti CasA']=estats(datadir+'/antiCasA_BeamA_bsx_20120704-1522.fits')
    cas['2012-07-05 anti CasA']=estats(datadir+'/antiCasA_BeamA_bsx_20120705-1519.fits')
    cas['2012-07-06 anti CasA']=estats(datadir+'/antiCasA_BeamA_bsx_20120706-1515.fits')
#    cas['2012-07-12 1420MHz']=estats(datadir+'/CasA_BeamA_bsx_20120712-0253.fits')
    cas['2012-07-13 quarter CasA']=estats(datadir+'/quarterCasA_BeamA_bsx_20120713-2046.fits')
    cas['2012-07-20 behind CasA']=estats(datadir+'/behindCasA_BeamA_bsx_20120720-0521.fits')
days=cas.keys()
days.sort()
pngfile='CasA-'+str("%02d" % len(days))+'days.png'
pngoffsrc='CasA-offsource-'+str("%02d" % len(days))+'days.png'

# assign bandwidth, if necessary
try:
    if isinstance(bwidth,int) or isinstance(bwidth,float):
        print "using bandwidth: ",bwidth
        
except:
    bwidth=55*100.0/512


# assign the timelines in linear
timeline={}
offsrc={}
maxvals=[]
minvals=[]
ndata=0
for day in days:
    tmp=cas[day].Timeline(1175.6,bwidth)
    for i in range(cas[day].nData()):tmp[i]=10**(tmp[i]/10.0)
    timeline[day]=tmp
    tmp=cas[day].Timeline(1175.6,bwidth,direction='Y')
    for i in range(cas[day].nData()):tmp[i]=10**(tmp[i]/10.0)
    offsrc[day]=tmp
    maxvals.append(max(timeline[day]))
    minvals.append(min(timeline[day]))
    if ndata < cas[day].nData():ndata=cas[day].nData()


# min/max for plotting
maxval=max(maxvals)
minval=min(minvals)
try:
    if isinstance(pmin,int) or isinstance(pmin,float):
        print "min power: ",minval,' but using: ',pmin
        
except:
    pmin=minval

try:
    if isinstance(pmax,int) or isinstance(pmax,float):
        print "max power: ",maxval,' but using: ',pmax
except:
    pmax=maxval

fig=cas['2012-06-29'].openPlot('Comparison of CasA tracking on different days',
                        'bogusfilename',
                        subttl='track, drift, track, drift : 1175.6MHz +/- '+str("%5.2f" % (bwidth/2.0))+'MHz')


plt.axis([0,ndata,pmin,pmax])
plt.xlabel('time / seconds ')
plt.ylabel('uncalibrated power / mW')

style_index=0
linestyles=cas['2012-06-29'].metainfo['linestyles']
colours=cas['2012-06-29'].metainfo['colours']
for day in days:
    plt.plot(timeline[day],label=day,
             linestyle=linestyles[style_index],
             color=colours[style_index])
    style_index+=1


cas['2012-06-29'].pltObsModeTimes([pmin,pmax])
plt.legend(prop={'size':10},loc='upper right',bbox_to_anchor=(0, 0, 1.1, 1.1))
plt.savefig(pngfile,format='png')
plt.show()

### and do the off source tracking
fig=cas['2012-06-29'].openPlot('Comparison of CasA/off-source tracking on different days',
                        'bogusfilename',
                        subttl='track, drift, track, drift : 1175.6MHz +/- '+str("%5.2f" % (bwidth/2.0))+'MHz')


plt.axis([0,ndata,pmin,pmax])
plt.xlabel('time / seconds ')
plt.ylabel('uncalibrated power / mW')

style_index=0
linestyles=cas['2012-06-29'].metainfo['linestyles']
colours=cas['2012-06-29'].metainfo['colours']
for day in days:
    plt.plot(offsrc[day],label=day,
             linestyle=linestyles[style_index],
             color=colours[style_index])
    style_index+=1


cas['2012-06-29'].pltObsModeTimes([pmin,pmax])
plt.legend(prop={'size':10},loc='upper right',bbox_to_anchor=(0, 0, 1.1, 1.1))
plt.savefig(pngoffsrc,format='png')
plt.show()
