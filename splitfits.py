#!/usr/bin/python
'''
$Id: splitfits.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Thu 27 Jun 2013 16:01:34 CEST

yet another attempt to get around the problem
split big fits file

$modified: Thu 25 Jul 2013 16:33:07 CEST
  using fats.py to find headers and spl


'''
from fats import *

batchsize=10000

import gc,os,sys


def splitfilename(filename,n):
    basename=os.path.basename(filename)
    return basename.replace('.fits','')+str('.%04i.fits' % n)

def splitfile(filename):
    if not os.path.exists(filename):
        print "file not found: ",filename
        return None

    headerpos,firstHDU=getheaderlist(filename)
    filesize=os.path.getsize(filename)
    print "filesize=",filesize

    nHDU=len(headerpos)
    nbatches=nHDU/batchsize + 1
    if nbatches==1:
        print 'no need to split. ',nHDU,' < ',batchsize
        return None


    ifile=None
    hdu=firstHDU
    for batch in xrange(nbatches):
        fname=splitfilename(filename,batch)

        if os.path.exists(fname):
            print "split already done: ",fname
            n=0
            hdu+=batchsize
            continue

        print "filling split file: ",fname
        ifile=open(filename,'r')
        ofile=open(fname,'w')

        # copy primary header, and headers up to first beam data hdu
        byte=0
        while byte < headerpos[firstHDU]:
            ofile.write(ifile.read(1))
            byte+=1


        # copy a batch of HDUs
        n=0
        byte=headerpos[hdu]
        ifile.seek(byte) # go to the position

        # find the location of the next header so we can count headers
        if hdu+1>nHDU:
            nexthdu_pos=filesize
        else:
            nexthdu_pos=headerpos[hdu+1]-1
        print "nexthdu_pos=",nexthdu_pos
        print "hdu=",hdu," n=",n," byte=",byte," ...",

        while n<batchsize and hdu<nHDU and byte<=filesize:
            ofile.write(ifile.read(1))

            byte+=1
            if byte>=nexthdu_pos:
                hdu+=1
                n+=1
                if hdu+1>=nHDU:
                    nexthdu_pos=filesize
                else:
                    nexthdu_pos=headerpos[hdu+1]
                print "complete.\nhdu=",hdu," n=",n," byte=",byte," ...",

        print " all done."
        ofile.close()

    if ifile!=None: ifile.close()
    return

    





    
if __name__=="__main__":
    if len(sys.argv) < 2:
        print "usage: "+sys.argv[0]+" <filename>"
        quit()

    filename=sys.argv[1]
    splitfile(filename)

