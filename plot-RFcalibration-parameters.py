"""
$Id: plot-RFcalibration-parameters.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Tue 29 Jan 2013 10:25:50 CET
 
read the RF calibration parameters from file and plot them

$modified: Wed 30 Jan 2013 11:10:53 CET
  cleaning up, making functions etc

"""
import os,sys
import math
import numpy as np

def readRFcalparms(filename):
    if not os.path.exists(filename):
      print "file not found: "+filename
      return None

    ifile=open(filename,'r')
    allparms=[]
    for line in ifile:
        RFcalparms={}
        datstr=line.rsplit(' ')
        RFcalparms['freq']=eval(datstr[0])
        RFcalparms['az']=eval(datstr[1])
        RFcalparms['el']=eval(datstr[2])
        datlist=eval('['+line.split('[')[1])

        parms=np.empty(len(datlist))
        i=0
        for val in datlist:
            parms[i]=val
            i+=1
        parms=parms.reshape(16,4)
        RFcalparms['data']=parms
        allparms.append(RFcalparms)
        del RFcalparms

    ifile.close()
    return allparms

# reduce a sequence to unique values
## using function f3 from: http://www.peterbe.com/plog/uniqifiers-benchmark
def uniqify(seq):
   # Not order preserving
   keys = {}
   for e in seq:
       keys[e] = 1
   return keys.keys()


# get cal parms for a given tileset, tile combo, etc
def getRFcalparms(RFcalparms,beam='A',freq=1176450000.0,rcu=0,tile=1):
    freqstr=str("%.2fMHz" % (freq/1000000.0))
    print "Beam"+beam+', '+freqstr+', Tileset '+str(rcu)+', Tile0-Tile'+str(tile)
    val=[]
    az=[]
    el=[]
    for parm in RFcalparms:
        if parm['freq']==freq:
            val.append(parm['data'][rcu][tile])
            az.append(math.degrees(parm['az']))
            el.append(math.degrees(parm['el']))
    print val,'\n'
    return val,az,el

# plot function
def pltRFcalparms(RFcalparms,beam='A',freq=1176450000.0,rcu=0,tile=1):
    freqstr=str("%.2fMHz" % (freq/1000000.0))
    ttl='RF Calibration Parameters for Beam-'+beam+', '+freqstr
    pngfile='RFcalibration-parameters_Beam'+beam+'_'+freqstr
    
          
    fig=plt.figure(figsize=(12.80,9.60))
    fig.canvas.set_window_title('plt: '+ttl) 
    plt.suptitle(ttl,fontsize=16)
    plt.ylabel('selected phase parameter')
    plt.xlabel('elevation angle / degrees')

    if isinstance(rcu,list):
        pngfile+='.png'
        for n in rcu:
            subttl='Tileset '+str(n)+' : Tile0-Tile'+str(tile)
            val,az,el=getRFcalparms(RFcalparms,beam,freq,n,tile)
            plt.plot(el,val,'o',label=subttl)
    else:
        pngfile+='_RCU'+str(rcu)+'.png'
        subttl='Tileset '+str(rcu)+' : Tile0-Tile'+str(tile)
        val,az,el=getRFcalparms(RFcalparms,beam,freq,rcu,tile)
        plt.title(subttl)
        plt.plot(el,val,'o')


    plt.axis([min(el),max(el),0,25])
    if isinstance(rcu,list):plt.legend()
    plt.savefig(pngfile,format='png',dpi=100)
    plt.show()
    return


# go to work
beams=['A','B']
calparmsFile={}
parms={}
freqs={}
for beam in beams:
  calparmsFile[beam]='RFcalibrationParameters_Beam-'+beam+'.txt'
  parms[beam]=readRFcalparms(calparmsFile[beam])

  # which frequencies
  freqslist=[]
  for parm in parms[beam]:
      freqslist.append(parm['freq'])
  freqs[beam]=uniqify(freqslist)


  for freq in freqs[beam]:          

      for rcu in range(16):
      #for rcu in [0]:
          for tile in range(1,4):
          #for tile in [1]:
              val=getRFcalparms(parms[beam],beam,freq,rcu,tile)
