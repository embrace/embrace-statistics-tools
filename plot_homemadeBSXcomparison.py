"""
$Id: plot_homemadeBSXcomparison.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed 20 Mar 2013 20:43:35 CET

plot homemade beamelet timeline vs bsx timeline

$modified: Tue 27 Aug 2013 10:31:00 CEST
  making a function call, rather than using simply execfile

  renamed from plot-homemadeBSXcomparison.py to plot_homemadeBSXcomparison.py
  this allows python to import.  python complains about minus-sign in the filename

$modified: Wed 02 Oct 2013 19:30:02 CEST
  calibated -> calibrated

"""
import matplotlib.pyplot as plt

def homemadeBSXcomparison(bx,cx,offset=6.36,pmin=80,pmax=105):
    """ 
    plot homemade beamelet timeline vs bsx timeline
    args:  bx,cx
           (beamlet statistics object, crosslet statistics object)
    optional args: offset (in dB) default=6.36
                   pmin
                   pmax
    """

    offset_label=' offset by '+str('%.2f' % offset)+' dB'
    axes=[0,cx.nData(),pmin,pmax]

    ttl,filename=cx.mkPlotTitle()
    filename+='_homemadeBSX-calibratedSB'+str(cx.sx.calibratedSBnum)+'-VS-bsx.png'
    

    fig=cx.openPlot(ttl,filename,subttl='homemade beamlets vs. system bsx')
    ax1 = fig.add_subplot(111)
    p,t=cx.Timeline(plot='beamlet',ignore=True)
    plt.plot(t,p-offset,label='homemade beamlets'+offset_label)
    plt.plot(bx.Timeline(),label='bsx beamlets')
    cx.pltObsModeTimes([pmin,pmax])
    ax1.axis(axes)
    plt.legend()
    plt.xlabel('time / seconds since '+str(cx.startTime()))
    plt.ylabel('power / dBm')
    plt.savefig(filename,format='png')
    plt.show()

    return
