#!/usr/bin/env python
"""
$Id: eclipse20150320.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Mon 23 Mar 2015 10:27:26 CET
$license: GPLv3 or later, see https://www.gnu.org/licenses/gpl-3.0.txt

plot the eclipse from 2015-03-20

$modified: Tue 29 Sep 2015 14:15:54 CEST
  output in ps instead of png

"""
import gc
gc.enable()
from embraceStats import embraceStats as estats
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as pltdate

# data
# read data, two days before, the day, and two days after
try:
    if len(timeline['A'])==5:
        print 'data has been loaded, apparently.'
except:
    timeline={}
    day={}
    t={}
    nData_list={}
    datafiles={}
    ndata={}
    freq={}
    eclipse_day=2
    for beam in ['A','B']:
        datafiles[beam]=[]
        nData_list[beam]=[]
        datafiles[beam].append('CalFromFile--Sun_Beam'+beam+'_bsx_20150318-0701.fits')
        datafiles[beam].append('CalFromFile--Sun_Beam'+beam+'_bsx_20150319-0659.fits')
        datafiles[beam].append('CalFromFile--Sun_Beam'+beam+'_bsx_20150320-0657.fits')
        datafiles[beam].append('CalFromFile--Sun_Beam'+beam+'_bsx_20150321-0655.fits')
        datafiles[beam].append('CalFromFile--Sun_Beam'+beam+'_bsx_20150322-0653.fits')
        
        timeline[beam]=[]
        day[beam]=[]
        day_index=0
        for F in datafiles[beam]:
            bx=estats(F)
            nData_list[beam].append(bx.nData())
            p=bx.Timeline()
            P=10*np.exp(p/10)
            timeline[beam].append(P)
            day[beam].append(bx.startTime().strftime('%Y-%m-%d'))

            # make the time axis
            if day_index==eclipse_day:
                freq[beam]=bx.RFcentre()
                t[beam]=[]
                for n in range(bx.nData()):
                    t[beam].append(bx.Timestamp(n))
            day_index+=1
            bx.close()
            del bx
            gc.collect()
        ndata[beam]=min(nData_list)
    

# check for defaults
try:
    print 'A: pmin=',pmin['A']
except:
    pmin={}
    pmax={}
    relpmin={}
    relpmax={}
    pmin['A']=50000.
    pmax['A']=400000.
    relpmin['A']=0.6
    relpmax['A']=1.1
    pmin['B']=500000.
    pmax['B']=3000000.
    relpmin['B']=0.6
    relpmax['B']=1.1
    tmin=None
    tmax=None
    

try:
    print markerStart
except:
    markerStart=str2dt('2015-03-20 08:14')

try:
    print markerEnd
except:
    markerEnd=str2dt('2015-03-20 10:43')
    
def initplot(tmin=None,tmax=None,pmin=None,pmax=None,title=None,subtitle=None,epsfile=None):
    if title==None:title='EMBRACE@Nancay: Solar Eclipse of 20 March 2015'
    if epsfile==None:epsfile='SolarEclipse_20150320.eps'
    fig=plt.figure(figsize=(12.80,9.60))
    fig.canvas.set_window_title('plt: '+title) 
    ax = fig.add_subplot(111)

    # plot
    if tmin==None:tmin=str2dt('2015-03-20 06:00')
    if tmax==None:tmax=tmin+dt.timedelta(hours=5)
    if pmin==None:pmin=0
    if pmax==None:pmax=1e9
    
    axes=[tmin,tmax,pmin,pmax]
    ax.axis(axes)

    plt.xlabel('time')
    plt.ylabel('Power')
    plt.suptitle(title,fontsize=16)
    if subtitle!=None:plt.title(subtitle,fontsize=14)
    txt_x=0.88
    txt_y=0.38
    
    #plt.gca().xaxis.set_major_formatter(pltdate.DateFormatter('%Y-%m-%d %H:%M:%S'))
    #plt.gca().xaxis.set_major_locator(pltdate.HourLocator())
    return

def plotlinemarker(date=None,ymin=None,ymax=None,label=None):
    if date==None:return
    if ymin==None:return
    if ymax==None:return

    plt.plot([date,date],[ymin,ymax],color='red')
    text_x=date+dt.timedelta(minutes=2)
    text_y=ymax-0.6*(ymax-ymin)
    text_size=10
    if label==None:label=date.strftime('%Y-%m-%d %H:%M:%S')
    plt.text(text_x,text_y,label,fontsize=text_size,rotation=90,va='top')

    return
    
Prel={}
for beam in ['A','B']:
    if tmin==None:tmin=t[beam][0]
    if tmax==None:tmax=t[beam][-1]

    # find the minimum nData
    ndata=min(nData_list[beam])



    # first plot: all five timelines
    title=str('EMBRACE@Nancay: Eclipse of 20 March 2015 at %.1fMHz' % freq[beam])
    subtitle='comparison to days before and after'
    epsfile=str('EMBRACE-Nancay_eclipse20150320_5days_%.1f.eps' % freq[beam])

    initplot(tmin,tmax,pmin[beam],pmax[beam],title,subtitle,epsfile)

    dayno=0            
    for P in timeline[beam]:
        plt.plot(t[beam][:ndata],P[:ndata],label=day[beam][dayno])
        dayno+=1

    plotlinemarker(date=markerStart,ymin=pmin[beam],ymax=pmax[beam],label=None)
    plotlinemarker(date=markerEnd,ymin=pmin[beam],ymax=pmax[beam],label=None)
    plt.legend(prop={'size':12},loc='upper right',bbox_to_anchor=(0, 0, 1.05, 1.05))
    plt.savefig(epsfile,format='eps',dpi=300)
    plt.show()

    # second plot: eclipse day relative to avg of other days
    subtitle='relative to days before and after'
    reftimeline=0.25*(timeline[beam][0][:ndata]+timeline[beam][1][:ndata]+timeline[beam][3][:ndata]+timeline[beam][4][:ndata])
    epsfile=str('EMBRACE-Nancay_eclipse20150320_%.1fMHz.eps' % freq[beam])
    Prel[beam]=timeline[beam][eclipse_day][:ndata]/reftimeline
    if relpmin[beam]==None:relpmin[beam]=min(Prel)
    if relpmax[beam]==None:relpmax[beam]=max(Prel)
    initplot(tmin,tmax,relpmin[beam],relpmax[beam],title,subtitle,epsfile)
    plt.plot(t[beam],Prel[beam])
    plotlinemarker(date=markerStart,ymin=relpmin[beam],ymax=relpmax[beam],label=None)
    plotlinemarker(date=markerEnd,ymin=relpmin[beam],ymax=relpmax[beam],label=None)
    plt.savefig(epsfile,format='eps',dpi=300)
    plt.show()

    
# third plot: the two frequencies overlapped
epsfile='EMBRACE-Nancay_eclipse20150320.eps'
beam='A' # for window limits
title='EMBRACE@Nancay: Eclipse of 20 March 2015'
initplot(tmin,tmax,relpmin[beam],relpmax[beam],title,subtitle,epsfile)
for beam in ['A','B']:
    label=str('Beam%s: %.1fMHz' % (beam,freq[beam]))
    plt.plot(t[beam],Prel[beam],label=label)
plotlinemarker(date=markerStart,ymin=relpmin[beam],ymax=relpmax[beam],label=None)
plotlinemarker(date=markerEnd,ymin=relpmin[beam],ymax=relpmax[beam],label=None)
plt.legend(prop={'size':12},loc='upper right',bbox_to_anchor=(0, 0, 1.05, 1.05))
plt.savefig(epsfile,format='eps',dpi=300)
plt.show()
    
