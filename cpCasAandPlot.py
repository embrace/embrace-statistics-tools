"""
This is a simplistic script to copy a CasA fits file and plot the thing

arguments: date, assume rotation angle

execute from within the ~/data/embrace on Linnaeus

no error checking
"""

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from embraceStats import embraceStats as estats
import sys

fname='CasA_BeamA_bsx_'+sys.argv[1]+'.fits'
rotang=sys.argv[2]

import os
os.system('cp -puv scudata/'+fname+' .')

bx=estats(fname)
bx.plotTimeline(1175.6,6,subtitle='Assumed Array Rotation: '+rotang+' degrees',pmin=83,pmax=86)
pngfile=fname.replace('.fits','_BeamA_Dig00_timeline1175.6.png')
os.system('scp -p '+pngfile+' torchinsky@telesto:')
