"""
$Id: plotONOFF.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Thu Dec  1 15:07:32 CET 2011
  copied from doit.py in the data reduction directory


# to plot ON-OFF CasA_tracking with labels for tracking and calibrating
# data file: 

this will eventually be incorporated into BLStats

$modified: Thu Dec  8 11:58:16 CET 2011
  use some methods already defined in class BLStats
  ie. pltObsModeTimes()

"""

import re
import datetime as dt

pngfile=bsx.filename.replace('.fits','_ON-OFF_timeline.png')
onoff=bsx.Timeline(1175.6,0)-bsx.Timeline(1175.6,0,direction='y')

if not isinstance(y,list) and not isinstance(y[0],float):
    y=[-1.5,1.5]

plt.figure(figsize=(12.80,9.60))
plt.title('Cas-A tracking with two drift scans at 1175.6MHz, ON-OFF')
plt.ylabel('ON-OFF power / dB')
plt.xlabel('time / seconds since '+str(bsx.Timestamp(1))+' UT')

plt.axis([-60,bsx.ndata,y[0],y[1]])
plt.plot(onoff)
bsx.pltObsModeTimes(y)

ret=plt.savefig(pngfile,format='png',dpi=100)
